<?php get_header(); ?>
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <section class="upper-page section-dark" id="home">
			<?php
			$class1 = "overlay-dark-15";
			if(get_field("video_background_header")){
				$class1 = "overlay-dark-15-video";
			}
			$url_bg_image = get_field("image_background_header");
			?>
            <div class="hero-fullscreen overlay <?= $class1; ?>">
				<?php if(get_field("video_background_header")){ ?>
                <div class="hero-bg hero-fullscreen-FIX">
                    <div class="html5-videoContainment" data-vide-bg="<?php the_field("video_background_header"); ?>" data-vide-options="loop: true, muted: true"></div>
                    <div class="html5-bg" style="background-image: url(<?= $url_bg_image; ?>);"></div>
                </div>
				<?php }else{ ?>
				<div class="hero-fullscreen-FIX">
                    <div class="hero-bg bg-img-SINGLE" style="background-image: url(<?= $url_bg_image; ?>);"></div>
                </div>
				<?php } ?>
            </div>
            <div class="center-container">
                <div class="center-block">
                    <div class="introduction-wrapper fadeIn-element">
                        <div class="the-overline the-overline-home"></div>
                        <div class="inner-divider-half"></div>
                        <h1 class="text62">
                            Saf<span class="text-hide">e</span>ty sponsor<br>
							<span class="text82">of th<span class="text-hide">e</span> road</span>
                        </h1>
                        <div class="inner-divider-half"></div>
						<div class="page-scroll"><a class="custom-button fadeIn-element" href="#gripline">Scopri di più</a></div>
                    </div>
                </div>
            </div>
            <div class="scroll-indicator fadeIn-element">
                <div class="scroll-indicator-wrapper">
                    <div class="scroll-line"></div>
                </div>
            </div>
        </section>
        <section class="section-dark inner-spacer" id="mission">
			<div class="inner-divider"></div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12">
						<div class="post-title post-title-light">
							<h2 class="text110 f-c-m orange text-center">MISSION</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="inner-divider"></div>
            <div class="halves">
                <div class="half">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
						<div class="post-title post-title-light">
							<h3 class="orange f-c-m">Una mobilità sicura, pulita e sostenibile. Continental illustra alla Milano Design Week il suo progetto per il futuro.</h3>
						</div>
                        <div class="section-txt-services f-c-b">
                            <p>
                                Con la sponsorizzazione del format <strong>BluE</strong>, <strong>Continental</strong> propone nella città di Milano uno scenario fatto di <strong>sicurezza e sostenibilità</strong>. <strong>Il gruppo investe e produce per rendere i veicoli e le strade sicure per arrivare a “Vision Zero”: una mobilità a Zero incidenti, Zero feriti e Zero vittime della strada.</strong> Una visione che consenta a tutti gli utenti, siano essi pedoni, ciclisti, motociclisti, automobilisti e autisti del trasporto pesante di mezzi e persone di condividere un ecosistema complesso in sicurezza.
                            </p>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
				<div class="half" style="background-image:url('<?=get_template_directory_uri()?>/public/images/img-mobilita.jpg'); background-position:center center;">
				</div>
            </div>
			<div class="inner-divider"></div>
        </section>
        <section class="section-dark" id="gripline">
            <div class="halves">
                <div class="half">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="inner-divider-half"></div>
						<div class="post-title post-title-light">
							<img src="<?=get_template_directory_uri()?>/public/images/gripline.svg" alt="Gripline" class="img-responsive">
							<!--<span class="text110 f-c-b">GRIPLIN<span class="text-hide">E</span></span>-->
						</div>
                        <div class="inner-divider-half"></div>
						<div class="section-txt-services">
							<p class="text-uppercase">
                                Vernice speciale con formula a microgranuli di gomma e il 35% di grip in più, per una segnaletica stradale più sicura.
                            </p>
						</div>
                        <div class="inner-divider-half"></div>
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
                        <div class="section-txt-services">
							<p>Per rendere concreto il suo impegno a favore della sicurezza, Continental ha creato Grip Line. Un innovativo prototipo di vernice che aumenta il grip della segnaletica orizzontale, riducendo i rischi sopratutto in caso di asfalto bagnato.</p>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
				<div class="half" style="background-image:url('<?=get_template_directory_uri()?>/public/images/strisce.jpg');"></div>
                <?php /*?><div class="half about-bg with-bg">
					<div class="hero-fullscreen overlay overlay-dark-60-video">
						<div class="hero-bg hero-fullscreen-FIX">
							<div class="html5-videoContainment" data-vide-bg="<?php the_field("video_background_header"); ?>" data-vide-options="loop: true, muted: true"></div>
							<div class="html5-bg" style="background-image: url(<?= $url_bg_image; ?>);"></div>
						</div>
					</div>
				</div><?php */?>
            </div>
			<div class="bg_black">
				<div class="inner-divider"></div>
				<div class="container">
					<?php /*?><div class="row">
						<div class="col-xs-8 col-xs-offset-2">
							<img src"<?= get_template_directory_uri()?>/img/gripline-in-numbers.png" srcset"<?= get_template_directory_uri()?>/img/gripline-in-numbers@2x.png 2x" alt="Gripline in numbers" class="img-responsive hidden-xs hidden-sm">
							<img src"<?= get_template_directory_uri()?>/img/gripline-in-numbers-m@2x.png" alt="Gripline in numbers" class="img-responsive visible-xs visible-sm">
						</div>
					</div><?php */?>
					<div class="row">
						 <div class="col-xs-12">
							<div class="inner-divider"></div>
							<img src="<?=get_template_directory_uri()?>/public/images/infografica.jpg" srcset="<?=get_template_directory_uri()?>/public/images/infografica@2x.jpg 2x" alt="" class="img-responsive hidden-xs">
							<img src="<?=get_template_directory_uri()?>/public/images/infografica-m.jpg" srcset="<?=get_template_directory_uri()?>/public/images/infografica-m@2x.jpg 2x" alt="" class="img-responsive visible-xs">
						</div>
					</div>
				</div>
				<div class="inner-divider"></div>
			</div>
        </section>

		<section class="section-light slider_events">
            <div class="inner-divider"></div>
            <div class="container-fluid">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<img src="<?=get_template_directory_uri()?>/img/il_mondo_gripline.svg" alt="Il mondo gripline" class="img-responsive">
					</div>
				</div>
				<div class="inner-divider"></div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="owl-carousel posts-carousel">
							<?php
								$args = array(
									'post_type'      => 'post',
									'posts_per_page'    => 3,
									'cat'     => 1
								);
								$wp_query = new WP_Query( $args );
								if($wp_query->have_posts()) :
									while($wp_query->have_posts()) : $wp_query->the_post();
								?>
								<div class="news-item">
									<figure class="news-content">
										<a href="<?php the_permalink(); ?>"><img alt="<?=the_title(false)?>" src="<?=get_the_post_thumbnail_url($post->ID, 'full');?>"></a>
										<figcaption>
											<div class="inner-divider-news"></div>
											<div class="the-overline the-overline-news"></div>
											<div class="inner-divider-news-half"></div>
											<h4 class="post-all-heading"><?=the_title(false)?></h4>
											<div class="inner-divider-news-half"></div>
											<h5><?=get_the_date('d F Y');?></h5>
											<div class="inner-divider-news"></div>
											<div class="section-txt-news">
												<p><?=the_excerpt()?></p>
											</div>
											<div class="inner-divider-news"></div>
											<center><a class="custom-button" href="<?php the_permalink(); ?>">Read more</a></center>
										</figcaption>
									</figure>
								</div>
								<?php
									endwhile;
								endif;
								wp_reset_query();
								wp_reset_postdata();
								?>
                        </div>
                    </div>
                </div>
            </div>
			<div class="inner-divider-half"></div>
            <center><a class="custom-button" href="<?php echo get_category_link(5); ?>">Archivio news</a></center>
			<div class="inner-divider"></div>
        </section>


		<section class="section-dark" id="mdw">
            <div class="halves">
                <div class="half -primary">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
                        <div>
                            <p class="gf-m-m black text100">CONTINENTAL ALLA MILANO DESIGN WEEK</p>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
                <div class="half about-bg hidden-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto1.jpg)"></div>
                <div class="half about-bg visible-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto1.jpg)"></div>
            </div>
        </section>
       	<section class="section-dark">
            <div class="halves">
                <div class="half about-bg hidden-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto2.jpg)"></div>
                <div class="half about-bg visible-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto2.jpg)"></div>
                <div class="half">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
                        <div>
                            <p class="gf-m-m text28">
                            	<b>Continental</b> e la <b>Gripline</b> saranno presenti alla Milano Design Week, dall’9 al 14 aprile 2019.<br />
                            	Vieni a scoprire l’innovazione applicata alla sicurezza in strada. 
							</p>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
            </div>
        </section>
        <section class="section-dark">
            <div class="halves">
                <div class="half tall" style="background-image:url(<?=get_template_directory_uri()?>/public/images/bg_gripline.jpg)">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="the-overline white"></div>
                        <div class="inner-divider-half"></div>
                        <div data-mh="group99">
                           <img id="event_gripline--logo" src="<?=get_template_directory_uri()?>/public/images/svg/gripline.svg" />
                        </div>
                        <div class="content--tall">
                        	<img class="half--marker" src="<?=get_template_directory_uri()?>/public/images/svg/marker.svg" />
                        	<p class="text25">Porta Venezia, Milano</p>
                        	<img class="half--divider" src="<?=get_template_directory_uri()?>/public/images/svg/divider.svg" />
                        	<img class="half--timer" src="<?=get_template_directory_uri()?>/public/images/svg/timer.svg" />
                        	<p class="text25">9,00 / 21,00</p>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
                <div class="half tall" style="background-image:url(<?=get_template_directory_uri()?>/public/images/bg_safety_wheels.jpg)">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="the-overline white right"></div>
                        <div class="inner-divider-half"></div>
                        <div data-mh="group99"><p id="event_safetyWheels--logo"><span>SAFETY</span><br />WHEELS</p></div>
                        <div class="content--tall">
                        	<img class="half--marker" src="<?=get_template_directory_uri()?>/public/images/svg/marker.svg" />
                        	<p class="text25">Porta Venezia, Milano</p>
                        	<img class="half--divider" src="<?=get_template_directory_uri()?>/public/images/svg/divider.svg" />
                        	<img class="half--timer" src="<?=get_template_directory_uri()?>/public/images/svg/timer.svg" />
                        	<p class="text25">9,00 / 21,00</p>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
            </div>
        </section>
		<section class="inner-spacer section-dark" id="featured-work">
            <div class="inner-divider featured-hide"></div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">
						<center><img src="<?=get_template_directory_uri()?>/img/exhibition.svg" alt="Exibithion" class="img-responsive"></center>
					</div>
				</div>
				<div class="inner-divider-half"></div>
				<div class="row">
					<div class="col-xs-12">
						<p class="white text-center">La visione di Continental è da sempre orientata alla sicurezza su tutte le strade, a una mobilità dove incidenti e rischio siano ridotti a zero.<br>
						Per questo abbiamo voluto richiamare l’attenzione sulla sicurezza degli utenti più deboli, i ciclisti, con sei dati che possono far pensare sia chi va in bici che chi con la bici condivide le stesse strade.<br>
						<strong>Perché la mobilità alternativa è il futuro e vogliamo che sia un futuro più sicuro per tutti.</strong></p>
					</div>
				</div>
			</div>
            <div class="inner-divider featured-hide"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 nopadding">
                        <!-- slick right start -->
                        <div class="slick-right">
                            <div class="img-fullwidth-wrapper">
								<div class="wheel1 wheel">
									<div class="circle" data-value="5.6"></div>
									<img src="<?=get_template_directory_uri()?>/img/wheel1-5.png" alt="" class="img-responsive">
								</div>
                            </div>
                            <div class="img-fullwidth-wrapper">
								<div class="wheel2 wheel">
									<div class="circle" data-value="52"></div>
									<img src="<?=get_template_directory_uri()?>/img/wheel2.png" alt="" class="img-responsive">
								</div>
                            </div>
                            <div class="img-fullwidth-wrapper">
								<div class="wheel3 wheel">
									<div class="circle" data-value="34"></div>
									<img src="<?=get_template_directory_uri()?>/img/wheel3.png" alt="" class="img-responsive">
								</div>
                            </div>
                            <div class="img-fullwidth-wrapper">
								<div class="wheel4 wheel">
									<div class="circle" data-value="67"></div>
									<img src="<?=get_template_directory_uri()?>/img/wheel4.png" alt="" class="img-responsive">
								</div>
                            </div>
                            <div class="img-fullwidth-wrapper">
								<div class="wheel5 wheel">
									<div class="circle" data-value="19.4"></div>
									<img src="<?=get_template_directory_uri()?>/img/wheel1-5.png" alt="" class="img-responsive">
								</div>
                             </div>
                        </div>
                    </div>
					<div class="col-md-6 nopadding">
                        <div class="slick-left inner-spacer">
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick text-center">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name text-center">
                                            <span class="facts-counter-number orange text100" data-decimals="1">5.6</span>
                                        </h3>
                                        <div class="inner-divider-news-half"></div>
										<center><div class="the-overline the-overline-featured"></div></center>
                                        <div class="inner-divider-news-half"></div>
                                        <div class="cd-section-txt">
                                            <p class="text-center">
                                                Solo il <strong>5,6%</strong> degli incidenti che coinvolgono ciclisti si concludono senza che questi riportino infortuni.
                                            </p>
											<p class="orange text-center">
												Questo è un dato che fa riflettere su cosa significa essere un soggetto “debole” sulla strada.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick text-center">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name text-center">
                                            <span class="facts-counter-number orange text100">52</span>
                                        </h3>
                                        <div class="inner-divider-news-half"></div>
										<center><div class="the-overline the-overline-featured"></div></center>
                                        <div class="inner-divider-news-half"></div>
                                        <div class="cd-section-txt">
                                            <p class="text-center">
                                                Il <strong>52%</strong> delle vittime di incidenti stradali urbani è un utente non motorizzato.
                                            </p>
											<p class="orange text-center">
												Questo dato ci ricorda che nell’ecosistema stradale gli utenti deboli sono particolarmente deboli.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick text-center">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name text-center">
											<span class="facts-counter-number orange text100">34</span>
                                        </h3>
                                        <div class="inner-divider-news-half"></div>
										<center><div class="the-overline the-overline-featured"></div></center>
                                        <div class="inner-divider-news-half"></div>
                                        <div class="cd-section-txt">
                                            <p class="text-center">
                                                Raddoppiando il numero di ciclisti per km si ottiene una diminuzione del <strong>34%</strong> del rischio di incidente.
                                            </p>
											<p class="orange text-center">
												Questo dato spiega come andare verso una mobilità leggera possa essere un vantaggio in termini di diminuzione di rischio per tutti.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick text-center">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name text-center">
											<span class="facts-counter-number orange text100">67</span>
                                        </h3>
                                        <div class="inner-divider-news-half"></div>
										<center><div class="the-overline the-overline-featured"></div></center>
                                        <div class="inner-divider-news-half"></div>
                                        <div class="cd-section-txt">
                                            <p class="text-center">
                                                Il <strong>67%</strong> degli incidenti stradali che coinvolgono biciclette è causato dallo scontro con autovetture.
                                            </p>
											<p class="orange text-center">
												Questo dato richiama al ruolo importante degli automobilisti nella creazione di un ambiente sicuro sulle strade anche per i soggetti “deboli” come i ciclisti.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name text-center">
											<span class="facts-counter-number orange text100" data-decimals="1">19.4</span>
                                        </h3>
                                        <div class="inner-divider-news-half"></div>
										<center><div class="the-overline the-overline-featured"></div></center>
                                        <div class="inner-divider-news-half"></div>
                                        <div class="cd-section-txt">
                                            <p class="text-center">
                                                Il <strong>19,4%</strong> degli incidenti causati dall’apertura della portiera di un’autovettura parcheggiata coinvolge i ciclisti.
                                            </p>
											<p class="orange text-center">
												Questo dato ci mostra come piccoli comportamenti da parte dei soggetti forti possano determinare (o abbattere) grandi fonti di rischio per i soggetti deboli.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="bar">
                        <img alt="Bar Arrow" src="<?=get_template_directory_uri()?>/img/bar.png">
                    </div>
                    <div class="slick-bottom">
                        <div>
                            <div class="cd-author">
                                <img alt="" src="<?=get_template_directory_uri()?>/img/thumb_slide1.png">
                            </div>
                        </div>
                        <div>
                            <div class="cd-author">
                                <img alt="" src="<?=get_template_directory_uri()?>/img/thumb_slide2.png">
                            </div>
                        </div>
                        <div>
                            <div class="cd-author">
                                <img alt="" src="<?=get_template_directory_uri()?>/img/thumb_slide3.png">
                            </div>
                        </div>
                        <div>
                            <div class="cd-author">
                                <img alt="" src="<?=get_template_directory_uri()?>/img/thumb_slide4.png">
                            </div>
                        </div>
                        <div>
                            <div class="cd-author">
                                <img alt="" src="<?=get_template_directory_uri()?>/img/thumb_slide5.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-divider"></div>
        </section>
        <section class="section-dark">
            <div class="halves">
                <div class="half -primary">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
                        <div>
                            <p class="gf-m-m black text100">MEZZANINO</p>
                            <p class="text25 black uppercase">Uno spazio aperto in collaborazione con Blue, per ospitare eventi, incontri e speech dedicati ai temi della sicurezza e dell’eco-sistema strada.</p>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
                <div class="half about-bg hidden-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/mezzanino.jpg)"></div>
                <div class="half about-bg visible-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/mezzanino.jpg)"></div>
            </div>
        </section>
        <?php
		$args = array(
			'post_type'      => 'evento',
			'posts_per_page'    => 1,
            'meta_key'       => 'date',
			'order'          => 'DESC',
			'orderby'        => 'meta_value_num',
			'meta_query'     => array(
				array(
					'key'    => 'date',
					'value'  => date('Y-m-d H:i:s'),
					'compare'=> '>',
					'type'   => 'DATE'
			   )
			)
		);
		$wp_query = new WP_Query( $args );
		if($wp_query->have_posts()) :
			while($wp_query->have_posts()) : $wp_query->the_post();
				$time_countdown = get_field('date');
		?>
        <section class="section-dark" id="last_event">
			<div class="parallax parallax-facts" data-parallax-speed="0.75">
				<?php /*?><div class="parallax-overlay"></div><?php */?>
				<div class="halves">
					<div class="half col-md-6">
						<div class="inner-divider-half"></div>
						<div class="post-all inner-spacer event">
							<div class="inner-divider-half"></div>
							<div class="title"><?=the_title(false)?></div>
							<div class="info">
								<?=strftime("%H:%M",strtotime(get_field('date')))?> // <?=strftime("%e %B",strtotime(get_field('date')))?> – <?=get_field('location')?>
							</div>
							<div class="desc">
								<p><?=the_excerpt()?></p>
							</div>
						</div>
						<div class="inner-divider-half"></div>
					</div>
					<div class="half col-md-6 gf-m-m" id="countdown" data-time-countdown="<?=$time_countdown?>">
						<div class="inner-divider"></div>
						<div class="cont_time"></div>
						<div class="inner-divider"></div>
					</div>
				</div>
			</div>
        </section>
		<?php
			endwhile;
		endif;
		?>
		<section id="slider_events">
            <div class="inner-divider"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="owl-carousel" id="news-carousel">
							<?php
								$args = array(
									'post_type'      => 'evento',
									'posts_per_page'    => 3,
									'meta_query'     => array(
										array(
											'key'    => 'date',
											'value'  => date('Y-m-d H:i:s'),
											'compare'=> '>=',
											'type'   => 'NUMERIC'
									   )
									)
								);
								setlocale(LC_TIME, 'it_IT');
								$wp_query = new WP_Query( $args );
								if($wp_query->have_posts()) :
									while($wp_query->have_posts()) : $wp_query->the_post();
								?>
								<div class="news-item">
									<figure class="news-content">
										<a href="javascript:;" data-evento="<?=get_the_ID()?>"><img alt="<?=the_title(false)?>" src="<?=get_the_post_thumbnail_url($post->ID, 'full');?>"></a>
										<figcaption>
											<div class="inner-divider-news"></div>
											<div class="the-overline the-overline-news"></div>
											<div class="inner-divider-news-half"></div>
											<h4 class="post-all-heading"><?=the_title(false)?></h4>
											<div class="inner-divider-news-half"></div>
											<h5><?=strftime("%e %B %Y",strtotime(get_field('date')))?></h5>
											<div class="inner-divider-news"></div>
											<div class="section-txt-news">
												<p><?=the_excerpt()?></p>
											</div>
											<div class="inner-divider-news"></div>
											<center><a class="custom-button" data-evento="<?=get_the_ID()?>" href="javascript:;">Read more</a></center>
										</figcaption>
									</figure>
								</div>
								<?php
									endwhile;
								endif;
								wp_reset_query();
								wp_reset_postdata();
								?>
                        </div>
                    </div>
                </div>
            </div>
			<center><a class="custom-button white" href="<?php echo get_post_type_archive_link("evento"); ?>">Archivio eventi</a>
			<div class="inner-divider"></div>
        </section>
		<?php
		$args = array(
			'post_type'      => 'post',
			'posts_per_page'    => 3,
			'cat'     => 4
		);
		$wp_query = new WP_Query( $args );
		if($wp_query->have_posts()) :
		?>
		<section class="section-dark" id="works">
            <div class="container-fluid nopadding">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="owl-carousel" id="works-page-img-carousel">
							<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
                            <div class="item item-grid-size">
                                <div class="works-page-carousel-item works-page-carousel-item-1">
                                    <a class="link-wrap" href="<?php the_permalink(); ?>"><span></span><span></span></a>
                                    <div class="description">
                                        <h3>
                                            <?php the_title(); ?>
                                        </h3>
                                        <div class="inner-divider-half inner-divider-half-works"></div>
                                        <div class="description-second">
                                            <h4 class="post-heading-works">
                                                <?php echo get_the_date("d F Y"); ?>
                                            </h4>
                                        </div>
                                        <div class="inner-divider-half inner-divider-half-works"></div>
                                    </div>
                                </div>
                            </div>
							<?php
							endwhile;
							wp_reset_query();
							wp_reset_postdata();
							?>
                        </div>
                    </div>
                </div>
            </div>
			<div class="inner-divider-half"></div>
			<center><a class="custom-button" href="<?php echo get_category_link(5); ?>">Archivio news</a>
			<div class="inner-divider-half"></div>
        </section>
		<?php endif; ?>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>