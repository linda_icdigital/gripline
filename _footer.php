<div class="contact-modal">
    <div class="container">
        <div class="center-container-contact-modal">
            <div class="center-block-contact-modal">
                <div class="inner-divider-half"></div>
                <div class="row contact-modal-wrapper">
                    <div class="col-xs-12">
                        <div class="col-xs-12 col-md-5"><img src="" id="modal--cover"/></div>
                        <div class="col-xs-12 col-md-7">
                            <p class="modal--title"></p>
                            <img class="half--divider" src="<?=get_template_directory_uri()?>/public/images/svg/divider.svg" />
                            <div class="modal--details">   
                                <div><img src="<?=get_template_directory_uri()?>/public/images/svg/marker.svg" /><p id="modal--location"></p></div>
                                <div><img src="<?=get_template_directory_uri()?>/public/images/svg/timer.svg" /><p id="modal--timer"></p></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row contact-modal-wrapper">
                    <div class="col-xs-12">
                    	<div id="modal--description"></div>
                    </div>
                </div>
                <div class="inner-divider-half"></div>
                <div class="row center-block-contact-modal-padding-bottom">
                    <div class="col-lg-12">
                        <div class="contact-modal-closer">
                            <span class="ion-close"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
		<footer class="bg_orange">
			<div class="inner-divider-half"></div>
			<div class="container">
				<div class="row">

					<div class="col-sm-2 col-sm-push-10 text-right">
						<img src="<?=get_template_directory_uri()?>/public/images/logo-auto.jpg" />
					</div>



					<div class="col-sm-4 col-sm-push-4 text-right">
						<?php if(get_field("link_twitter", "options") || get_field("link_facebook", "options") || get_field("link_pinterest", "options")){ ?>
						<ul class="social-icons">
							<?php if(get_field("link_twitter", "options")){ ?>
							<li class="social-icon">
								<a class="ion-social-twitter" target="_blank" href="<?php the_field("link_twitter", "options"); ?>"></a>
							</li>
							<?php } ?>
							<?php if(get_field("link_facebook", "options")){ ?>
							<li class="social-icon">
								<a class="ion-social-facebook" target="_blank" href="<?php the_field("link_facebook", "options"); ?>"></a>
							</li>
							<?php } ?>
							<?php if(get_field("link_pinterest", "options")){ ?>
							<li class="social-icon">
								<a class="ion-social-pinterest" target="_blank" href="<?php the_field("link_pinterest", "options"); ?>"></a>
							</li>
							<?php } ?>
						</ul>
						<?php } ?>
					</div>
					<div class="col-sm-4 col-sm-pull-4">
						<a href="https://www.continental-pneumatici.it/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/Continental-black.png" srcset="<?php echo get_template_directory_uri(); ?>/img/Continental-black@2x.png 2x" class="img-responsive logo2" alt="Continental" ></a>
						<?php if(get_field("continental_footer_address", "options")){ ?>
						<div class="gf-l-i address">
							<?php the_field("continental_footer_address", "options"); ?>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-center">
						<?php
						$args = array(
							'menu'            => 'footer',
							'menu_class' => 'menu gf-l-b'
						);
						wp_nav_menu($args);
						?>
					</div>
				</div>
			</div>
			<div class="inner-divider-half"></div>
		</footer>
		<a class="page-scroll" href="#home">
        <div class="to-top-arrow">
            <span class="ion-ios-arrow-up"></span>
        </div></a>
		<?php wp_footer(); ?>
    </body>
</html>