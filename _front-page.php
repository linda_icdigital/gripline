<?php get_header(); ?>
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <section class="upper-page section-dark" id="home">
			<?php
			$class1 = "overlay-dark-15";
			if(get_field("video_background_header")){
				$class1 = "overlay-dark-15-video";
			}
			$url_bg_image = get_field("image_background_header");
			?>
            <div class="hero-fullscreen overlay <?= $class1; ?>">
				<?php if(get_field("video_background_header")){ ?>
                <div class="hero-bg hero-fullscreen-FIX">
                    <div class="html5-videoContainment" data-vide-bg="<?php the_field("video_background_header"); ?>" data-vide-options="loop: true, muted: true"></div>
                    <div class="html5-bg" style="background-image: url(<?= $url_bg_image; ?>);"></div>
                </div>
				<?php }else{ ?>
				<div class="hero-fullscreen-FIX">
                    <div class="hero-bg bg-img-SINGLE" style="background-image: url(<?= $url_bg_image; ?>);"></div>
                </div>
				<?php } ?>
            </div>
            <div class="center-container">
                <div class="center-block">
                    <div class="introduction-wrapper fadeIn-element">
                        <div class="the-overline the-overline-home"></div>
                        <div class="inner-divider-half"></div>
                        <h1 class="text62">
                            Saf<span class="text-hide">e</span>ty sponsor<br>
							<span class="text82">of th<span class="text-hide">e</span> road</span>
                        </h1>
                        <div class="inner-divider-half"></div>
						<div class="page-scroll"><a class="custom-button fadeIn-element" href="#gripline">Scopri di più</a></div>
                    </div>
                </div>
            </div>
            <div class="scroll-indicator fadeIn-element">
                <div class="scroll-indicator-wrapper">
                    <div class="scroll-line"></div>
                </div>
            </div>
        </section>


        <section class="section-dark" id="bikelife">
            <div class="halves">
                <div class="half about-bg hidden-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto7.jpg)"></div>
                <div class="half about-bg visible-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto7.jpg)"></div>
                <div class="half -primary">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
                        <div>
                            <p class="gf-m-b black text65">#BIKELIFE</p>
                        </div>
                        <div>
                            <p class="gf-m-b black text28">Un'iniziativa per rendere gli appassionati di ciclismo protagonisti. Il Giro d'Italia aspetta il suo tifoso più grande.</p>
                        </div>
                        <div class="inner-divider-half"></div>
                        <div>
                            <a id="bikelife-hp" class="custom-button black fadeIn-element" href="<?php the_permalink(133); ?>">Scopri di più</a>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
            </div>
        </section>


        <section class="section-dark" id="giro">
            <div class="halves">
                <div class="half -primary">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
                        <div>
                            <p class="gf-m-b black text65">SAFETY SPONSOR OF GIRO D'ITALIA</p>
                        </div>
                        <div>
                            <p class="gf-m-b black text28">Continental è orgogliosa di essere sponsor della più importante competizione ciclistica italiana.</p>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
                <div class="half about-bg hidden-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto3.jpg)"></div>
                <div class="half about-bg visible-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto3.jpg)"></div>
            </div>
        </section>
        <section class="section-dark">
            <div class="halves">
                <div class="half about-bg hidden-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto4.jpg)"></div>
                <div class="half about-bg visible-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto4.jpg)"></div>
                <div class="half">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
                        <div>
                            <p class="gf-m-r text28">
                                Un'occasione per sensibilizzare sulla <b>sicurezza dei ciclisti</b> e dare risposte concrete a una delle categorie più a rischio dell'ecosistema strada. 
                            </p>
                        </div>
                        <div class="inner-divider-half"></div>
                        <div>
                            <a class="custom-button fadeIn-element" href="<?php the_permalink(107); ?>">Scopri di più</a>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
            </div>
        </section>



        <section class="section-dark inner-spacer" id="mission">
			<div class="inner-divider"></div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12">
						<div class="post-title post-title-light">
							<h2 class="text110 f-c-m orange text-center">LA NOSTRA MISSION</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="inner-divider"></div>
            <div class="halves">
                <div class="half">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
						<div class="post-title post-title-light">
							<h3 class="orange f-c-m">Continental al Giro d’Italia<br>per il futuro delle nostre strade.</h3>
						</div>
                        <div class="section-txt-services f-c-l">
                            <p>
                                In occasione della sponsorship con il Giro, Continental propone un nuovo scenario per le strade italiane. Una nuova mobilità fatta di sicurezza, anche, e soprattutto, per i ciclisti. La salvaguardia delle categorie più deboli è infatti alla base della Vision Zero del gruppo, una visione a Zero incidenti, Zero feriti e Zero vittime della strada. Perché tutti gli utenti, siano essi pedoni, ciclisti, motociclisti, automobilisti e autisti del trasporto pesante di mezzi e persone condividano un ecosistema complesso in totale sicurezza.<br> Sappiamo essere una strada ancora lunga, ma con il Giro d’Italia vogliamo percorrere più tappe possibili.
                            </p>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
				<div class="half" style="background-image:url('<?=get_template_directory_uri()?>/public/images/img-mobilita.jpg'); background-position:center center;">
				</div>
            </div>
			<div class="inner-divider"></div>
        </section>


        <section class="section-dark" id="gripline">
            <div class="inner-divider"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="post-title post-title-light">
                            <h2 class="text110 f-c-m orange text-center">IL NOSTRO IMPEGNO</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-divider"></div>
            <div class="halves">
                <div class="half">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="inner-divider-half"></div>
						<div class="post-title post-title-light">
							<img src="<?=get_template_directory_uri()?>/public/images/gripline.svg" alt="Gripline" class="img-responsive">
							<!--<span class="text110 f-c-b">GRIPLIN<span class="text-hide">E</span></span>-->
						</div>
                        <div class="inner-divider-half"></div>
						<div class="section-txt-services">
							<p>
                                Una vernice speciale che aumenta il grip della segnaletica stradale fino al 35%, in condizioni di bagnato e pioggia. Un progetto innovativo, che dalle strade del Giro arriva su quelle di tutti i ciclisti. 
                            </p>
						</div>
                        <div class="inner-divider-half"></div>
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
                        <div class="section-txt-services">
                            <a class="custom-button fadeIn-element" href="<?php the_permalink(202); ?>">Scopri di più</a>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
				<div class="half about-bg hidden-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto5.jpg)"></div>
                <div class="half about-bg visible-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto5.jpg)"></div> 
            </div>			
        </section>
        <section class="section-dark">
            <div class="halves">
                <div class="half about-bg hidden-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto6.jpg)"></div>
                <div class="half about-bg visible-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/foto6.jpg)"></div>
                <div class="half">
                    <div class="inner-divider"></div>
                    <div class="post-all inner-spacer">
                        <div class="inner-divider-half"></div>
                        <div class="post-title post-title-light">
                            <span class="text110 orange f-c-b">SAFETY POINT</span>
                        </div>
                        <div class="inner-divider-half"></div>
                        <div class="section-txt-services">
                            <p>
                                Un’installazione dove esplorare le nuove frontiere della sicurezza in strada, sensibilizzando tutti, ciclisti e non, su tematiche e problemi legati alla mobilità di oggi. Uno spazio che accompagna il Giro d’Italia, per tracciare una nuova strada.<br>Quella della sicurezza. 
                            </p>
                        </div>
                        <div class="inner-divider-half"></div>
                        <div class="the-overline"></div>
                        <div class="inner-divider-half"></div>
                        <div class="section-txt-services">
                            <a class="custom-button fadeIn-element" href="<?php the_permalink(71); ?>">Scopri di più</a>
                        </div>
                    </div>
                    <div class="inner-divider"></div>
                </div>
            </div>
        </section>


		<section class="inner-spacer section-dark" id="featured-work">
            <div class="inner-divider featured-hide"></div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12">
						<center><img src="<?=get_template_directory_uri()?>/img/dove_siamo_partiti.svg" alt="Da dove siamo partiti" class="img-responsive"></center>
					</div>
				</div>
				<div class="inner-divider-half"></div>
				<div class="row">
					<div class="col-xs-12">
						<p class="white text-center">Ogni iniziativa legata alla sicurezza parte sempre da dati concreti.<br><strong>Solo da qui si possono risolvere problemi veri.</strong></p>
					</div>
				</div>
			</div>
            <div class="inner-divider featured-hide"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 nopadding">
                        <!-- slick right start -->
                        <div class="slick-right">
                            <div class="img-fullwidth-wrapper">
								<div class="wheel1 wheel">
									<div class="circle" data-value="5.6"></div>
									<img src="<?=get_template_directory_uri()?>/img/wheel1-5.png" alt="" class="img-responsive">
								</div>
                            </div>
                            <div class="img-fullwidth-wrapper">
								<div class="wheel2 wheel">
									<div class="circle" data-value="52"></div>
									<img src="<?=get_template_directory_uri()?>/img/wheel2.png" alt="" class="img-responsive">
								</div>
                            </div>
                            <div class="img-fullwidth-wrapper">
								<div class="wheel3 wheel">
									<div class="circle" data-value="34"></div>
									<img src="<?=get_template_directory_uri()?>/img/wheel3.png" alt="" class="img-responsive">
								</div>
                            </div>
                            <div class="img-fullwidth-wrapper">
								<div class="wheel4 wheel">
									<div class="circle" data-value="67"></div>
									<img src="<?=get_template_directory_uri()?>/img/wheel4.png" alt="" class="img-responsive">
								</div>
                            </div>
                            <div class="img-fullwidth-wrapper">
								<div class="wheel5 wheel">
									<div class="circle" data-value="19.4"></div>
									<img src="<?=get_template_directory_uri()?>/img/wheel1-5.png" alt="" class="img-responsive">
								</div>
                             </div>
                        </div>
                    </div>
					<div class="col-md-6 nopadding">
                        <div class="slick-left inner-spacer">
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick text-center">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name text-center">
                                            <span class="facts-counter-number orange text100" data-decimals="1">5.6</span>
                                        </h3>
                                        <div class="inner-divider-news-half"></div>
										<center><div class="the-overline the-overline-featured"></div></center>
                                        <div class="inner-divider-news-half"></div>
                                        <div class="cd-section-txt">
                                            <p class="text-center">
                                                Solo il <strong>5,6%</strong> degli incidenti che coinvolgono ciclisti si concludono senza che questi riportino infortuni.
                                            </p>
											<p class="orange text-center">
												Questo è un dato che fa riflettere su cosa significa essere un soggetto “debole” sulla strada.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick text-center">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name text-center">
                                            <span class="facts-counter-number orange text100">52</span>
                                        </h3>
                                        <div class="inner-divider-news-half"></div>
										<center><div class="the-overline the-overline-featured"></div></center>
                                        <div class="inner-divider-news-half"></div>
                                        <div class="cd-section-txt">
                                            <p class="text-center">
                                                Il <strong>52%</strong> delle vittime di incidenti stradali urbani è un utente non motorizzato.
                                            </p>
											<p class="orange text-center">
												Questo dato ci ricorda che nell’ecosistema stradale gli utenti deboli sono particolarmente deboli.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick text-center">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name text-center">
											<span class="facts-counter-number orange text100">34</span>
                                        </h3>
                                        <div class="inner-divider-news-half"></div>
										<center><div class="the-overline the-overline-featured"></div></center>
                                        <div class="inner-divider-news-half"></div>
                                        <div class="cd-section-txt">
                                            <p class="text-center">
                                                Raddoppiando il numero di ciclisti per km si ottiene una diminuzione del <strong>34%</strong> del rischio di incidente.
                                            </p>
											<p class="orange text-center">
												Questo dato spiega come andare verso una mobilità leggera possa essere un vantaggio in termini di diminuzione di rischio per tutti.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick text-center">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name text-center">
											<span class="facts-counter-number orange text100">67</span>
                                        </h3>
                                        <div class="inner-divider-news-half"></div>
										<center><div class="the-overline the-overline-featured"></div></center>
                                        <div class="inner-divider-news-half"></div>
                                        <div class="cd-section-txt">
                                            <p class="text-center">
                                                Il <strong>67%</strong> degli incidenti stradali che coinvolgono biciclette è causato dallo scontro con autovetture.
                                            </p>
											<p class="orange text-center">
												Questo dato richiama al ruolo importante degli automobilisti nella creazione di un ambiente sicuro sulle strade anche per i soggetti “deboli” come i ciclisti.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name text-center">
											<span class="facts-counter-number orange text100" data-decimals="1">19.4</span>
                                        </h3>
                                        <div class="inner-divider-news-half"></div>
										<center><div class="the-overline the-overline-featured"></div></center>
                                        <div class="inner-divider-news-half"></div>
                                        <div class="cd-section-txt">
                                            <p class="text-center">
                                                Il <strong>19,4%</strong> degli incidenti causati dall’apertura della portiera di un’autovettura parcheggiata coinvolge i ciclisti.
                                            </p>
											<p class="orange text-center">
												Questo dato ci mostra come piccoli comportamenti da parte dei soggetti forti possano determinare (o abbattere) grandi fonti di rischio per i soggetti deboli.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="bar">
                        <img alt="Bar Arrow" src="<?=get_template_directory_uri()?>/img/bar.png">
                    </div>
                    <div class="slick-bottom">
                        <div>
                            <div class="cd-author">
                                <img alt="" src="<?=get_template_directory_uri()?>/img/thumb_slide1.png">
                            </div>
                        </div>
                        <div>
                            <div class="cd-author">
                                <img alt="" src="<?=get_template_directory_uri()?>/img/thumb_slide2.png">
                            </div>
                        </div>
                        <div>
                            <div class="cd-author">
                                <img alt="" src="<?=get_template_directory_uri()?>/img/thumb_slide3.png">
                            </div>
                        </div>
                        <div>
                            <div class="cd-author">
                                <img alt="" src="<?=get_template_directory_uri()?>/img/thumb_slide4.png">
                            </div>
                        </div>
                        <div>
                            <div class="cd-author">
                                <img alt="" src="<?=get_template_directory_uri()?>/img/thumb_slide5.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-divider"></div>
        </section>


        <section class="section-light slider_events" id="news">
            <div class="inner-divider"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="post-title post-title-light">
                            <h2 class="text110 f-c-m orange text-center">IL MONDO CONTINENTAL</h2>
                        </div>
                    </div>
                </div>
                <div class="inner-divider"></div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="owl-carousel posts-carousel">
                            <?php
                                $args = array(
                                    'post_type'      => 'post',
                                    'posts_per_page'    => 3,
                                    'cat'     => 1
                                );
                                $wp_query = new WP_Query( $args );
                                if($wp_query->have_posts()) :
                                    while($wp_query->have_posts()) : $wp_query->the_post();
                                ?>
                                <div class="news-item">
                                    <figure class="news-content">
                                        <a href="<?php the_permalink(); ?>"><img alt="<?=the_title(false)?>" src="<?=get_the_post_thumbnail_url($post->ID, 'full');?>"></a>
                                        <figcaption>
                                            <div class="inner-divider-news"></div>
                                            <div class="the-overline the-overline-news"></div>
                                            <div class="inner-divider-news-half"></div>
                                            <h4 class="post-all-heading"><?=the_title(false)?></h4>
                                            <div class="inner-divider-news-half"></div>
                                            <h5><?=get_the_date('d F Y');?></h5>
                                            <div class="inner-divider-news"></div>
                                            <div class="section-txt-news">
                                                <p><?=the_excerpt()?></p>
                                            </div>
                                            <div class="inner-divider-news"></div>
                                            <center><a class="custom-button" href="<?php the_permalink(); ?>">Scopri di più</a></center>
                                        </figcaption>
                                    </figure>
                                </div>
                                <?php
                                    endwhile;
                                endif;
                                wp_reset_query();
                                wp_reset_postdata();
                                ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-divider-half"></div>
            <center><a class="custom-button" href="<?php echo get_category_link(5); ?>">Archivio news</a></center>
            <div class="inner-divider"></div>
        </section>



        <section id="slider_events">
            <div class="container-fluid">
                <div class="inner-divider"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="post-title post-title-light">
                            <h2 class="text110 f-c-m orange text-center">EVENTI</h2>
                        </div>
                    </div>
                </div>
                <div class="inner-divider"></div>
                <div class="row">
                    <div class="col-sm-12">


                         
                        <?php 

                        $posts = get_field('eventi_sticky');

                        if( $posts ): ?>
                            <div class="owl-carousel" id="news-carousel">
                            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                                <?php setup_postdata($post); ?>

                                <div class="news-item">
                                <figure class="news-content">
                                    <a href="javascript:;" data-evento="<?=get_the_ID()?>"><img alt="<?=the_title(false)?>" src="<?=get_the_post_thumbnail_url($post->ID, 'full');?>"></a>
                                    <figcaption>
                                        <div class="inner-divider-news"></div>
                                        <div class="the-overline the-overline-news"></div>
                                        <div class="inner-divider-news-half"></div>
                                        <h4 class="post-all-heading"><?=the_title(false)?></h4>
                                        <div class="inner-divider-news-half"></div>
                                        <h5><?=strftime("%e %B %Y",strtotime(get_field('date')))?></h5>
                                        <div class="inner-divider-news"></div>
                                        <div class="section-txt-news">
                                            <p><?=the_excerpt()?></p>
                                        </div>
                                        <div class="inner-divider-news"></div>
                                        <center><a class="custom-button" data-evento="<?=get_the_ID()?>" href="javascript:;">Read more</a></center>
                                    </figcaption>
                                </figure>
                            </div>


                            <?php endforeach; ?>
                             </div>
                            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                        <?php else : ?>
                            <div class="post-all">
                                <p class="white text-center">Al momento non ci sono eventi in arrivo!</p>
                            </div>
                        <?php
                            endif;
                            wp_reset_query();
                            wp_reset_postdata();
                        ?>


                        <?php /*
                            $args = array(
                                'post_type'      => 'evento',
                                'posts_per_page'    => 3,
                                'meta_query'     => array(
                                    array(
                                        'key'    => 'date',
                                        'value'  => date('Y-m-d H:i:s'),
                                        'compare'=> '>=',
                                        'type'   => 'NUMERIC'
                                   )
                                )
                            );
                            setlocale(LC_TIME, 'it_IT');
                            $wp_query = new WP_Query( $args );
                            if($wp_query->have_posts()) :
                        ?>
                        <div class="owl-carousel" id="news-carousel">
                            <?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
                            <div class="news-item">
                                <figure class="news-content">
                                    <a href="javascript:;" data-evento="<?=get_the_ID()?>"><img alt="<?=the_title(false)?>" src="<?=get_the_post_thumbnail_url($post->ID, 'full');?>"></a>
                                    <figcaption>
                                        <div class="inner-divider-news"></div>
                                        <div class="the-overline the-overline-news"></div>
                                        <div class="inner-divider-news-half"></div>
                                        <h4 class="post-all-heading"><?=the_title(false)?></h4>
                                        <div class="inner-divider-news-half"></div>
                                        <h5><?=strftime("%e %B %Y",strtotime(get_field('date')))?></h5>
                                        <div class="inner-divider-news"></div>
                                        <div class="section-txt-news">
                                            <p><?=the_excerpt()?></p>
                                        </div>
                                        <div class="inner-divider-news"></div>
                                        <center><a class="custom-button" data-evento="<?=get_the_ID()?>" href="javascript:;">Read more</a></center>
                                    </figcaption>
                                </figure>
                            </div>
                            <?php endwhile; ?>
                        </div>

                        <?php else : ?>
                            <div class="post-all">
                                <p class="white text-center">Al momento non ci sono eventi in arrivo!</p>
                            </div>
                        <?php
                        endif;
                        wp_reset_query();
                        wp_reset_postdata();
                        */
                        ?>


                    </div>
                </div>
            </div>
            <div class="inner-divider-half"></div>
            <center><a class="custom-button white" href="<?php echo get_post_type_archive_link("evento"); ?>">Archivio eventi</a>
            <div class="inner-divider"></div>
        </section>


	<?php endwhile; endif; ?>
<?php get_footer(); ?>