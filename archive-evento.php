<?php get_header(); setlocale(LC_TIME, 'it_IT.UTF8');?>
	<section class="upper-page section-dark" id="home">
        <div class="hero-fullscreen overlay">
			<div class="hero-fullscreen-FIX">
                <div class="hero-bg bg-img-SINGLE" style="background-image: url(<?=get_template_directory_uri()?>/public/images/bg_archivio.jpg);"></div>
            </div>
        </div>
        <div class="center-container">
            <div class="center-block">
                <div class="introduction-wrapper fadeIn-element">
                    <div class="the-overline the-overline-home"></div>
                    <div class="inner-divider-half"></div>
                    <h1 class="text113">EVENTI</h1>
                </div>
            </div>
        </div>
        <div class="scroll-indicator fadeIn-element">
            <div class="scroll-indicator-wrapper">
                <div class="scroll-line"></div>
            </div>
        </div>
    </section>
    <section class="wrapper_card">
    	<div class="columns">
    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	    	<div class="column">
		    	<div class="archive_card">
			    	<figure class="news-content">
						<?php if(get_the_post_thumbnail_url($post->ID, 'full')){ ?>
			            <a href="javascript:;" data-evento="<?=get_the_ID()?>"><img alt="<?=the_title(false)?>" src="<?=get_the_post_thumbnail_url($post->ID, 'full');?>"></a>
						<?php } ?>
			            <figcaption>
			                <div class="inner-divider-news-half"></div>
			                <div class="the-overline the-overline-news"></div>
			                <div class="inner-divider-news-half"></div>
			                <h4 data-mh="group1" class="post-all-heading"><span><?=the_title(false)?></span></h4>
			                <div class="inner-divider-news-half"></div>
			                <h5><?=strftime("%e %B %Y",strtotime(get_field('date')))?></h5>
			                <div class="inner-divider-news-half"></div>
			                <div data-mh="group2" class="section-txt-news">
			                    <p><?=the_excerpt()?></p>
			                </div>
			                <div class="inner-divider-news-half"></div>
			                <center><a class="custom-button" href="javascript:;" data-evento="<?=get_the_ID()?>">Read more</a></center>
			            </figcaption>
			        </figure>
			    </div>
			</div>
    <?php endwhile; endif; ?>
    	</div>
    </section>
<?php get_footer(); ?>