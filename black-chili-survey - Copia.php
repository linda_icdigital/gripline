<?php 
	/* Template Name: Black chili Survey */
	get_header(); 
	setlocale(LC_TIME, 'it_IT.UTF8');


	// CHECK URL UTENTE - PRESENZA PARAMETRI
	if(!isset($_GET['id'])) die();
	if(!isset($_GET['email'])) die();

	global $wpdb;

	$wpdb->get_results( "SELECT * FROM `utenti` WHERE id='".$_GET['id']."' AND email='".$_GET['email']."'");

	if($wpdb->num_rows != 1){

		die();
		exit;

	}

	// SE UTENTE VERIFICATO CERCO SE HA FATTO LA SURVEY
	$survey = $wpdb->get_results( "SELECT * FROM `utenti_survey` WHERE id_utente='".$_GET['id']."'", ARRAY_A);

	// NON HA ANCORA FATTO UNA SURVEY
	if($wpdb->num_rows == 0){

		$step_initial = 0;
		$class_initial[1]="active";

	} else {

		$survey = $survey[0];

		// CICLO GLI STEP
		for($i=1;$i<14;$i++){


			if($survey['fb_url'] != '' || $survey['ig_url'] != ''){

				$class_initial[14]="active";
				break;

			}else{

				if($survey['risposta_'.$i] == ''){
					// INDIVIDUO LO STEP CHE NON HA ANCORA FATTO
					$step_initial = $i;
					$class_initial[$i]="active";
					break;
				}
			}

		}

	}

?>
	<div id="home"></div>
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>


    <section class="upper-page section-dark screen">
		<?php $url_bg_image = get_the_post_thumbnail_url($post->ID, 'full'); ?>
        <div class="hero-fullscreen overlay overlay-dark-15">
			<div class="hero-fullscreen-FIX">
                <div class="hero-bg bg-img-SINGLE" style="background-image: url(<?= $url_bg_image; ?>);"></div>
            </div>
        </div>
    </section>

	<section class="wrapper_card inner-spacer wrapper_art">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="inner-divider-news"></div>
					<div class="the-overline <?php if($category_id == 4){ ?> black <?php } ?>"></div>
					<div class="inner-divider-news-half"></div>
					<h1 class="post-all-heading"><?=the_title(false)?></h1>
					<div class="steplist">
						<input type="hidden" id="id_utente" value="<?=$_GET['id']?>" />
						<input type="hidden" id="email_utente" value="<?=$_GET['email']?>" />

						<?php //if($step_initial <= 1){ ?>
<!---------------------- STEP 1 -->
						<div class="step step_1 <?php echo $class_initial[1];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[1];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>1. Storia</strong>
			                        </p>
									<p class="title_domanda">Continental nasce nel 1871 come azienda produttrice di:</p>
									<div class="steps_bar clearfix">
										<span class="active"></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_1" data-risposta="1" data-punteggio="10" value="Scarpe, abbigliamento, tessuti">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Scarpe, abbigliamento, tessuti</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_1"  data-risposta="2" data-punteggio="0" value="Oggetti in gomma come cuscinetti per cavalli, tubi per vapore, acqua e gas, gomma per palloni, aerostati, velivoli, medicina e articoli igienico-sanitari">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Oggetti in gomma come cuscinetti per cavalli, tubi per vapore, acqua e gas, gomma per palloni, aerostati, velivoli, medicina e articoli igienico-sanitari</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_1"  data-risposta="3" data-punteggio="100" value="Oggetti in metallo per il successo delle industrie durante la seconda rivoluzione industriale">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Oggetti in metallo per il successo delle industrie durante la seconda rivoluzione industriale</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Peccato, risposta errata! Ecco qualche cenno storico su Continental: l’8 ottobre 1871 venne costituita a Hannover la società per azioni Continental-Caoutchouc- und Gutta-Percha Compagnie. Nello stabilimento principale si producevano articoli in gomma morbida, tessuti gommati e pneumatici pieni per carrozze e biciclette.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Complimenti, risposta esatta! L’8 ottobre 1871 venne costituita a Hannover la società per azioni Continental-Caoutchouc- und Gutta-Percha Compagnie. Nello stabilimento principale si producevano articoli in gomma morbida, tessuti gommati e pneumatici pieni per carrozze e biciclette.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
						<?php //} ?>
						<?php //if($step_initial <= 2){ ?>
<!---------------------- STEP 2 -->	
						<div class="step step_2 <?php echo $class_initial[2];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[2];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>2. Il Gruppo</strong>
			                        </p>
									<p class="title_domanda">Quali sono le divisioni Continental e di cosa si occupano:</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span class="active"></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_2" data-risposta="1" value="Non ha divisioni: Continental è un’azienda che da sempre ha prodotto solo pneumatici">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Non ha divisioni: Continental è un’azienda che da sempre ha prodotto solo pneumatici</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_2"  data-risposta="2" value="Il gruppo Continental è composto soltanto da 3 divisioni">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Il gruppo Continental è composto soltanto da 3 divisioni<br>
	                                                	- Chassis & Safety (tecnologie attive e passive per la sicurezza di guida)<br>
	                                                	- Powertrain (efficienti soluzioni di sistema per la catena cinematica dei veicoli al fine di ottimizzare il consumo di carburante)<br>
	                                                	- Tire (Pneumatici per tutti i tipi di veicoli)
	                                            	</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_2"  data-risposta="3" value="Continental è una realtà automotive completa che sviluppa tecnologie intelligenti per il trasporto di cose e persone e conta 5 divisioni">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Continental è una realtà automotive completa che sviluppa tecnologie intelligenti per il trasporto di cose e persone e conta 5 divisioni<br>
	                                                	- Chassis & Safety (tecnologie attive e passive per la sicurezza di guida)<br>
	                                                	- Powertrain (efficienti soluzioni di sistema per la catena cinematica dei veicoli al fine di ottimizzare il consumo di carburante)<br>
	                                                	- Interior (gestione dell’informazione dentro e fuori il veicolo)<br>
	                                                	- Tire (Pneumatici per tutti i tipi di veicoli)<br>
	                                                	- ContiTech (produzione di soluzioni per l’industria)
	                                                </span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Peccato, risposta errata! Continental è molto di più: è composta da ben 5 divisioni e sviluppa tecnologie intelligenti per il trasporto di cose e persone. I principali clienti operano nell’industria automobilistica, in settori industriali chiave e nel mercato dei consumatori finali. Grazie alle sue 5 divisioni, si delinea come un partner di estrema affidabilità fornendo ai clienti soluzioni sostenibili, sicure, confortevoli, personalizzate.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Complimenti, risposta esatta! Continental è composta da ben 5 divisioni e sviluppa tecnologie intelligenti per il trasporto di cose e persone. I principali clienti operano nell’industria automobilistica, in settori industriali chiave e nel mercato dei consumatori finali. Grazie alle sue 5 divisioni, si delinea come un partner di estrema affidabilità fornendo ai clienti soluzioni sostenibili, sicure, confortevoli, personalizzate e convenienti.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
		                <?php //} ?>
						<?php //if($step_initial <= 3){ ?>
<!---------------------- STEP 3 -->	
						<div class="step step_3 <?php echo $class_initial[3];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[3];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>3. Vettorizzazione della forza</strong>
			                        </p>
									<p class="title_domanda">Lo pneumatico Continental SportContact 6 è stato progettato per le vetture sportive di alto profilo e grazie alla tecnologia Aralon 350™, consente la massima stabilità fino a 350 km/h. Un’altra delle sue principali caratteristiche è la “vettorizzazione della forza”. Che cosa significa?</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span class="active"></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_3" data-risposta="1" value="Una miglior precisione dello sterzo, un’elevata stabilità di guida in curva e il massimo controllo in ogni condizione di guida del veicolo">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Una miglior precisione dello sterzo, un’elevata stabilità di guida in curva e il massimo controllo in ogni condizione di guida del veicolo</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_3"  data-risposta="2" value="Una crescita esponenziale della potenza della vettura per dare maggiore velocità al veicolo">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Una crescita esponenziale della potenza della vettura per dare maggiore velocità al veicolo</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_3"  data-risposta="3" value="Un aumento della potenza dei veicoli non sportivi migliorandone l’accelerazione da 0 a 100">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Un aumento della potenza dei veicoli non sportivi migliorandone l’accelerazione da 0 a 100</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Peccato, risposta errata! La vettorizzazione della forza consente miglior precisione dello sterzo, un’elevata stabilità di guida in curva e il massimo controllo in ogni condizione di guida del veicolo.  Il tutto è possibile grazie a un’ottimizzazione del disegno del battistrada e al profilo a macro-blocchi dello pneumatico SportContact 6 di Continental.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Complimenti, risposta esatta! Grazie a un’ottimizzazione del disegno del battistrada e al profilo a macro-blocchi, lo pneumatico SportContact 6 consente una miglior precisione dello sterzo, un’elevata stabilità di guida in curva e il massimo controllo in ogni condizione di guida del veicolo.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
		                <?php //} ?>
						<?php //if($step_initial <= 4){ ?>
<!---------------------- STEP 4 -->		
						<div class="step step_4 <?php echo $class_initial[4];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[4];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>4. Black Chili</strong>
			                        </p>
									<p class="title_domanda">Ha già sentito parlare di Black Chili. Ma di che cosa si tratta? Quale delle seguenti definizioni è corretta?</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span></span>
										<span class="active"></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_4" data-risposta="1" value="Uno pneumatico che più si consuma più sa di peperoncino">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Uno pneumatico che più si consuma più sa di peperoncino</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_4"  data-risposta="2" value="Una mescola rivoluzionaria con un grip, una velocità e una durevolezza imbattibili, che si traducono in una tenuta di strada incredibilmente affidabile">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Una mescola rivoluzionaria con un grip, una velocità e una durevolezza imbattibili, che si traducono in una tenuta di strada incredibilmente affidabile</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_4"  data-risposta="3" value="Un disegno del battistrada a forma di peperoncino che consente un maggiore drenaggio dell’acqua in caso di pioggia">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Un disegno del battistrada a forma di peperoncino che consente un maggiore drenaggio dell’acqua in caso di pioggia</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Peccato, risposta errata! Black Chili è una rivoluzionaria mescola del battistrada. È caratterizzata da una presa, una velocità e una durevolezza imbattibili, che si traducono in una tenuta di strada sicura. Montati in una bicicletta, gli pneumatici con Black Chili danno ai ciclisti un'eccellente affidabilità. Se montati su un'automobile danno la massima presa in accelerazione, in curva e in frenata su strade bagnate o asciutte.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Complimenti, risposta esatta! Black Chili è una rivoluzionaria mescola del battistrada. È caratterizzata da una presa, una velocità e una durevolezza imbattibili, che si traducono in una tenuta di strada sicura. Montati in una bicicletta, gli pneumatici con Black Chili danno ai ciclisti un'eccellente affidabilità. Se montati su un'automobile danno la massima presa in accelerazione, in curva e in frenata su strade bagnate o asciutte.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
		                <?php //} ?>
						<?php //if($step_initial <= 5){ ?>
<!---------------------- STEP 5 -->	
						<div class="step step_5 <?php echo $class_initial[5];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[5];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>5. Lo Pneumatico e la strada</strong>
			                        </p>
									<p class="title_domanda">Ogni volta che ci mettiamo alla guida bisognerebbe sempre ricordarsi che l’unico punto di contatto tra il veicolo e la strada è lo pneumatico. Per questo è uno degli elementi più importanti in termini di sicurezza. Ma cosa bisogna fare affinché sia garantita la massima sicurezza?</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span class="active"></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_5" data-risposta="1" value="Scegliere pneumatici di alta qualità, controllarli costantemente, affidarsi a tecnici professionisti per il montaggio e smontaggio ed effettuare una manutenzione regolare">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Scegliere pneumatici di alta qualità, controllarli costantemente, affidarsi a tecnici professionisti per il montaggio e smontaggio ed effettuare una manutenzione regolare</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_5"  data-risposta="2" value="Controllare lo stato degli pneumatici, ripararli in modo autonomo, sostituire anche un solo pneumatico per volta se necessario">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Controllare lo stato degli pneumatici, ripararli in modo autonomo, sostituire anche un solo pneumatico per volta se necessario</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_5"  data-risposta="3" value="Avere solo ed esclusivamente uno pneumatico di scorta sempre nel bagagliaio">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Avere solo ed esclusivamente uno pneumatico di scorta sempre nel bagagliaio</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Peccato, risposta errata! Ecco cosa bisogna fare per garantire la massima sicurezza su strada: scegliere pneumatici di alta qualità, controllarli costantemente, affidarsi solo a tecnici professionisti per il montaggio e smontaggio ed effettuare una manutenzione regolare.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Complimenti, risposta esatta! La manutenzione degli pneumatici è di fondamentale importanza per garantire la sicurezza su strada. Per questo è indispensabile scegliere pneumatici di alta qualità, controllarli costantemente, affidarsi solo a tecnici professionisti per il montaggio e smontaggio ed effettuare una manutenzione regolare.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
		                <?php //} ?>
						<?php //if($step_initial <= 6){ ?>
<!---------------------- STEP 6 -->	
						<div class="step step_6 <?php echo $class_initial[6];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[6];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>6. Guida nella nebbia</strong>
			                        </p>
									<p class="title_domanda">Con la nebbia diventano più difficili sia la visibilità attiva e passiva, sia la valutazione della differenza di velocità con il veicolo che precede. Per questo in caso di nebbia, la vostra incolumità è condizionata non solo dal vostro comportamento, ma soprattutto dal comportamento (e dagli errori) degli altri. Cosa bisogna fare in caso di nebbia fitta?</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span class="active"></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_6" data-risposta="1" value="Diminuire la velocità e rendersi visibili">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Diminuire la velocità e rendersi visibili</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_6"  data-risposta="2" value="Suonare il clacson ripetutamente">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Suonare il clacson ripetutamente</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_6"  data-risposta="3" value="Tenere gli abbaglianti accesi">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Tenere gli abbaglianti accesi</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Peccato, risposta errata! Il consiglio fondamentale è: diminuire la velocità e rendersi visibili. La velocità deve mantenersi nei limiti imposti dalla visibilità di oggetti non emettitori di luce. Occorre mettersi nella condizione di percepire in tempo la presenza di ostacoli e di poter arrestare il veicolo. Su alcune autostrade sono posti dei cartelli che segnalano la velocità da non superare in base alla percezione di visibilità da parte del guidatore di alcuni indicatori appositamente dipinti sull’asfalto.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Complimenti, risposta esatta! Il consiglio fondamentale è: diminuire la velocità e rendersi visibili. La velocità deve mantenersi nei limiti imposti dalla visibilità di oggetti non emettitori di luce. Occorre mettersi nella condizione di percepire in tempo la presenza di ostacoli e di poter arrestare il veicolo. Su alcune autostrade sono posti dei cartelli che segnalano la velocità da non superare in base alla percezione di visibilità da parte del guidatore di alcuni indicatori appositamente dipinti sull’asfalto.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
						<?php //} ?>
						<?php //if($step_initial <= 7){ ?>
<!---------------------- STEP 7 -->	
						<div class="step step_7 <?php echo $class_initial[7];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[7];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>7. Aquaplaning</strong>
			                        </p>
									<p class="title_domanda">Se si sta viaggiando in auto, in caso di pioggia, cosa bisogna fare per evitare possibili problemi e fenomeni di aquaplaning?</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span class="active"></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_7" data-risposta="1" value="Mantenere una velocità massima di 110 km/h in autostrada e di 90 km/h sulle strade extraurbane principali, evitando brusche accelerazioni, decelerazioni e improvvise sterzate.">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Mantenere una velocità massima di 110 km/h in autostrada e di 90 km/h sulle strade extraurbane principali, evitando brusche accelerazioni, decelerazioni e improvvise sterzate.</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_7"  data-risposta="2" value="Aumentare la velocità di marcia per planare sulla pozzanghera e superarla prima">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Aumentare la velocità di marcia per planare sulla pozzanghera e superarla prima</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_7"  data-risposta="3" value="Mantenere una velocità ridotta con una marcia alta (30km/h con la quinta marcia)">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Mantenere una velocità ridotta con una marcia alta (30km/h con la quinta marcia)</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Peccato, risposta errata! Per evitare il fenomeno di acquaplaning è necessario: guidare con attenzione mantenendo una velocità moderata, evitando brusche accelerazioni, decelerazioni e improvvise sterzate. Rispettare i limiti di velocità ridotti a 110 km/h sulle autostrade e 90 km/h sulle strade extraurbane principali. Prestare attenzione alle pozzanghere o ai tratti di strada allagati: una velocità eccessiva implica la certezza della assoluta ingovernabilità del veicolo. 
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Complimenti, risposta esatta! Infatti, per evitare il fenomeno di acquaplaning è necessario: guidare con attenzione mantenendo una velocità moderata, evitando brusche accelerazioni, decelerazioni e improvvise sterzate. Rispettare i limiti di velocità ridotti a 110 km/h sulle autostrade e 90 km/h sulle strade extraurbane principali. Prestare attenzione alle pozzanghere o ai tratti di strada allagati: una velocità eccessiva implica la certezza della assoluta ingovernabilità del veicolo. 
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
						<?php //} ?>
						<?php //if($step_initial <= 8){ ?>
<!---------------------- STEP 8 -->	
						<div class="step step_8 <?php echo $class_initial[8];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[8];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>8. Sosta di emergenza</strong>
			                        </p>
									<p class="title_domanda">Cosa fare in caso di sosta di emergenza?</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span class="active"></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_8" data-risposta="1" value="Rendersi visibili, fermarsi in sicurezza fuori della carreggiata, indossare il giubbotto (o le bretelle ad alta visibilità), uscire dal veicolo disponendosi sul margine destro della carreggiata, rendere visibile il veicolo segnalando l'ingombro con luci di emergenza o posizione e mettendo il triangolo ad almeno 50 mt dalla parte posteriore del veicolo">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Rendersi visibili, fermarsi in sicurezza fuori della carreggiata, indossare il giubbotto (o le bretelle ad alta visibilità), uscire dal veicolo disponendosi sul margine destro della carreggiata, rendere visibile il veicolo segnalando l'ingombro con luci di emergenza o posizione e mettendo il triangolo ad almeno 50 mt dalla parte posteriore del veicolo</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_8"  data-risposta="2" value="Suonare il clacson per farsi notare, rimanere all’interno del veicolo e chiamare i soccorsi">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Suonare il clacson per farsi notare, rimanere all’interno del veicolo e chiamare i soccorsi</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_8"  data-risposta="3" value="Fermarsi immediatamente, scendere dal veicolo posizionandosi al centro della carreggiata per segnalare la necessità di soccorso">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Fermarsi immediatamente, scendere dal veicolo posizionandosi al centro della carreggiata per segnalare la necessità di soccorso</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Peccato, risposta errata! In caso di sosta di emergenza bisogna fermarsi in condizione di sicurezza, fuori della carreggiata, segnalando il veicolo con il triangolo, di giorno, e con le luci di posizione o di emergenza, di notte. Se queste non funzionano, collocare il triangolo ad almeno 50 mt dalla parte posteriore del veicolo. Anche chi esegue operazioni di emergenza deve rendersi visibile indossando il giubbotto o le bretelle ad alta visibilità. È inoltre opportuno che tutti i passeggeri escano dal veicolo e si dispongano sul margine destro della strada, per evitare tamponamenti. 
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Complimenti, risposta esatta! In caso di sosta di emergenza bisogna fermarsi in condizione di sicurezza, fuori della carreggiata, segnalando il veicolo con il triangolo, di giorno, e con le luci di posizione o di emergenza, di notte. Se queste non funzionano, collocare il triangolo ad almeno 50 mt dalla parte posteriore del veicolo. Anche chi esegue operazioni di emergenza deve rendersi visibile indossando il giubbotto o le bretelle ad alta visibilità. È inoltre opportuno che tutti i passeggeri escano dal veicolo e si dispongano sul margine destro della strada, per evitare tamponamenti.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
						<?php //} ?>
						<?php //if($step_initial <= 9){ ?>
<!---------------------- STEP 9 -->		
						<div class="step step_9 <?php echo $class_initial[9];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[9];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>9. Distanza di sicurezza</strong>
			                        </p>
									<p class="title_domanda">Quali sono i principali fattori da tenere in considerazione nella valutazione della distanza di sicurezza da mantenere rispetto al veicolo che precede?</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span class="active"></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_9" data-risposta="1" value="Esperienza di guida e prontezza di riflessi del conducente">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Esperienza di guida e prontezza di riflessi del conducente</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_9"  data-risposta="2" value="La prontezza dei riflessi del conducente, il tipo e lo stato di efficienza del veicolo, la velocità, la visibilità, le condizioni atmosferiche, le condizioni del traffico, la pendenza della strada, le caratteristiche e condizioni del manto stradale, l'entità del carico.">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">La prontezza dei riflessi del conducente, il tipo e lo stato di efficienza del veicolo, la velocità, la visibilità, le condizioni atmosferiche, le condizioni del traffico, la pendenza della strada, le caratteristiche e condizioni del manto stradale, l'entità del carico.</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_9"  data-risposta="3" value="Esperienza di guida dopo anni passati ad esercitarsi su simulatori virtuali">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Esperienza di guida dopo anni passati ad esercitarsi su simulatori virtuali</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Peccato, risposta errata! La distanza di sicurezza è la distanza che ogni veicolo deve mantenere da quello che lo precede, per potersi arrestare, quando necessario, senza tamponarlo. Nella valutazione della distanza di sicurezza è importante tenere in conto alcuni fattori: la prontezza dei riflessi del conducente; il tipo e lo stato di efficienza del veicolo; la velocità; la visibilità e le condizioni atmosferiche; il traffico; la pendenza della strada; le caratteristiche e condizioni del manto stradale e l'entità del carico.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Complimenti, risposta esatta! La distanza di sicurezza è la distanza che ogni veicolo deve mantenere da quello che lo precede, per potersi arrestare, quando necessario, senza tamponarlo. Nella valutazione della distanza di sicurezza è importante tenere in conto alcuni fattori: la prontezza dei riflessi del conducente; il tipo e lo stato di efficienza del veicolo; la velocità; la visibilità e le condizioni atmosferiche; il traffico; la pendenza della strada; le caratteristiche e condizioni del manto stradale e l'entità del carico.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
		                <?php //} ?>
						<?php //if($step_initial <= 10){ ?>	
<!---------------------- STEP 10 -->	
						<div class="step step_10 <?php echo $class_initial[10];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[10];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>10. E quale deve essere la distanza di sicurezza corretta?</strong>
			                        </p>
									<p class="title_domanda">Di seguito alcuni esempi di distanza di sicurezza. Quali di questi sono corretti?</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span class="active"></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_10" data-risposta="1" value="Velocità 50 km/h distanza minima 25m">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Velocità 50 km/h distanza minima 25m</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_10"  data-risposta="2" value="Velocità 70 km/h distanza minima 10m">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Velocità 70 km/h distanza minima 10m</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_10"  data-risposta="3" value="Velocità 100 km/h distanza minima 30m">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Velocità 100 km/h distanza minima 30m</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Peccato, risposta errata! Ecco un aiutino per aiutarti a ricordare meglio: una formula per calcolare approssimativamente una buona distanza di sicurezza è dividere la propria velocità espressa in km/h per 10 ed elevare il risultato al quadrato; il numero risultante è un buon indicatore, in metri, della distanza di sicurezza da mantenere.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Complimenti, risposta esatta! Una semplice formula da ricordare per calcolare approssimativamente una buona distanza di sicurezza è la seguente: dividere la propria velocità espressa in km/h per 10 ed elevare il risultato al quadrato; il numero risultante è un buon indicatore, in metri, della distanza di sicurezza da mantenere.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
		                <?php //} ?>
						<?php //if($step_initial <= 11){ ?>
<!---------------------- STEP 11 -->	
						<div class="step step_11 <?php echo $class_initial[11];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[11];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>11. ATTITUDINALE 1</strong>
			                        </p>
									<p class="title_domanda">Abbiamo quasi finito, ci serve solo qualche altra risposta.<br> Preferisci le situazioni:</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span class="active"></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_11" data-risposta="1" value="del tutto organizzate, che non richiedano alcun intervento successivo">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">del tutto organizzate, che non richiedano alcun intervento successivo</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_11"  data-risposta="2" value="da organizzare con precise direttive preliminari">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">da organizzare con precise direttive preliminari</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_11"  data-risposta="3" value="da organizzare  e da gestire in corso d’opera con sostanziale autonomia">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">da organizzare  e da gestire in corso d’opera con sostanziale autonomia</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Perfetto, ancora un ultimo sforzo...</p>
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Perfetto, ancora un ultimo sforzo...</p>
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
		                <?php //} ?>
						<?php //if($step_initial <= 12){ ?>
<!---------------------- STEP 12 -->	
						<div class="step step_12 <?php echo $class_initial[12];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[12];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>12. ATTITUDINALE 2</strong>
			                        </p>
									<p class="title_domanda">In mezzo ad un gruppo di persone che hai conosciuto da poco, tendi a:</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span class="active"></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_12" data-risposta="1" value="proporre le tue idee">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">proporre le tue idee</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_12"  data-risposta="2" value="ascoltare con attenzione le proposte degli altri intervenendo con continuità nella discussione">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">ascoltare con attenzione le proposte degli altri intervenendo con continuità nella discussione</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_12"  data-risposta="3" value="ascoltare con attenzione le proposte degli altri intervenendo nella discussione solo quando lo ritieni opportuno">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">ascoltare con attenzione le proposte degli altri intervenendo nella discussione solo quando lo ritieni opportuno</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Siamo arrivati al traguardo!</p>
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Siamo arrivati al traguardo!</p>
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>
		                <?php //} ?>
						<?php //if($step_initial <= 13){ ?>
<!---------------------- STEP 13 -->	
						<div class="step_social step step_13 <?php echo $class_initial[13];?> ">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question <?php echo $class_initial[13];?> ">
								<div class="domanda">
			                        <p class="gf-m-r black text28">
			                            <strong>Vuoi farci sapere qualcosa in più su di te?</strong>
			                        </p>
									<p class="title_domanda">Puoi, se lo ritieni, farci sapere i tuoi profili Facebook o Instagram.<br>Inserisci qui il tuo nome utente:</p>
									<div class="steps_bar clearfix">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span class="active"></span>
									</div>
									<div>
										<div class="social_url">
											<div class="prev"><strong>www.facebook.com/</strong></div>
											<input type="text" id="register_facebook" placeholder="inserisci il tuo nome utente" value="" />
										</div>
										<div class="social_url">
											<div class="prev"><strong>www.instagram.com/</strong></div>
											<input type="text" id="register_instagram" placeholder="inserisci il tuo nome utente" value="" />
										</div>
										<div class="inner-divider-news-half"></div>
										<div>
											<label class="checkboxWrap -overlay disabled" data-check="c1">*Dichiaro di aver letto l'integrazione del 23/07/19 all'informativa sulla selezione
												<input type="checkbox" id="register_terms" disabled value="0" />
												<span class="checkmark"></span>
											</label>
											<label class="checkboxWrap disabled" for="register_policy1">*Acconsento al trattamento dei dati per la partecipazione alla selezione, ai fini indicati nella integrazione del 23/07/2019 dell'informativa, da parte di Continental Italia S.p.A. e IC Digital S.r.l.
												<input type="checkbox" id="register_policy1" id="register_policy1" disabled value="0" />
												<span class="checkmark"></span>
											</label>
										</div>
										<div class="inner-divider-news-half"></div>
										<p>
											Se non ti ricordi il tuo nome utente in Facebook o Instagram:<br>
											- Accedi al tuo profilo dal browser (non dall'app)<br>
											- Fai login<br>
											- Vai nella tua pagina profilo: nella barra dell'indirizzo trovi il tuo nome utente
										</p>
										<div class="inner-divider-news-half"></div>	
										<div class="text-center mt-30"><a id="btn_finish" class="custom-button fadeIn-element">Continua</a></div>									
										<div class="inner-divider-news"></div> 
					                </div>
					            </div>
							</div>
		                </div>
		                <?php //} ?>
						<?php //if($step_initial <= 14){ ?>
<!---------------------- STEP 14 -->	
						<div class="step step_14 <?php echo $class_initial[14];?> ">
							<div class="inner-divider-news"></div>
							<div class="mb-30">
								<p class="gf-m-r orange text28">
		                        	Il questionario è completato.
		                        </p>
		                        <p class="gf-m-r">
		                            Hai già scelto chi sarà il tuo copilota?<br>
		                            Con lui/lei, se sarai selezionato, parteciperai alla ultima fase di preparazione alla Black Chili Driving Experience, con una sessione di prove su pista nel mese di settembre.<br>
		                            Buona fortuna.<br><br>

		                            Se vuoi maggiori informazioni su Continental e i suoi prodotti:<br><br>
		                            <a class="custom-button fadeIn-element" href="https://www.continental-pneumatici.it/auto" target="_black">Scopri di più</a>
		                        </p>
		                    </div>
		                </div>
		                <?php //} ?>
		            </div> 
		            <div class="inner-divider-news"></div>  
				</div>
			</div>
		</div>
    </section>
    

	<section class="wrapper_card inner-spacer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div style="margin:30px 0;">
                    	<a class="custom-button fadeIn-element" href="<?php bloginfo('url'); ?>">Torna alla home</a>
                    </div>
				</div>
			</div>
		</div>
    </section>
	<?php endwhile; endif; ?>

	<div class="form__lightbox_wrapper"></div>
	<div class="form__lightbox" id="c1">

        <div class="form__lightbox--close">X</div>

        <div class="form__lightbox--content">
            
            <p><em class="small" style="color: #f1972a;">Leggi e scorri fino in fondo per cliccare sul pulsate di conferma</em></p>
            
            <h3 align="center">INFORMATIVA PER LA PRIVACY</h3>
            <h3 align="center">Informativa resa ai sensi degli artt.13 e 14 del Regolamento (UE) n.679/2016 (GDPR)</h3><br>
            
            <p>Benvenuto dallo staff della IC Digital S.r.l. e di Continental Italia S.p.a., i <a href="/wp-content/uploads/2019/07/Accordo_contitolarita.pdf" target="_blank">Contitolari</a> dei tuoi Dati Personali. Come puoi intuire, al fine di svolgere adeguatamente i nostri servizi di informazione, raccogliamo e utilizziamo alcune informazioni che ti riguardano, in conformità con la legge. IC Digital S.r.l. e Continental Italia S.p.a. si impegnano a proteggere e rispettare la tua privacy. La presente Informativa descrive il Tuo Diritto alla Privacy in merito ai tuoi dati che trattiamo e le misure che adottiamo al fine di tutelarli. Sappiamo che si tratta di un documento lungo, ma ti chiediamo cortesemente di leggere questa Informativa molto attentamente. Di seguito troverai un indice che elenca i punti trattati:</p>

			<p><strong>1. Alcuni termini da chiarire</strong></p>
			<p><strong>2. Quali sono i dati personali che le Società raccolgono e trattano?</strong></p> 
			<p><strong>3. Perché utilizziamo dati personali che ti riguardano? Finalità</strong></p>
			<p><strong>4. Che cosa prevede la legge in merito?</strong></p>
			<p><strong>5. Protezione dei dati conservati</strong></p>
			<p><strong>6. Condivisione in social network</strong></p>
			<p><strong>7. Sei tenuto/a fornirci i dati personali che richiediamo?</strong></p>
			<p><strong>8. Trattiamo i tuoi dati personali senza alcun intervento umano?</strong></p>
			<p><strong>9. Per quanto tempo conserviamo i tuoi dati personali?</strong></p>
			<p><strong>10. Trasferiamo i tuoi dati a terzi o al di fuori dell’Unione Europea?</strong></p>
			<p><strong>11. Quali sono i tuoi diritti?</strong></p>
			<p><strong>12. Come puoi contattarci?</strong></p>
			<p><strong>13. Come gestiamo le modifiche apportate alla presente Informativa?</strong></p>


			<p>
				*<br>
				<strong>1. Alcuni termini da chiarire</strong><br>
				Per prima cosa è necessario essere chiari sul significato che alcune parole rivestono all’interno della presente Informativa. Può sembrare ovvio, ma all’interno di questa Informativa sarai indicato/a come “Interessato”. In questa Informativa utilizziamo il termine "Dati Personali" per qualificare le informazioni che tu, come persona fisica, ci trasmetti e di cui ci fornisci il consenso al trattamento.<br>
				Quando parliamo di "noi" o delle "Società" facciamo riferimento a IC Digital S.r.l. e Continental Italia S.p.a. La sede legale di IC Digital S.r.l. (di seguito anche menzionata come “IC” si trova in C.so di Porta Nuova, 11 cap 20121, Milano, la sede legale di Continental Italia Spa (di seguito anche menzionata come CONTI_ITA) si trova in Via Giovanni Gioacchino Winckelmann, 1, 20146 Milano.<br> 
				IC conduce, su mandato di CONTI_ITA, un’attività di comunicazione volta a raccogliere “candidati” (tu in qualità di “Interessato”)  da selezionare per una collaborazione agli eventi “BLACK CHILI DRIVE EXPERIENCE” (di seguito menzionata come la “Selezione”) come indicato nell’informativa dell’iniziativa (<a href="/wp-content/uploads/2019/07/TERMINI-E-CONDIZIONI-BLACKCHILI_def.pdf" target="_blank">TERMS AND CONDITIONS</a>).
			</p>

			<p>
				<strong>2. Quali sono i dati personali che le Società raccolgono e trattano?</strong><br>
				I dati personali che le Società utilizzano includono, a titolo esemplificato ma non esaustivo:<br>
				A. Nome, data e luogo di nascita, dati personali di contatto e qualifiche, indirizzo e-mail;<br>
				B. La tua conoscenza delle tematiche di guida di automobili in sicurezza, ottenuta mediante la volontaria compilazione di questionario online.<br>
				IC tratterà i tuoi dati nell’ambito dell’iniziativa di “Selezione” sopra menzionata per valutare i candidati e sottoporli a CONTI_ITA per addivenire ad una scelta che porti alla formulazione di un Contratto di collaborazione occasionale ex Art. 2222 c.c con IC.<br> 
				Qualora sia tu a contattarci, i <em></em> dati personali saranno utilizzati esclusivamente per elaborare le richieste di informazioni nel contesto del consenso concesso o in conformità alle normative applicabili in materia di protezione dei dati. Potremmo in questo caso anche tenere traccia delle tue richieste tra cui ad esempio, non esaustivo, la data e l'ora della visita, le pagine visitate, l’uso del cursore sul sito, le funzioni della pagina utilizzate e il sito web da cui l'utente accede al sito della Selezione. L'indirizzo IP viene trasmesso in modo anonimo ed è utilizzato esclusivamente per assegnare una posizione geografica. IC utilizza tali informazioni per misurare l'attività del sito web, per produrre statistiche e per migliorare i servizi e le informazioni fornite tramite il sito. Troverai la cookie policy al seguente <a href="/wp-content/uploads/2019/04/COOKIE-POLICY.pdf" target="_blank">link</a><br><br>

				<strong>2bis. Integrazione del 23.07.2019.</strong><br>
				I dati personali che le società raccolgono includono da oggi l'indirizzo pubblico del profilo personale del candidato sui seguenti social network:<br>
				a. www.facebook.com<br>
				b. www.instagram.com<br>
				Tale raccolta prevede l’inserimento della sola URL pubblica del profilo personale del candidato, nel modulo in cui è presentata la presente integrazione all’informativa. Non può e non deve includere alcuna informazione ulteriore.<br>
				La raccolta e il trattamento avvengono ai soli fini della visualizzazione dei contenuti resi pubblici dal candidato in sintonia con i fini espressi al punto 3.<br>
				Le società proprietarie dei social network sopra menzionati non sono coinvolte né interessate in alcun modo nella presente raccolta, trattamento ed iniziativa in generale.
			</p>

			<p>
				<strong>3. Perché utilizziamo dati personali che ti riguardano? Finalità</strong><br>
				IC e Continental raccolgono e trattano i tuoi “dati personali”:<br>
				A. per inviarti comunicazioni riguardanti la “Selezione”<br> 
				B. CONTI_ITA per inviarti, solo con tuo consenso, comunicazioni riguardanti i propri prodotti e promozioni commerciali<br>
				C. Per inviarti altre iniziative di selezione ed eventi<br> 
				D. Senza il tuo consenso per ottemperare a tutti gli obblighi di legge;<br>
				Nel corso della seconda fase della Selezione, i Contitolari potranno chiedere ulteriori dati personali e particolari per i quali ti forniranno una specifica informativa e per i quali richiederanno un tuo consenso esplicito.<br>
				Nel corso della seconda fase della Selezione, i Contitolari potranno chiedere ulteriori dati personali e particolari (quali la visibilità delle pagine pubbliche del tuo profilo Facebook, delle tue foto pubbliche e post pubblici) per i quali ti forniranno una specifica informativa e per i quali richiederanno un tuo consenso esplicito. La condivisione di questi dati non è obbligatoria in fase di selezione ma lo sarà nella seconda fase del processo. In questa occasione, infatti, sarà proposta una nuova specifica informativa di tutela dei dati personali e particolari conferiti volontariamente.
			</p>

			<p>
				<strong>4. Che cosa prevede la legge in merito?</strong><br>
			La legge prevede che l’informativa per il trattamento dei tuoi dati illustri le finalità e le modalità di trattamento.
			</p> 

			<p>
				<strong>5. Protezione dei dati conservati</strong><br>
			Le nostre società utilizzano misure di sicurezza tecniche e organizzative per proteggere i dati personali da voi forniti contro la manipolazione, perdita, distruzione o dall'accesso da parte di persone non autorizzate. Le misure di sicurezza vengono costantemente migliorate e adattate secondo le tecnologie più recenti. I dati forniti che non sono crittografati potrebbero essere potenzialmente visualizzati da terzi. Per questo motivo ci preme far notare che, per quanto riguarda la trasmissione di dati su Internet (ad esempio tramite e-mail oppure mediante link a siti terzi e social), non può essere garantito un trasferimento sicuro. I dati sensibili non devono pertanto essere trasmessi in alcun modo o devono essere trasmessi esclusivamente tramite una connessione sicura (SSL). Se si accede a pagine e file e viene richiesto di inserire dati personali, si prega di notare che la trasmissione di tali dati tramite Internet potrebbe non essere sicura e che vi è un rischio che tali dati possano essere visualizzati e manipolati da persone non autorizzate.
			</p>

			<p>
				<strong>6. Condivisione in social network</strong><br>
			Nel caso in cui desideri autenticarti al sito coi social, desideriamo informarti che non sarà trasferito alcun dato personale al rispettivo servizio di social media. Nel caso in cui tu voglia utilizzare il tuo profilo per condividere i contenuti del nostro sito web, ti informiamo che non abbiamo alcun controllo sui contenuti e sulla portata dei dati raccolti da tali reti. A tale riguardo, si applicano le condizioni di utilizzo e le politiche sulla privacy delle rispettive reti sociali, pertanto, decliniamo qualsiasi responsabilità per tali contenuti. Il fornitore o l'operatore della pagina è sempre responsabile del contenuto delle pagine collegate. Le pagine collegate sono state controllate al fine di rilevare eventuali violazioni di legge al momento della creazione dei link. Il contenuto illecito non è stato rilevato al momento della creazione dei link. Tuttavia, la verifica continua dei contenuti delle pagine collegate non è ragionevole senza una concreta indicazione di una violazione di legge. Rimuoveremo immediatamente i link pertinenti se questi dovessero violare qualsiasi legge.
			</p>

			<p>
				<strong>7. Sei tenuto/a fornirci i dati personali che richiediamo?</strong><br>
			Teniamo a precisare che il conferimento dei dati di contatto è necessario alla partecipazione della selezione, pertanto qualora incompleti o inesatti l’interessato non potrà accedere alla seconda fase di selezione.
			</p> 

			<p>
				<strong>8. Trattiamo i tuoi dati personali senza alcun intervento umano?</strong><br>
			Si, a volte lo facciamo. Le Società utilizzano sistemi/processi automatizzati e una procedura decisionale altresì automatizzata (come la profilazione) per comunicare con te nell’ambito dei trattamenti illustrati in questa informativa. A tale proposito ti informiamo che i tuoi dati personali   saranno quindi trattati mediante procedure informatiche e cartacee.
			</p>

			<p>
				<strong>9. Per quanto tempo conserviamo i tuoi dati personali?</strong><br>
			Registrandoti alla Selezione tu, in qualità di interessato, presti il tuo esplicito consenso a che IC e Continental conservino i tuoi dati personali per tutta la durata dell’evento e, ove necessario, li trasferiscano ai partner di cooperazione (come a titolo esemplificativo ma non esaustivo, alla compagnia assicuratrice , che ha anche il diritto di memorizzare i dati personali nei termini consentiti dalla legislazione vigente), nella misura in cui la conservazione e il trasferimento di tali dati siano necessari per condurre la selezione e l’evento BLACK CHILI EXPERIENCE.<br> 
			I tuoi dati personali saranno conservati dalle Società per un periodo massimo di 30 mesi dalla registrazione. In seguito essi saranno cancellati in conformità alle normative vigenti. Salvo che i partecipanti non confermino espressamente altrimenti, le informazioni fornite come parte della presente Selezione saranno utilizzate esclusivamente per lo svolgimento della stessa e non saranno divulgate a terzi senza l'espressa autorizzazione dei partecipanti.
			</p>

			<p>
				<strong>10. Trasferiamo i tuoi dati personali a terzi o al di fuori dell’Unione Europea?</strong><br>
			I tuoi dati sono ospitati su server di proprietà di IC collocati presso SEEWEB S.p.a. in via Caldera, 1 Milano e possono essere trasferiti sia su sistemi interni alle sedi operative dei contitolari sia trasferiti alle società del gruppo Continental Italia S.p.a.nel rispetto del GDPR.
			</p>

			<p>
				<strong>11. Quali sono i tuoi diritti?</strong><br>
			Si prega di notare che, in qualità di interessati ai sensi del Regolamento generale sulla protezione dei dati dell’UE (GDPR), hai i seguenti diritti in relazione al trattamento dei tuoi dati personali:<br>
			- Diritti d'informazione ai sensi dell'articolo 13 e dell'articolo 14 del GDPR <br>
			- Diritto di accesso ai sensi dell'articolo 15 del GDPR<br>
			- Diritto di rettifica ai sensi dell'articolo 16 del GDPR<br>
			- Diritto alla cancellazione ai sensi dell'articolo 17 del GDPR<br>
			- Diritto di limitazione di trattamento ai sensi dell'articolo 18 del GDPR<br>
			- Diritto alla portabilità dei dati ai sensi dell’articolo 20 del GDPR<br>
			Ai sensi dell'articolo 7 (3) del GDPR, hai anche il diritto di revocare il tuo consenso alla raccolta, al trattamento/conservazione e all'utilizzo dei dati personali in qualsiasi momento. Il consenso deve essere ritirato scrivendo a IC Digital srl C.so di Porta Nuova, 11 cap 20121, Milano o inviando un’e-mail a <a href="mailto:privacy@ic-digital.com">privacy@ic-digital.com</a><br>
			Quando invii una e-mail per esercitare i tuoi diritti, la Società potrebbe chiederti di identificarti prima di procedere con la tua richiesta. Infine, hai il diritto di presentare reclamo presso l’Autorità Garante della privacy del Paese in cui vivi o lavori oppure del luogo in cui credi si sia generato un problema legato ai tuoi dati.
			</p>

			<p><strong>12. Come puoi contattarci?</strong><br>
			In caso desideri porre domande o reclami, potrai sempre contattare i nostri responsabili della protezione dei dati per iscritto (anche tramite e-mail). E-mail: <a href="mailto:dpo@ic-digital.com">dpo@ic-digital.com</a>.
			</p> 

			<p><strong>13. Come gestiamo le modifiche apportate alla presente Informativa?</strong><br>
			I termini indicati nella presente Informativa potrebbero essere di volta in volta modificati. Laddove questo fosse il caso, sarà nostra premura pubblicare qualsiasi modifica rilevante apportata alla presente Informativa mediante apposita comunicazione sul nostro sito internet/Portale, tramite Newsletter oppure mettendoci direttamente in contatto con te tramite altri canali di comunicazione disponibili.<br>
			Continental Italia S.p.A.<br>
			IC Digital Srl
			</p>
			<br>

            <div class="e-button confirm_check">Confermo di aver letto e accettatato il contenuto dell’informativa</div>
        </div>

    </div>


<script>window.step_initial = <?=$step_initial?>;</script>
<?php get_footer(); ?>