<?php 
	/* Template Name: Black chili */
	if(isset($_GET['is'])){
		$imageToShare = $_GET['is'];
?>
<html>
<head>
  <title>Partecipa ora</title>
  <meta property="fb:app_id"          content="2280950928823676" />
  <meta property="og:url"           content="https://www.sponsoroftheroad.com/black-chili-driving-experience/?is=<?=$imageToShare?>" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Partecipa ora" />
  <meta property="og:description"   content="https://www.sponsoroftheroad.com/black-chili-driving-experience/" />
  <meta property="og:image"         content="https://www.sponsoroftheroad.com/wp-content/themes/gripline/public/images/black-chili/badge/targhe-share/<?=$imageToShare?>.jpg" />
  <meta property="og:image:width"   content="1200" />
  <meta property="og:image:height"   content="900" />
</head>
<body></body>
<?php } else {
	get_header(); 
	setlocale(LC_TIME, 'it_IT.UTF8');
?>
	<div id="home"></div>
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>


    <section class="upper-page section-dark screen">
		<?php $url_bg_image = get_the_post_thumbnail_url($post->ID, 'full'); ?>
        <div class="hero-fullscreen overlay overlay-dark-15">
			<div class="hero-fullscreen-FIX">
                <div class="hero-bg bg-img-SINGLE" style="background-image: url(<?= $url_bg_image; ?>);"></div>
            </div>
        </div>
    </section>

  
	<section class="wrapper_card inner-spacer wrapper_art">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="inner-divider-news"></div>
					<div class="the-overline <?php if($category_id == 4){ ?> black <?php } ?>"></div>
					<div class="inner-divider-news-half"></div>
					<h1 class="post-all-heading"><?=the_title(false)?></h1>
					<div class="steplist">



<!---------------------- STEP 1 -->	
						<div class="step step_1 active">
							<div class="inner-divider-news"></div>
							<div class="content">
								<p class="title">
<strong>Con il suo mix di gomma naturale, polimeri artificiali e particelle di carbone, Black Chili ha tutto quel che serve per affrontare in sicurezza ogni strada, con ogni clima.<br><br>
E tu ce l’hai?<br>
Partecipa alle selezioni della Black Chili Driving Experience: 7 top car con allestimento sportivo, 14 piloti e 4 giorni di tour per le strade più suggestive del sud della Francia. Un’occasione per testare l’adrenalina della guida sportiva con la sicurezza degli pneumatici a mescola Black Chili. Metti alla prova la tua passione per la guida in tre domande, scegli la targa personalizzata per te e il tuo co-pilota e preparati a vivere un’esperienza di guida targata Continental.</strong>
								</p>
							</div>
							<div class="inner-divider-news-half"></div>
							<div style="margin:30px 0;">
		                    	<button class="custom-button fadeIn-element" id="btn_partecipa_ora">Partecipa ora</button>
		                    	<div class="legal">
		                    		<a class="fadeIn-element" href="<?php bloginfo('url'); ?>/wp-content/uploads/2019/07/informativa-blackchili.pdf" target="_blank">Informativa</a>
		                    		<a class="fadeIn-element" href="<?php bloginfo('url'); ?>/wp-content/uploads/2019/07/TERMINI-E-CONDIZIONI-BLACKCHILI_def.pdf" target="_blank">Termini e condizioni</a>
		                    	</div>
		                    </div>
		                </div>



<!---------------------- STEP 2 -->	
						<div class="step step_2">
							<div class="inner-divider-news"></div>
							<div class="content">
								<p class="title">
									<strong>Registrati per iniziare il test, creare la tua targa e partecipare alle selezioni.<br>
									Buona fortuna.</strong>
								</p>
							</div>
							<div class="inner-divider-news-half"></div>
							<div class="signin signin_1 active">
								<div class="col">
									<button class="facebook-button fadeIn-element" id="btn_login_facebook"><span class="ion-social-facebook"></span>Continua con Facebook</button>
								</div>
								<div class="col">
									oppure
								</div>
								<div class="col input-prepend">
									<form id="formPreRegister">
										<input type="email" name="preRegister--email" placeholder="Inserisci l'indirizzo e-mail*" required />
										<input type="submit" value="" class="icon-mail" style="width:auto;" />
									</form>
								</div>
							</div>
							<div class="signin signin_2">
								<form id="formRegister">
									<input type="text" name="register--nome" placeholder="Nome*" value="" required />
									<input type="email" name="register--email" placeholder="Inserisci l'indirizzo e-mail*" value="" required />
									<div class="inner-divider-news-half"></div>
									<p>
									<div>
										<label class="checkboxWrap">*Dichiaro di aver compiuto 25 anni.
											<input name="register--age" type="checkbox" required />
											<span class="checkmark"></span>
										</label>
										<label class="checkboxWrap -overlay" data-check="c1">*Dichiaro di aver letto l’informativa e il regolamento dell’iniziativa di “selezione”.
											<input type="checkbox" name="register--terms" required />
											<span class="checkmark"></span>
										</label>
										<label class="checkboxWrap -overlay" for="register--policy1" data-check="c2">*Acconsento al trattamento da parte di Continental Italia SpA e IC Digital Srl dei dati per la partecipazione alla “selezione” ai fini indicati nell’informativa e al loro trasferimento ai soggetti indicati per lo specifico trattamento.
											<input type="checkbox" name="register--policy1" id="register--policy1" required />
											<span class="checkmark"></span>
										</label>
										<label class="checkboxWrap" for="register--policy2">Acconsento al trattamento da parte di Continental Italia SpA per ricevere ulteriori comunicazioni promozionali riguardanti propri prodotti ed iniziative e al loro trasferimento ai soggetti indicati per lo specifico trattamento.
											<input type="checkbox" name="register--policy2" id="register--policy2" />
											<span class="checkmark"></span>
										</label>
									</div>
									<div class="inner-divider-news-half"></div>										
									<div class="legal">
			                    		<a class="fadeIn-element" href="<?php bloginfo('url'); ?>/wp-content/uploads/2019/07/informativa-blackchili.pdf" target="_blank">Informativa</a>
		                    			<a class="fadeIn-element" href="<?php bloginfo('url'); ?>/wp-content/uploads/2019/07/TERMINI-E-CONDIZIONI-BLACKCHILI_def.pdf" target="_blank">Termini e condizioni</a>
			                    	</div>
									<div class="inner-divider-news-half"></div>
									<button type="submit" id="btn_conferma_dati" class="the-button the-button-submit fadeIn-element">Conferma</button>
								</form>
								<div class="inner-divider-news"></div> 
			                </div>
		                </div>




<!---------------------- STEP 3 -->	
						<div class="step step_3">
							<div class="inner-divider-news"></div>
							<!--DOMANDA UNO-->
							<div class="slide_question active">
								<div class="domanda">
									<p class="title_domanda">Quale di questi componenti dell’auto può influenzare lo spazio di frenata?</p>
									<div class="steps_bar clearfix">
										<span class="active"></span>
										<span></span>
										<span></span>
									</div>
									<div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_1" data-risposta="1" value="Il tipo di vernice della carrozzeria">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">Il tipo di vernice della carrozzeria</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_1"  data-risposta="2" value="L'aria condizionata">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">L'aria condizionata</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                    <div class="radio">
	                                        <label>
	                                            <input type="radio" class="radio--domanda" name="domanda_1"  data-risposta="3" value="La mescola degli pneumatici">
	                                            <span class="radio-body d-block">
	                                                <span class="radio-bullet"></span>
	                                                <span class="radio-label">La mescola degli pneumatici</span>
	                                            </span>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Domanda scivolosa? Ecco la risposta per rimetterti in carreggiata: gli pneumatici sono l’unico punto di contatto tra auto e strada. Per ridurre lo spazio di frenata serve una mescola ad alta aderenza e dalle alte prestazioni su ogni superficie, come Black Chili.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Questo sì che è Black Chili!
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
							<!--DOMANDA DUE-->
							<div class="slide_question">
								<p class="title_domanda">Quale tra questi è lo pneumatico ideale per affrontare una lunga sessione di guida sotto la pioggia?</p>
								<div class="steps_bar clearfix">
									<span></span>
									<span class="active"></span>
									<span></span>
								</div>
								<div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="radio--domanda" name="domanda_2"  data-risposta="1" value="Pneumatico a camera d’acqua">
                                            <span class="radio-body d-block">
                                                <span class="radio-bullet"></span>
                                                <span class="radio-label">Pneumatico a camera d’acqua</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="radio--domanda" name="domanda_2"  data-risposta="2" value="Pneumatico aderente in accelerazione e in frenata">
                                            <span class="radio-body d-block">
                                                <span class="radio-bullet"></span>
                                                <span class="radio-label">Pneumatico aderente in accelerazione e in frenata</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="radio--domanda" name="domanda_2"  data-risposta="3" value="Nessuno dei precedenti">
                                            <span class="radio-body d-block">
                                                <span class="radio-bullet"></span>
                                                <span class="radio-label">Nessuno dei precedenti</span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Ecco la risposta per non fare un altro buco nell’acqua: un terreno bagnato richiede uno pneumatico in grado di mantenere la massima aderenza in accelerazione e di ridurre lo spazio di frenata. La mescola Black Chili è stata progettata per offrire trazione e aderenza.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Questo sì che è Black Chili!
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
							<!--DOMANDA TRE-->
							<div class="slide_question">
								<p class="title_domanda">La mescola Black Chili offre un incremento su tutti e tre i principali fattori di performance dello pneumatico. Quali sono questi fattori?</p>
								<div class="steps_bar clearfix">
									<span></span>
									<span></span>
									<span class="active"></span>
								</div>
								<div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="radio--domanda" name="domanda_3" data-risposta="1" value="Elasticità, peso e coefficiente di rimbalzo">
                                            <span class="radio-body d-block">
                                                <span class="radio-bullet"></span>
                                                <span class="radio-label">Elasticità, peso e coefficiente di rimbalzo</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="radio--domanda" name="domanda_3" data-risposta="2" value="Colore, profumo e data di scadenza">
                                            <span class="radio-body d-block">
                                                <span class="radio-bullet"></span>
                                                <span class="radio-label">Colore, profumo e data di scadenza</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="radio--domanda" name="domanda_3" data-risposta="3" value="Aderenza, resistenza al rotolamento e spazio di frenata.">
                                            <span class="radio-body d-block">
                                                <span class="radio-bullet"></span>
                                                <span class="radio-label">Aderenza, resistenza al rotolamento e spazio di frenata.</span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="risposta">
                                	<div class="arrow-top"></div>
                                	<div class="risposta--content sbagliato">
                                		<p class="risposta__title">Sbagliato!</p>
                                		Tre è il numero perfetto, ma solo in un caso: i fattori chiave sono aderenza, resistenza al rotolamento e spazio di frenata. Sono strettamente connessi e per questo è importante scegliere una mescola ad alte prestazioni su tutti e tre i fronti.
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                	<div class="risposta--content giusto">
                                		<p class="risposta__title">Esatto!</p>
                                		Questo sì che è Black Chili!
                                		<div class="text-center mt-30"><a class="custom-button fadeIn-element">Continua</a></div>
                                	</div>
                                </div>
							</div>
		                </div>



<!---------------------- STEP 4 -->	
						<div class="step step_4">
							<div class="inner-divider-news"></div>
							
							<!-- 3 risposte esatte --> 
							<p class="title text-center">
								Complimenti! Performance e sicurezza, in perfetto stile Continental.<br>
								Ora non ti resta che scegliere la targa per la tua squadra.
							</p> 
							<p class="title text-center">
								Non scoraggiarti, la guida sicura si impara strada facendo!<br>
								Ora non ti resta che scegliere la targa per la tua squadra. 
							</p>

							<div class="text-center mt-30">
								<button class="custom-button fadeIn-element" id="btn_crea_badge">Crea la tua targa</button>
							</div>
		                </div>



<!---------------------- STEP 5 -->	
						<div class="step step_5">
							<div class="inner-divider-news"></div>
							<p class="mb-30 title text-center">
								Scegli il design della targa e il nome del tuo team per ricevere subito la targa personalizzata Black Chili Driving Experience. 
							</p>
							<div>
								<p class="text-center">Seleziona il nome</p>
								<div class="owl-badge owl-carousel owl-theme" id="slider-badge">
									<div class="item">
										<img src="<?=get_template_directory_uri()?>/public/images/black-chili/badge/nome/A-black_asphalt.jpg" alt="">
									</div>
									<div class="item">
										<img src="<?=get_template_directory_uri()?>/public/images/black-chili/badge/nome/B-road_riders.jpg" alt="">
									</div>
									<div class="item">
										<img src="<?=get_template_directory_uri()?>/public/images/black-chili/badge/nome/C-turbo_grip.jpg" alt="">
									</div>
									<div class="item">
										<img src="<?=get_template_directory_uri()?>/public/images/black-chili/badge/nome/D-chili_racers.jpg" alt="">
									</div>
									<div class="item">
										<img src="<?=get_template_directory_uri()?>/public/images/black-chili/badge/nome/E-tyre_masters.jpg" alt="">
									</div>
									<div class="item">
										<img src="<?=get_template_directory_uri()?>/public/images/black-chili/badge/nome/F-motor_lovers.jpg" alt="">
									</div>
									<div class="item">
										<img src="<?=get_template_directory_uri()?>/public/images/black-chili/badge/nome/G-driving_force.jpg" alt="">
									</div>
								</div>
							</div>
							<div class="inner-divider-news-half"></div>
							<div>
								<p class="text-center">Seleziona la targa</p>
								<div class="owl-badge owl-carousel owl-theme" id="slider-target"></div>
							</div>
							<div class="inner-divider-news-half"></div>
							<div class="text-center">
								<button class="custom-button fadeIn-element" id="btn_to_share">Continua</button>
							</div>
		                </div>



<!---------------------- STEP 6 -->	
						<div class="step step_6">
							<div class="inner-divider-news"></div>
							<p class="mb-30 title text-center">
								Scarica la targa e condividilo con i supporter del tuo team!<br>Ricorda di utilizzare gli hashtag <strong>#roadtoblackchili</strong> e <strong>#bcde2019</strong> per far sentire il rombo della tua squadra!
							</p>
							<div class="badge-share">
								<img src="<?=get_template_directory_uri()?>/public/images/black-chili/badge/targhe-share/1A.jpg" alt="">
							</div>
							<div class="button mt-30 text-center">
								<a href="" target="_blank" class="facebook-button fadeIn-element" id="btn_share_result"><span class="ion-social-facebook"></span>Share</a>
								<a href="" target="_blank" class="default-button fadeIn-element" id="btn_download_result">Scarica</a>
							</div>
							<div class="inner-divider-news-half"></div>
							<div class="text-center">
								<button class="custom-button fadeIn-element" id="btn_finish">Continua</button>
							</div>
		                </div>



<!---------------------- STEP 7 -->	
						<div class="step step_7">
							<div class="inner-divider-news"></div>
							<div class="mb-30">
								<p class="gf-m-r orange text28">
		                        	Congratulazioni!
		                        </p>
		                        <p class="gf-m-r">
		                            Parteciperai alle selezioni per la Black Chili Driving Experience.<br>
Tieni d’occhio l’e-mail nelle prossime settimane, Continental potrebbe inviarti un questionario attitudinale per accedere al prossimo step verso il test drive sulle strade della Provenza.
		                        </p>
		                    </div>
		                    <div class="mb-30">
		                        <p class="gf-m-r text28">
		                        	Invita un amico appassionato come te a partecipare alle selezioni, ti basta il suo indirizzo e-mail.
		                        </p>
		                    </div>
							<div class="inner-divider-news-half"></div>
							<div class="input-prepend">
								<form id="formConsigliaAmico">
									<input type="email" name="email" id="consiglia--email" placeholder="Inserisci l'indirizzo e-mail*" value="">
									<input type="submit" class="icon-mail" style="width:auto;" />
								</form>
							</div>
		                </div>
		            </div> 
		            <div class="inner-divider-news"></div>  
				</div>
			</div>
		</div>
    </section>
    
    <section class="section-light" id="footer_intro" style="display: block;">
        <div class="halves">
            <div class="half about-bg hidden-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/black-chili/continental-black-chili.jpg)"></div>
            <div class="half about-bg visible-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/black-chili/continental-black-chili.jpg)"></div>
            <div class="half">
                <div class="inner-divider"></div>
                <div class="post-all inner-spacer">
                    <div class="the-overline"></div>
                    <div class="inner-divider-half"></div>
                    <div>
                        <p class="gf-m-r black text28">
                            Performance e sicurezza senza compromessi con la mescola <b>Black Chili.</b>
                        </p>
                    </div>
                    <div class="inner-divider-half"></div>
                    <div>
                        <a class="custom-button fadeIn-element" href="https://www.continental-pneumatici.it/auto/stories/cycling/la-vuelta-espana/vuelta-a-espana/vuelta-black-chili" target="_black">Scopri di più</a>
                    </div>
                </div>
                <div class="inner-divider"></div>
            </div>
        </div>
    </section>

	<section class="section-light" id="footer_end">
        <div class="halves">
            <div class="half about-bg hidden-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/black-chili/continental-black-chili.jpg)"></div>
            <div class="half about-bg visible-xs" style="background-image:url(<?=get_template_directory_uri()?>/public/images/black-chili/continental-black-chili.jpg)"></div>
            <div class="half">
                <div class="inner-divider"></div>
                <div class="post-all inner-spacer">
                    <div class="the-overline"></div>
                    <div class="inner-divider-half"></div>
                    <div>
                        <p class="gf-m-r black text28">
                            Vuoi saperne di più sulla rivoluzionaria mescola alla base della Black Chili Driving Experience?
                        </p>
                        <p class="gf-m-r black">
                            Grazie all’aggiunta di nano-particelle di carbone, gli pneumatici a mescola Black Chili hanno performance, tenuta di strada e durabilità migliorate, per prestazioni senza precedenti, in totale sicurezza.
                        </p>
                    </div>
                    <div class="inner-divider-half"></div>
                    <div>
                        <a class="custom-button fadeIn-element" href="https://www.continental-pneumatici.it/auto/stories/cycling/la-vuelta-espana/vuelta-a-espana/vuelta-black-chili" target="_black">Scopri di più</a>
                    </div>
                </div>
                <div class="inner-divider"></div>
            </div>
        </div>
    </section>

	<section class="wrapper_card inner-spacer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div style="margin:30px 0;">
                    	<a class="custom-button fadeIn-element" href="<?php bloginfo('url'); ?>">Torna alla home</a>
                    </div>
				</div>
			</div>
		</div>
    </section>
	<?php endwhile; endif; ?>
	<div class="form__lightbox_wrapper"></div>
	<div class="form__lightbox" id="c1">

        <div class="form__lightbox--close">X</div>

        <div class="form__lightbox--content">
            
            <p><em class="small" style="color: #f1972a;">Leggi e scorri fino in fondo per cliccare sul pulsate di conferma</em></p>
            
            <h3 align="center">INFORMATIVA PER LA PRIVACY</h3>
            <h3 align="center">Informativa resa ai sensi degli artt.13 e 14 del Regolamento (UE) n.679/2016 (GDPR)</h3><br>
            
            <p>Benvenuto dallo staff della IC Digital S.r.l. e di Continental Italia S.p.a., i <a href="/wp-content/uploads/2019/07/Accordo_contitolarita.pdf" target="_blank">Contitolari</a> dei tuoi Dati Personali. Come puoi intuire, al fine di svolgere adeguatamente i nostri servizi di informazione, raccogliamo e utilizziamo alcune informazioni che ti riguardano, in conformità con la legge. IC Digital S.r.l. e Continental Italia S.p.a. si impegnano a proteggere e rispettare la tua privacy. La presente Informativa descrive il Tuo Diritto alla Privacy in merito ai tuoi dati che trattiamo e le misure che adottiamo al fine di tutelarli. Sappiamo che si tratta di un documento lungo, ma ti chiediamo cortesemente di leggere questa Informativa molto attentamente. Di seguito troverai un indice che elenca i punti trattati:</p>

			<p><strong>1. Alcuni termini da chiarire</strong></p>
			<p><strong>2. Quali sono i dati personali che le Società raccolgono e trattano?</strong></p> 
			<p><strong>3. Perché utilizziamo dati personali che ti riguardano? Finalità</strong></p>
			<p><strong>4. Che cosa prevede la legge in merito?</strong></p>
			<p><strong>5. Protezione dei dati conservati</strong></p>
			<p><strong>6. Condivisione in social network</strong></p>
			<p><strong>7. Sei tenuto/a fornirci i dati personali che richiediamo?</strong></p>
			<p><strong>8. Trattiamo i tuoi dati personali senza alcun intervento umano?</strong></p>
			<p><strong>9. Per quanto tempo conserviamo i tuoi dati personali?</strong></p>
			<p><strong>10. Trasferiamo i tuoi dati a terzi o al di fuori dell’Unione Europea?</strong></p>
			<p><strong>11. Quali sono i tuoi diritti?</strong></p>
			<p><strong>12. Come puoi contattarci?</strong></p>
			<p><strong>13. Come gestiamo le modifiche apportate alla presente Informativa?</strong></p>


			<p>
				*<br>
				<strong>1. Alcuni termini da chiarire</strong><br>
				Per prima cosa è necessario essere chiari sul significato che alcune parole rivestono all’interno della presente Informativa. Può sembrare ovvio, ma all’interno di questa Informativa sarai indicato/a come “Interessato”. In questa Informativa utilizziamo il termine "Dati Personali" per qualificare le informazioni che tu, come persona fisica, ci trasmetti e di cui ci fornisci il consenso al trattamento.<br>
				Quando parliamo di "noi" o delle "Società" facciamo riferimento a IC Digital S.r.l. e Continental Italia S.p.a. La sede legale di IC Digital S.r.l. (di seguito anche menzionata come “IC” si trova in C.so di Porta Nuova, 11 cap 20121, Milano, la sede legale di Continental Italia Spa (di seguito anche menzionata come CONTI_ITA) si trova in Via Giovanni Gioacchino Winckelmann, 1, 20146 Milano.<br> 
				IC conduce, su mandato di CONTI_ITA, un’attività di comunicazione volta a raccogliere “candidati” (tu in qualità di “Interessato”)  da selezionare per una collaborazione agli eventi “BLACK CHILI DRIVE EXPERIENCE” (di seguito menzionata come la “Selezione”) come indicato nell’informativa dell’iniziativa (<a href="/wp-content/uploads/2019/07/TERMINI-E-CONDIZIONI-BLACKCHILI_def.pdf" target="_blank">TERMS AND CONDITIONS</a>).
			</p>

			<p>
				<strong>2. Quali sono i dati personali che le Società raccolgono e trattano?</strong><br>
				I dati personali che le Società utilizzano includono, a titolo esemplificato ma non esaustivo:<br>
				A. Nome, data e luogo di nascita, dati personali di contatto e qualifiche, indirizzo e-mail;<br>
				B. La tua conoscenza delle tematiche di guida di automobili in sicurezza, ottenuta mediante la volontaria compilazione di questionario online.<br>
				IC tratterà i tuoi dati nell’ambito dell’iniziativa di “Selezione” sopra menzionata per valutare i candidati e sottoporli a CONTI_ITA per addivenire ad una scelta che porti alla formulazione di un Contratto di collaborazione occasionale ex Art. 2222 c.c con IC.<br> 
				Qualora sia tu a contattarci, i <em></em> dati personali saranno utilizzati esclusivamente per elaborare le richieste di informazioni nel contesto del consenso concesso o in conformità alle normative applicabili in materia di protezione dei dati. Potremmo in questo caso anche tenere traccia delle tue richieste tra cui ad esempio, non esaustivo, la data e l'ora della visita, le pagine visitate, l’uso del cursore sul sito, le funzioni della pagina utilizzate e il sito web da cui l'utente accede al sito della Selezione. L'indirizzo IP viene trasmesso in modo anonimo ed è utilizzato esclusivamente per assegnare una posizione geografica. IC utilizza tali informazioni per misurare l'attività del sito web, per produrre statistiche e per migliorare i servizi e le informazioni fornite tramite il sito. Troverai la cookie policy al seguente <a href="/wp-content/uploads/2019/04/COOKIE-POLICY.pdf" target="_blank">link</a>
			</p>

			<p>
				<strong>3. Perché utilizziamo dati personali che ti riguardano? Finalità</strong><br>
				IC e Continental raccolgono e trattano i tuoi “dati personali”:<br>
				A. per inviarti comunicazioni riguardanti la “Selezione”<br> 
				B. CONTI_ITA per inviarti, solo con tuo consenso, comunicazioni riguardanti i propri prodotti e promozioni commerciali<br>
				C. Per inviarti altre iniziative di selezione ed eventi<br> 
				D. Senza il tuo consenso per ottemperare a tutti gli obblighi di legge;<br>
				Nel corso della seconda fase della Selezione, i Contitolari potranno chiedere ulteriori dati personali e particolari per i quali ti forniranno una specifica informativa e per i quali richiederanno un tuo consenso esplicito.
			</p>

			<p>
				<strong>4. Che cosa prevede la legge in merito?</strong><br>
			La legge prevede che l’informativa per il trattamento dei tuoi dati illustri le finalità e le modalità di trattamento.
			</p> 

			<p>
				<strong>5. Protezione dei dati conservati</strong><br>
			Le nostre società utilizzano misure di sicurezza tecniche e organizzative per proteggere i dati personali da voi forniti contro la manipolazione, perdita, distruzione o dall'accesso da parte di persone non autorizzate. Le misure di sicurezza vengono costantemente migliorate e adattate secondo le tecnologie più recenti. I dati forniti che non sono crittografati potrebbero essere potenzialmente visualizzati da terzi. Per questo motivo ci preme far notare che, per quanto riguarda la trasmissione di dati su Internet (ad esempio tramite e-mail oppure mediante link a siti terzi e social), non può essere garantito un trasferimento sicuro. I dati sensibili non devono pertanto essere trasmessi in alcun modo o devono essere trasmessi esclusivamente tramite una connessione sicura (SSL). Se si accede a pagine e file e viene richiesto di inserire dati personali, si prega di notare che la trasmissione di tali dati tramite Internet potrebbe non essere sicura e che vi è un rischio che tali dati possano essere visualizzati e manipolati da persone non autorizzate.
			</p>

			<p>
				<strong>6. Condivisione in social network</strong><br>
			Nel caso in cui desideri autenticarti al sito coi social, desideriamo informarti che non sarà trasferito alcun dato personale al rispettivo servizio di social media. Nel caso in cui tu voglia utilizzare il tuo profilo per condividere i contenuti del nostro sito web, ti informiamo che non abbiamo alcun controllo sui contenuti e sulla portata dei dati raccolti da tali reti. A tale riguardo, si applicano le condizioni di utilizzo e le politiche sulla privacy delle rispettive reti sociali, pertanto, decliniamo qualsiasi responsabilità per tali contenuti. Il fornitore o l'operatore della pagina è sempre responsabile del contenuto delle pagine collegate. Le pagine collegate sono state controllate al fine di rilevare eventuali violazioni di legge al momento della creazione dei link. Il contenuto illecito non è stato rilevato al momento della creazione dei link. Tuttavia, la verifica continua dei contenuti delle pagine collegate non è ragionevole senza una concreta indicazione di una violazione di legge. Rimuoveremo immediatamente i link pertinenti se questi dovessero violare qualsiasi legge.
			</p>

			<p>
				<strong>7. Sei tenuto/a fornirci i dati personali che richiediamo?</strong><br>
			Teniamo a precisare che il conferimento dei dati di contatto è necessario alla partecipazione della selezione, pertanto qualora incompleti o inesatti l’interessato non potrà accedere alla seconda fase di selezione.
			</p> 

			<p>
				<strong>8. Trattiamo i tuoi dati personali senza alcun intervento umano?</strong><br>
			Si, a volte lo facciamo. Le Società utilizzano sistemi/processi automatizzati e una procedura decisionale altresì automatizzata (come la profilazione) per comunicare con te nell’ambito dei trattamenti illustrati in questa informativa. A tale proposito ti informiamo che i tuoi dati personali   saranno quindi trattati mediante procedure informatiche e cartacee.
			</p>

			<p>
				<strong>9. Per quanto tempo conserviamo i tuoi dati personali?</strong><br>
			Registrandoti alla Selezione tu, in qualità di interessato, presti il tuo esplicito consenso a che IC e Continental conservino i tuoi dati personali per tutta la durata dell’evento e, ove necessario, li trasferiscano ai partner di cooperazione (come a titolo esemplificativo ma non esaustivo, alla compagnia assicuratrice , che ha anche il diritto di memorizzare i dati personali nei termini consentiti dalla legislazione vigente), nella misura in cui la conservazione e il trasferimento di tali dati siano necessari per condurre la selezione e l’evento BLACK CHILI EXPERIENCE.<br> 
			I tuoi dati personali saranno conservati dalle Società per un periodo massimo di 30 mesi dalla registrazione. In seguito essi saranno cancellati in conformità alle normative vigenti. Salvo che i partecipanti non confermino espressamente altrimenti, le informazioni fornite come parte della presente Selezione saranno utilizzate esclusivamente per lo svolgimento della stessa e non saranno divulgate a terzi senza l'espressa autorizzazione dei partecipanti.
			</p>

			<p>
				<strong>10. Trasferiamo i tuoi dati personali a terzi o al di fuori dell’Unione Europea?</strong><br>
			I tuoi dati sono ospitati su server di proprietà di IC collocati presso SEEWEB S.p.a. in via Caldera, 1 Milano e possono essere trasferiti sia su sistemi interni alle sedi operative dei contitolari sia trasferiti alle società del gruppo Continental Italia S.p.a.nel rispetto del GDPR.
			</p>

			<p>
				<strong>11. Quali sono i tuoi diritti?</strong><br>
			Si prega di notare che, in qualità di interessati ai sensi del Regolamento generale sulla protezione dei dati dell’UE (GDPR), hai i seguenti diritti in relazione al trattamento dei tuoi dati personali:<br>
			- Diritti d'informazione ai sensi dell'articolo 13 e dell'articolo 14 del GDPR <br>
			- Diritto di accesso ai sensi dell'articolo 15 del GDPR<br>
			- Diritto di rettifica ai sensi dell'articolo 16 del GDPR<br>
			- Diritto alla cancellazione ai sensi dell'articolo 17 del GDPR<br>
			- Diritto di limitazione di trattamento ai sensi dell'articolo 18 del GDPR<br>
			- Diritto alla portabilità dei dati ai sensi dell’articolo 20 del GDPR<br>
			Ai sensi dell'articolo 7 (3) del GDPR, hai anche il diritto di revocare il tuo consenso alla raccolta, al trattamento/conservazione e all'utilizzo dei dati personali in qualsiasi momento. Il consenso deve essere ritirato scrivendo a IC Digital srl C.so di Porta Nuova, 11 cap 20121, Milano o inviando un’e-mail a <a href="mailto:privacy@ic-digital.com">privacy@ic-digital.com</a><br>
			Quando invii una e-mail per esercitare i tuoi diritti, la Società potrebbe chiederti di identificarti prima di procedere con la tua richiesta. Infine, hai il diritto di presentare reclamo presso l’Autorità Garante della privacy del Paese in cui vivi o lavori oppure del luogo in cui credi si sia generato un problema legato ai tuoi dati.
			</p>

			<p><strong>12. Come puoi contattarci?</strong><br>
			In caso desideri porre domande o reclami, potrai sempre contattare i nostri responsabili della protezione dei dati per iscritto (anche tramite e-mail). E-mail: <a href="mailto:dpo@ic-digital.com">dpo@ic-digital.com</a>.
			</p> 

			<p><strong>13. Come gestiamo le modifiche apportate alla presente Informativa?</strong><br>
			I termini indicati nella presente Informativa potrebbero essere di volta in volta modificati. Laddove questo fosse il caso, sarà nostra premura pubblicare qualsiasi modifica rilevante apportata alla presente Informativa mediante apposita comunicazione sul nostro sito internet/Portale, tramite Newsletter oppure mettendoci direttamente in contatto con te tramite altri canali di comunicazione disponibili.<br>
			Continental Italia S.p.A.<br>
			IC Digital Srl
			</p>
			<br>

            <div class="e-button confirm_check">Confermo di aver letto e accettatato il contenuto dell’informativa</div>
        </div>

    </div>
	<div class="form__lightbox" id="c2">

        <div class="form__lightbox--close">X</div>

        <div class="form__lightbox--content">
            
            <p><em class="small" style="color: #f1972a;">Leggi e scorri fino in fondo per cliccare sul pulsate di conferma</em></p>
            
            <h3 align="center">TERMINI E CONDIZIONI</h3>
            <h3 align="center">INFORMATIVA SUL FUNZIONAMENTO DELL'INIZIATIVA DENOMINATA "BLACK CHILI DRIVING EXPERIENCE"</h3><br>
            
            <p>
			<strong>INTRODUZIONE</strong><br>
			Il presente documento (“Informativa afferente all’iniziativa denominata “BLACK CHILI”) regola l’adesione all’iniziativa di selezione di soggetti (nel seguito “i Candidati)” a cui sottoporre un incarico di collaborazione professionale.
			</p>

			<p><strong>1. SOGGETTO SELEZIONATORE</strong><br>
			IC Digital s.r.l. con sede in C.so di Porta Nuova, 11 20121 Milano, Codice Fiscale e Partita 12884390159 (di seguito il <strong>“Soggetto Selezionatore”</strong> <a href="https://www.ic-digital.com" target="_blank">https://www.ic-digital.com</a>), incaricato dal committente Continental Italia SpA (di seguito "<strong>Continental</strong> o <strong>Committente</strong>").
			</p>

			<p><strong>2. DENOMINAZIONE DELLA SELEZIONE</strong><br>
			 BLACK CHILI DRIVING EXPERIENCE (di seguito, l'"<strong>Iniziativa di Selezione</strong>").
			 </p> 

			<p><strong>3. SITO</strong><br>
			<a href="https://www.sponsoroftheroad.com/black-chili-driving-experience/" target="_blank">https://www.sponsoroftheroad.com/black-chili-driving-experience/</a> (di seguito il "<strong>Sito</strong>")
			</p>

			<p><strong>4. PERIODO DI PARTECIPAZIONE</strong><br>
			Il periodo di partecipazione è stabilito dal 10/07/2019 al 31/07/2019 (di seguito "<strong>Periodo di Partecipazione</strong>").
			</p> 

			<p><strong>5. OGGETTO DELLA SELEZIONE</strong><br>
			L’obiettivo della Selezione, a partecipazione gratuita, è individuare cinque coppie di utenti, disposti a partecipare agli eventi “Black Chili Driving Experience” che si terranno presso Centro Guida Sicura ACI SARA di Lainate in data 13/09/2019 e in due giornate tra il 29/09/2019 e il 02/10/2019 in Provenza.<br>
			Per partecipare all’ Iniziativa di Selezione, tutti i Candidati, dovranno registrarsi sul Sito tramite la funzione “Connect with Facebook” o tramite inserimento diretto dei propri dati personali e acconsentire al trattamento dei propri dati con le finalità della presente selezione. A tutti i Candidati verrà richiesto di effettuare un primo questionario, redatto a cura del Committente, attinente al tema della guida sicura e successivamente, scegliere una targa grafica (di seguito "<strong>Targa</strong>") che caratterizzi ciascuna candidatura tra una serie di formati già predisposti. Ad ogni Candidato verrà altresì inviato a mezzo e-mail un secondo questionario, sempre redatto a cura del Committente, quale seconda verifica delle proprie conoscenze in tema di “performance in sicurezza”.<br>
			Al termine del Periodo di Partecipazione, i Candidati selezionati dovranno sostenere un colloquio conoscitivo e/o test attitudinale, <em>sotto la responsabilità di Continental</em>, a seguito del quale saranno individuati i partecipanti al corso di guida sicura previsto per il 13/09/2019 presso Centro Guida Sicura ACI SARA di Lainate e infine le 2 coppie di persone che realmente prenderanno parte alla “Black Chili Driving Experience” in Provenza.<br>
			Questa esperienza è finalizzata a stimolare riflessioni sulla relazione tra sicurezza e passione automobilistica e potrà prevedere la produzione di contenuti audio, video, testuali e fotografici a cui i Candidati prenderanno parte.
			</p>

			<p><strong>6. AMBITO TERRITORIALE E DESTINATARI</strong><br>
			L’Iniziativa di Selezione è rivolta a Candidati ambosessi, maggiorenni, nel rispetto del codice della strada e in possesso di regolare licenza autorizzativa alla guida da almeno 5 anni, residenti nel territorio della Repubblica Italiana, cittadini italiani o stranieri in possesso di un titolo valido di soggiorno che autorizzi a prestare opera lavorativa.<br>
			In sede di registrazione sul Sito, deve essere confermata l’accettazione del presente documento.
			</p>

			<p><strong>7. MODALITÀ DI PARTECIPAZIONE </strong><br>
			a) I Candidati per poter aderire all’Iniziativa della Selezione devono registrarsi sul Sito tramite la funzione “Connect with Facebook” o tramite inserimento diretto dei propri dati personali e fornire il consenso al loro trattamento anche per ricevere comunicazioni da parte di Continental su future attività e promozioni. La registrazione alla Selezione prevede la compilazione di una scheda di dati personali: nome, cognome ed e-mail, città di residenza e il proprio numero di telefono. I campi obbligatori sono identificati da un asterisco nel modulo di registrazione.<br> 
			b) I Candidati devono sostenere un questionario di tre domande attinenti al tema della guida sicura, della manutenzione degli pneumatici e del loro funzionamento. A seguito del questionario, i Candidati dovranno scegliere una Targa.<br>
			c)  Ad ogni Candidato, potrà essere sottoposto mediante e-mail un secondo questionario attinente al tema della guida sicura.<br> 
			d) Alla fine del Periodo di Partecipazione, alle ore 23.59 del 31/07/2019, il Soggetto Selezionatore secondo indicazione di Continental procederà alla selezione di una rosa di Candidati che saranno contattati tramite e-mail o telefono e chiamati a presentarsi per un colloquio conoscitivo e messi alla prova su un circuito in pista per la guida sicura. Dalla rosa di Candidati saranno selezionati i partecipanti per la “Black Chili Driving Experience”.<br>  
			e) I Candidati selezionati saranno contattati tramite e-mail o telefono dal Soggetto Selezionatore, per i seguenti aspetti, segnatamente:<br>
			i. Stipula del contratto di lavoro autonomo occasionale ex art. 2222 c.c.;<br>
			ii. Sottoscrizione informativa privacy;<br>  
			iii. Accordo per copertura assicurativa.<br>
			Nel caso in cui, i Candidati selezionati non dovessero riscontrare prontamente la comunicazione scritta ricevuta dal Soggetto Selezionatore, questo avrà, ora per allora, il diritto di selezionare un altro Candidato, in sostituzione.
			</p>
			 
			<p><strong>8. TERMINI E CONDIZIONI GENERALI</strong><br>
			a) La partecipazione all’Iniziativa della Selezione implica l’accettazione da parte di ciascun Candidato del presente documento, che è soggetto alla legge della Repubblica Italiana.<br>
			b) I Candidati potranno aderire registrandosi sul Sito con un unico account a loro intestato. Nel caso in cui il Soggetto Selezionatore venga a conoscenza del fatto che un Candidato si è registrato con plurimi account allo stesso riconducibili, avrà la facoltà di escludere tale Candidato dall’Iniziativa della Selezione.<br>
			c) Le piattaforme dei Social Network sono utilizzate come veicolo di comunicazione e partecipazione all’Iniziativa della Selezione, che non è in nessun modo sponsorizzata, promossa, o amministrata dai Social Network né in alcun modo associata a questi.<br>
			Nessuna responsabilità è imputabile al Soggetto Selezionatore né Continental nei confronti dei Candidati e/o dei terzi, per condotte illecite di terzi ascrivibili ai Candidati, a terzi e/o al Social Network e che siano riconducibili a condotte illecite o illegittime o contra legem quali, ad esempio, la pubblicazione di fotografie contrarie al buon costume, all'ordine pubblico, denigratorie, offensive o che ledano qualsiasi tipo di diritto, della persona e di proprietà, anche intellettuale e d'autore.<br>
			d) Ogni Candidato è responsabile del contenuto dei propri Post pubblicati in relazione alla selezione e garantisce di esserne autore nonché di vantare, a titolo originario, tutti i diritti di utilizzazione e sfruttamento relativi al materiale prodotto per l’Iniziativa della Selezione. Qualora i contenuti non fossero stati realizzati dal Candidato e questi non fosse titolare di ogni più ampio diritto di utilizzazione e sfruttamento economico, lo stesso, nell’aderire all’Iniziativa della Selezione si impegna a manlevare e tenere indenne il Soggetto Selezionatore, nonché Continental, da qualsiasi richiesta, anche di risarcimento di danni, che potesse venire avanzata nei suoi confronti dall'autore o dal titolare di tali diritti ovvero da terzi aventi causa.<br>
			e) Il Soggetto Selezionatore e Continental non sono in alcun modo responsabili per eventuali richieste di risarcimento avanzate da soggetti ritratti o da soggetti aventi la potestà genitoriale su minori nominati o comparenti che apparissero nei Post, senza aver ottenuto apposita autorizzazione in merito: pertanto, ciascun Candidato rilascia, con l'accettazione del presente documento, ogni più ampia manleva in tal senso nei confronti sia del Soggetto Selezionatore che di Continental. 
			Il Candidato, con l'adesione all’Iniziativa della Selezione, concede, ad ogni effetto di legge, al Soggetto Selezionatore ed a Continental il diritto alla ripubblicazione (repost) dei propri contenuti sui propri profili Social.  
			f) I Candidati che, secondo il giudizio del Soggetto Selezionatore, risultino in violazione del normale svolgimento dell’Iniziativa della Selezione o comunque giudicati in maniera sospetta e/o fraudolenta, potranno essere esclusi dalla stessa ad insindacabile giudizio del Soggetto Selezionatore. Il Soggetto Selezionatore e/o Continental si riservano il diritto di procedere, nei termini giudicati più opportuni, e nel rispetto delle leggi vigenti, a limitare e ad inibire ogni iniziativa volta ad aggirare le regole dell’Iniziativa della Selezione, nonché si riservano di tutelare i propri diritti ed interessi nelle sedi opportune.<br>
			g) Saranno in ogni caso esclusi e senza possibilità di reclamo, a semplice discrezione del Soggetto Selezionatore, tutti i Post lesivi della comune decenza e/o contenenti riferimenti pubblicitari, sessuali, politici o riconducibili all'ambito religioso.<br>
			h) Il Soggetto Selezionatore e/o Continental non si assumono alcuna responsabilità per eventuali problemi di accesso, impedimento, disfunzione o difficoltà riguardanti gli strumenti tecnici, il computer, i cavi, l'elettronica, il software e l'hardware, la trasmissione e la connessione, la linea telefonica e/o simili altri impedimenti che possano impedire o ritardare ad un Candidato l’adesione all’Iniziativa della Selezione.<br>
			i) Il transito dei contenuti dalle piattaforme dei Social Network al Sito avviene per tramite di un software che opera dialogando con canali di comunicazione e relative regole stabilite dai Social Network. Tali canali e regole sono soggetti a cambiamento secondo le volontà dei Social Network, eventuali mancati trasferimenti di alcuni Post al Sito non potranno essere imputati né al Soggetto Selezionatore né a Continental.<br> 
			j) Ogni Candidato, con l’adesione all’Iniziativa della Selezione, concede, ad ogni effetto di legge al Soggetto Selezionatore e/o a Continental una licenza d’utilizzo, gratuita, per la riproduzione e pubblicazione dei contenuti (testi, elementi grafici, immagini, video, ecc.), in qualsiasi modalità o forma, ivi incluso il diritto di modifica, rinunciando a qualsiasi pretesa al riguardo, anche di sfruttamento economico. <br>
			k) Ogni Candidato garantisce al Soggetto Selezionatore e/o a Continental il pacifico godimento dei diritti ceduti ai sensi dell’articolo che precede, senza eccezione alcuna. <br>
			l) Il Soggetto Selezionatore dichiara che il Sito, di proprietà di Continental, e il relativo software di raccolta e gestione di tutti i dati relativi all’Iniziativa di Selezione si trovano presso Seeweb Spa in Milano via Caldera, 21, Italia. 
			m) Il Soggetto Selezionatore dichiara altresì di essere dotato di modello Privacy e DPO presso cui esercitare i propri diritti all’indirizzo <a href="mailto:dpo@ic-digital.com">dpo@ic-digital.com</a><br>
			n) Continental dichiara di essere dotato di modello Privacy e DPO presso cui esercitare i propri diritti all’indirizzo <a href="mailto:passenger_marketing.italy@conti.de">passenger_marketing.italy@conti.de</a><br>
			o) La raccolta dei dati dei Candidati avviene in ottemperanza del D. Lgs. n. 101/2018 recante le disposizioni di adeguamento alla normativa nazionale, D. Lgs. n.196/2003, alle disposizioni del Regolamento (UE) n.679/2016 del Parlamento europeo e del Consiglio, 27 aprile 2016, relativo alla protezione delle persone fisiche con riguardo al trattamento dei dati personali, nonché alla libera circolazione di tali dati e che abroga la direttiva 95/46/CE, per la finalità di partecipazione all’Iniziativa della Selezione.
			</p>
			 
			<p><strong>9. INFORMAZIONI E MEZZI USATI PER LA PUBBLICIZZAZIONE</strong><br>
			Per richieste di informazioni relative all’Iniziativa di Selezione e/o al presente documento si prega di contattare <a href="mailto:blackchilidriveexperience@onlytalent.it">blackchilidriveexperience@onlytalent.it</a>
			</p>

			<p><strong>10. LEGGE REGOLATRICE E FORO COMPETENTE</strong><br>
			Per ogni controversia sarà esclusivamente competente il Foro di Milano
			</p>
			<br>

            <div class="e-button confirm_check">Confermo di aver letto e accettatato il contenuto dei termini e condizioni</div>
        </div>

    </div>
<?php get_footer(); ?>
<?php } ?>