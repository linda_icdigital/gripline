<?php
/* ---Basics--- */
//show_admin_bar(false);
add_theme_support("menus");
add_theme_support("post-thumbnails");
remove_action('wp_head', 'wp_generator');

/* ---Script and styles--- */
function load_script_and_style() {
	wp_enqueue_style('plugins', get_template_directory_uri().'/css/plugins.css');
	wp_enqueue_style('style_t', get_template_directory_uri().'/css/style.css');
    wp_enqueue_style('google_font', 'https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900%7COswald:300,400,700%7CMontserrat:300,400,500,600,700,800%7CLato:400i,700');
    wp_enqueue_style('style', get_stylesheet_uri(), false);
	wp_enqueue_style('custom', get_template_directory_uri().'/public/css/app.css', false, 8);
	
	wp_enqueue_script( 'continental-jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), false, true );
	wp_enqueue_script('plugins', get_template_directory_uri().'/public/js/plugins.js', array(), false, true);
	wp_enqueue_script('spirex', get_template_directory_uri().'/public/js/spirex.js', array(), 6, true);
	wp_enqueue_script('moment', get_template_directory_uri().'/public/js/moment.js', array(), false, true);
	wp_enqueue_script('matchHeight', get_template_directory_uri().'/public/js/jquery.matchHeight-min.js', array(), false, true);
	wp_enqueue_script('circle-progress', get_template_directory_uri().'/public/js/circle-progress.min.js', array(), false, true);
	wp_enqueue_script('custom', get_template_directory_uri().'/public/js/app.js', array(), 10, true);
	wp_localize_script('custom', 'parameters', array(
		'template_url' => esc_url(get_template_directory_uri()),
		'ajax_url' => admin_url('admin-ajax.php')
	));
}
add_action( 'wp_enqueue_scripts', 'load_script_and_style' );

/* ---Disable emojicons--- */
function disable_emojicons_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}
function disable_wp_emojicons() {
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
	wp_deregister_script('wp-embed');
}
add_action( 'init', 'disable_wp_emojicons' );

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);

function my_jquery_enqueue() {
   wp_deregister_script('jquery');
}

/* ---ACF Option page--- */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Config. sito',
		'menu_title'	=> 'Config. sito'
	));
}

/* ---Add class active to wp nav menu for the template--- */
add_filter('nav_menu_css_class' , 'add_class_active' , 10 , 2);
function add_class_active ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

add_filter('body_class', 'custom_class');

function custom_class( $bodyClass ) {

    if ( is_front_page() ) {
        $bodyClass[] = 'homePage';
    }

    if ( is_page_template( 'black-chili.php' ) ) {
        $bodyClass[] = 'blackChiliPage';
    }
    if ( is_page_template( 'black-chili-survey.php' ) ) {
        $bodyClass[] = 'blackChiliSurveyPage';
    }

    return $bodyClass;

}

// NEWS
add_action('wp_ajax_get_detail_event', 'get_detail_event' );
add_action('wp_ajax_nopriv_get_detail_event', 'get_detail_event' );

function get_detail_event(){

	$args = [
		'suppress_filters' => true,
		'post_type' 	=> 'evento',
		'p'	=> $_POST["id_evento"]
	];

	global $wp_query;
	$wp_query = new WP_Query( $args );
	setlocale(LC_TIME, 'it_IT');
	if( $wp_query->have_posts() ):
		
		while( $wp_query->have_posts() ) : $wp_query->the_post();

			echo json_encode([

				"title" => get_the_title(),
				"cover" => get_the_post_thumbnail_url(get_the_ID(),'full'),
				"data" => strftime("%e %B %Y // %H:%M",strtotime(get_field('date'))),
				"description" => get_the_content(),
				"location" => get_field('location')

			]);

		endwhile;
	
	endif;
						
    wp_die();
}

// BALCKCHILI
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include(__DIR__.'/phpmailer/src/Exception.php');
include(__DIR__.'/phpmailer/src/PHPMailer.php');
include(__DIR__.'/phpmailer/src/SMTP.php');


function mail2smtp($destinatario,$oggetto,$txt){

	$mail = new PHPMailer(true);
	try {

		$mail->IsSMTP();
		$mail->Host = "email-smtp.eu-west-1.amazonaws.com";
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "tls";
		$mail->Username = "AKIAJBHD35ZWZRCRLYXQ";
		$mail->Password = "AqXjjEYal+tDu0c3K0PSmy0cV/VDspZYGMqTD+I2X8fv";
		$mail->From = "passenger_marketing.italy@conti.de";
		$mail->FromName = "sponsoroftheroad.com";
		$mail->AddAddress($destinatario);
		$mail->WordWrap = 80;
		$mail->IsHTML(true);
		$mail->CharSet = 'UTF-8';
		$mail->Subject = $oggetto;
		$mail->Body = $txt; 		

		$mail->Send();
	
    	echo json_encode([

			"status" => "OK"

		]);

	} catch (Exception $e) {
		echo json_encode([

			"status" => "KO",
			"message" => $mail->ErrorInfo

		]);
	}

}

function mail2smtp_survey($destinatario,$oggetto,$txt){

	$mail = new PHPMailer(true);
	try {

		$mail->IsSMTP();
		$mail->Host = "email-smtp.eu-west-1.amazonaws.com";
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "tls";
		$mail->Username = "AKIAJBHD35ZWZRCRLYXQ";
		$mail->Password = "AqXjjEYal+tDu0c3K0PSmy0cV/VDspZYGMqTD+I2X8fv";
		$mail->From = "passenger_marketing.italy@conti.de";
		$mail->FromName = "Black Chili Driving Experience";
		$mail->AddAddress($destinatario);
		$mail->WordWrap = 80;
		$mail->IsHTML(true);
		$mail->CharSet = 'UTF-8';
		$mail->Subject = $oggetto;
		$mail->Body = $txt; 		


		$mail->Send();
	

	} catch (Exception $e) {
		echo json_encode([

			"status" => "KO",
			"message" => $mail->ErrorInfo

		]);
	}

}

add_action('wp_ajax_check__userBlackChili', 'check__userBlackChili' );
add_action('wp_ajax_nopriv_check__userBlackChili', 'check__userBlackChili' );

function check__userBlackChili(){

	if(!isset($_POST['email'])) die();

	global $wpdb;

	if(!isset($_POST['fbid'])){
		$email = $_POST['email'];
		$wpdb->get_results( "SELECT * FROM `utenti` WHERE email = '$email'" );
		echo json_encode($wpdb->num_rows);
	} else {
		$fb_id = $_POST['fbid'];
		$wpdb->get_results( "SELECT * FROM `utenti` WHERE fb_id = '$fb_id'" );
		if($wpdb->num_rows > 0){

			echo json_encode($wpdb->num_rows);

		} else {

			$email = $_POST['email'];
			$wpdb->get_results( "SELECT * FROM `utenti` WHERE email = '$email'" );
			echo json_encode($wpdb->num_rows);

		}
	}
    wp_die();
}

add_action('wp_ajax_blackChili__registraQuestionario', 'blackChili__registraQuestionario' );
add_action('wp_ajax_nopriv_blackChili__registraQuestionario', 'blackChili__registraQuestionario' );

function blackChili__registraQuestionario(){

	global $wpdb;

	if(!isset($_POST['nome'])) die();
	if(!isset($_POST['email'])) die();
	if(!isset($_POST['age'])) die();
	if(!isset($_POST['terms'])) die();
	if(!isset($_POST['policy1'])) die();
	if(!isset($_POST['policy2'])) die();
	if(!isset($_POST['risposte'])) die();
	if(!isset($_POST['squadra'])) die();
	if(!isset($_POST['badge_name'])) die();

	$fb_id = isset($_POST['fbid']) ? $_POST['fbid'] : '';
	$nome = $_POST['nome'];
	$cognome = isset($_POST['cognome']) ? $_POST['cognome'] : '';
	$email = $_POST['email'];
	$age = $_POST['age'];
	$terms = $_POST['terms'];
	$policy1 = $_POST['policy1'];
	$policy2 = $_POST['policy2'];
	$risposte = $_POST['risposte'];
	$squadra = $_POST['squadra'];
	$badge_name = $_POST['badge_name'];

	$queryRegistrazione = "INSERT INTO `utenti` (fb_id, nome, cognome, email, privacy_1, privacy_2, privacy_3, privacy_4, risposta_1, risposta_2, risposta_3, squadra, badge_name) VALUES ('".$fb_id."', '".$nome."', '".$cognome."', '".$email."', '".$age."', '".$terms."', '".$policy1."', '".$policy2."', '".$risposte[0]['risposta']."', '".$risposte[1]['risposta']."', '".$risposte[2]['risposta']."', '".$squadra."', '".$badge_name."');";

	$results = $wpdb->query( $queryRegistrazione );
	$lastid = $wpdb->insert_id;
	
	if($results == false) die();

    $template_survey = file_get_contents(__DIR__ . "/email/template_survey.html");
	$template_survey = str_replace("@@EMAILU@@",$email,$template_survey);
	$template_survey = str_replace("@@IDUTENTE@@",$lastid,$template_survey);	


	mail2smtp_survey($email,"Per te la Black Chili Driving Experience continua.",$template_survey);
	echo json_encode(["status" => "OK", "data" => $wpdb->insert_id]);
    
	wp_die();
}

add_action('wp_ajax_blackChili__consigliaAmico', 'blackChili__consigliaAmico' );
add_action('wp_ajax_nopriv_blackChili__consigliaAmico', 'blackChili__consigliaAmico' );


function blackChili__consigliaAmico(){

	if(!isset($_POST['email'])) die();
	if(!isset($_POST['nome'])) die();

	global $wpdb;
	$email = $_POST['email'];
	$nome = stripslashes($_POST['nome']);
	$id_utente = $_POST['id_utente'];
	$wpdb->get_results( "SELECT * FROM `utenti` WHERE email = '$email'" );
	
	if($wpdb->num_rows == 0){  
 
		$template_email = file_get_contents(__DIR__ . "/email/template.html");
		$template_email = str_replace("@@NOME@@",$nome,$template_email);
		$template_email = str_replace("@@IDU@@",$id_utente,$template_email);


		mail2smtp($email,$nome." ti sfida a partecipare alla Black Chili Driving Experience! Accetti l'invito?",$template_email);
 
	}
    wp_die();
}

add_action('wp_ajax_survey__saveStep', 'survey__saveStep' );
add_action('wp_ajax_nopriv_survey__saveStep', 'survey__saveStep' );

function survey__saveStep(){

	if(!isset($_POST['id'])) die();
	if(!isset($_POST['email'])) die();
	if(!isset($_POST['numero_risposta'])) die();
	if(!isset($_POST['risposta'])) die();
	if(!isset($_POST['punteggio'])) die();

	global $wpdb;
	$id_utente = $_POST['id'];
	$email = $_POST['email'];
	$wpdb->get_results( "SELECT * FROM `utenti` WHERE id='".$id_utente."' AND email='".$email."'");
	if($wpdb->num_rows != 1){

		wp_die();

	}

	$wpdb->get_results( "SELECT * FROM `utenti_survey` WHERE id_utente='".$id_utente."'");

	if($wpdb->num_rows == 0){

		$querySurvey = "INSERT INTO `utenti_survey` (id_utente,risposta_".$_POST['numero_risposta'].", risposta_".$_POST['numero_risposta']."_punteggio) VALUES ('".$id_utente."','".$_POST['risposta']."', '".$_POST['punteggio']."')";

	} else {

		$querySurvey = "UPDATE `utenti_survey` SET risposta_".$_POST['numero_risposta']." = '".$_POST['risposta']."', risposta_".$_POST['numero_risposta']."_punteggio = '".$_POST['punteggio']."' WHERE id_utente = '".$id_utente."'";

	}

	$results = $wpdb->query( $querySurvey );
	if($results == false) die();

	echo json_encode(["status" => "OK", "q" => $querySurvey]);

    wp_die();
}





add_action('wp_ajax_blackChili__registraSurvey', 'blackChili__registraSurvey' );
add_action('wp_ajax_nopriv_blackChili__registraSurvey', 'blackChili__registraSurvey' );

function blackChili__registraSurvey(){

	global $wpdb;
	$id_utente = $_POST['id'];

			
	$querySurvey = "UPDATE `utenti_survey` SET privacy_1 = '".$_POST['register_policy1']."', privacy_2 = '".$_POST['register_terms']."', fb_url = '".$_POST['register_facebook']."', ig_url = '".$_POST['register_instagram']."' WHERE id_utente = '".$id_utente."'";
			
		echo $querySurvey;


	$results = $wpdb->query( $querySurvey );
	if($results == false) die();

	echo json_encode(["status" => "OK", "q" => $querySurvey]);

	wp_die();
}


?>