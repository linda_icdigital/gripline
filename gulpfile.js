const gulp = require("gulp");
const Elixir = require('laravel-elixir');
require('laravel-elixir-imagemin');

var Task = Elixir.Task;

Elixir((mix) => {
  mix.browserSync({
    proxy: "dev.sponsoroftheroad.com"
  })
  .imagemin()
  .sass('app.scss')
  .webpack('app.js')
});
