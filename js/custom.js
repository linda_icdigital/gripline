$(document).ready(function(){
	check_size_of_e();
	
	$(window).resize(function(){
		check_size_of_e();
	});
	
	function check_size_of_e(){
		$(".text-hide").each(function(){
			var $this = $(this);
			$this.css({
				"height": ($this.width() / 0.6818181818) + "px"
			});
		})
	}
	
	if($("#countdown").length > 0){
		var countDownDate = new Date(''+$("#countdown").attr("data-time-countdown")+'').getTime(),
			countdown = "", days, hours, minutes, seconds, day_string = "giorni", hour_string = "ore", minute_string = "minuti", second_string = "secondi", x, $cont_time = $("#countdown .cont_time");
		x = setInterval(function() {
			var now = new Date().getTime(),
			distance = countDownDate - now;
			days = Math.floor(distance / (1000 * 60 * 60 * 24)),
			hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
			minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
			seconds = Math.floor((distance % (1000 * 60)) / 1000);

			if(days === 1){ day_string = "giorno"; }
			if(hours === 1){ hour_string = "ora"; }
			if(minutes === 1){ minute_string = "minuto"; }
			if(seconds === 1){ second_string = "secondo"; }

			countdown = '<div class="col-xs-3"><span class="number">' + days + '</span><br><span>' + day_string + '</span></div>' + 
				'<div class="col-xs-3"><span class="number">' + hours + '</span><br><span>' + hour_string + '</span></div>' + 
				'<div class="col-xs-3"><span class="number">' + minutes + '</span><br><span>' + minute_string + '</span></div>' + 
				'<div class="col-xs-3"><span class="number">' + seconds + '</span><br><span>' + second_string + '</span></div>';

			$cont_time.html(countdown);

			if (distance < 0) {
				clearInterval(x);
				// window.location.href = window.location.href;
			}
		}, 1000);
	}
	
})