<?php get_header(); setlocale(LC_TIME, 'it_IT.UTF8');?>
	<div id="home"></div>
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>


	<?php /* ?><section class="upper-page section-dark screen">
		<?php $url_bg_image = get_the_post_thumbnail_url($post->ID, 'full'); ?>
        <div class="hero-fullscreen overlay overlay-dark-15">
			<div class="hero-fullscreen-FIX">
                <div class="hero-bg bg-img-SINGLE" style="background-image: url(<?= $url_bg_image; ?>);"></div>
            </div>
        </div>
    </section><?php */ ?>

    <section class="wrapper_card inner-spacer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="inner-divider-news"></div>
					<img alt="<?=the_title(false)?>" src="<?=get_the_post_thumbnail_url($post->ID, 'full');?>" class="img-responsive">
					<div class="inner-divider-news-half"></div>
					<div class="the-overline <?php if($category_id == 4){ ?> black <?php } ?>"></div>
					<div class="inner-divider-news-half"></div>
					<h1 class="post-all-heading"><?=the_title(false)?></h1>
					<div class="inner-divider-news-half"></div>
					<div class="content">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
    </section>

    <?php if(get_field('cta_page')) { ?>
	    <section class="wrapper_card inner-spacer">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div style="margin:30px 0;" class="text-center">
	                    	<a id="cta-page" class="custom-button fadeIn-element" href="<?php the_field('cta_page'); ?>" <?php if(get_field('target_cta_page')) { ?>target="_blank"<?php } ?>>
	                    		<?php the_field('cta_label_page'); ?>
	                    	</a>
	                    </div>
					</div>
				</div>
			</div>
	    </section>
	<?php } ?>


	<section class="wrapper_card inner-spacer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div style="margin:30px 0;">
                    	<a class="custom-button fadeIn-element" href="<?php bloginfo('url'); ?>">Torna alla home</a>
                    </div>
				</div>
			</div>
		</div>
    </section>


	<?php endwhile; endif; ?>
<?php get_footer(); ?>