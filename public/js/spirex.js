/*
  [JS Index]
  
  ---
  
  Template Name: Spirex - Photography Portfolio Template
  Author:  ex-nihilo
  Version: 1.0
*/


/*
  1. preloader
  2. fadeIn.element
  3. navigation
    3.1. page scroll
	3.2. highlight navigation
	3.3. close mobile menu
	3.4. highlight navigation
	3.5. collapse navigation
  4. to top arrow animation
  5. home fadeOut animation
  6. typed text
  7. forms
    7.1. newsletter form
    7.2. contact form
  8. modals
    8.1. sign up modal
    8.2. contact modal
  9. YouTube player
  10. slick slider
    10.1. slick fullscreen slideshow ZOOM/FADE
	10.2. slick featured work
  11. owl slider
    11.1. owl news carousel slideshow
    11.2. owl home IMG carousel slider
    11.3. owl home IMG gallery carousel slider
	11.4. owl testimonials carousel slider
  12. swiper slider
    12.1. swiper parallax slider
	12.2. swiper thumbnail slider horizontal thumbs
  13. magnificPopup
    13.1. magnificPopup works gallery
	13.2. magnificPopup works gallery slider
  14. facts counter
  15. skills bar
*/


$(function() {
    "use strict";
	
	
    $(window).on("load", function() {
        // 1. preloader
        $("#preloader").fadeOut(600);
        $(".preloader-bg").delay(400).fadeOut(600);
		
        // 2. fadeIn.element
        setTimeout(function() {
            $(".fadeIn-element").delay(600).css({
                display: "none"
            }).fadeIn(800);
        }, 0);
    });
	
    // 3. navigation
    // 3.1. page scroll
    $(".page-scroll a").on("click", function(e) { //LP
        var $anchor = $(this);
        $("html, body").stop().animate({
            scrollTop: $($anchor.attr("href")).offset().top - 57
        }, 1500, 'easeInOutExpo');
        e.preventDefault();
    });
    // 3.2. highlight navigation
    $("body").scrollspy({
        target: ".navbar",
        offset: 57
    });
    // 3.3. close mobile menu
    $(".navbar-collapse ul li a").on("click", function() {
        $(".navbar-toggle:visible").click();
    });
    // 3.4. highlight navigation
    $(".link-underline-menu").on("click", function() {
        $(".link-underline-menu").removeClass("active");
        $(this).addClass("active");
    });
	
    $(window).on("scroll", function() {
        // 3.5. collapse navigation
        if ($(".navbar").offset().top > 50) {
            $(".navbar-bg-switch").addClass("main-navigation-bg");
        } else {
            $(".navbar-bg-switch").removeClass("main-navigation-bg");
        }
		
        // 4. to top arrow animation
        if ($(this).scrollTop() > 400) {
            $(".to-top-arrow").addClass("show");
        } else {
            $(".to-top-arrow").removeClass("show");
        }
		
        // 5. home fadeOut animation
        $(".introduction-wrapper, .sign-up-modal-launcher, .social-icons-wrapper, .scroll-indicator-wrapper, .copyright-home").css("opacity", 1 - $(window).scrollTop() / 500);
    });
	
    // 6. typed text
    $(".typed-title").typed({
        strings: ["Fully Responsive", "Photography Portfolio", "Made for KINGS"],
        typeSpeed: 25,
        backDelay: 3500,
        loop: true
    });
	
    // 7. forms
    // 7.1. newsletter form
    $("form#subscribe").on("submit", function() {
        $("form#subscribe .subscribe-error").remove();
        $.post("subscribe.php");
        var s = !1;
        if ($(".subscribe-requiredField").each(function() {
                if ("" === jQuery.trim($(this).val())) $(this).prev("label").text(), $(this).parent().append('<span class="subscribe-error">Please enter your Email</span>'),
                    $(this).addClass("inputError"), s = !0;
                else if ($(this).hasClass("subscribe-email")) {
                    var r = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    r.test(jQuery.trim($(this).val())) || ($(this).prev("label").text(), $(this).parent().append('<span class="subscribe-error">Please enter a valid Email</span>'),
                        $(this).addClass("inputError"), s = !0);
                }
            }), !s) {
            $("form#subscribe input.submit").fadeOut("normal", function() {
                $(this).parent().append("");
            });
            var r = $(this).serialize();
            $.post($(this).attr("action"), r, function() {
                $("form#subscribe").slideUp("fast", function() {
                    $(this).before('<div class="subscribe-success">Thank you for subscribing.</div>');
                });
            });
        }
        return !1;
    });
    // 7.2. contact form
    $("form#form").on("submit", function() {
        $("form#form .error").remove();
        var s = !1;
        if ($(".requiredField").each(function() {
                if ("" === jQuery.trim($(this).val())) $(this).prev("label").text(), $(this).parent().append('<span class="error">This field is required</span>'), $(this).addClass(
                    "inputError"), s = !0;
                else if ($(this).hasClass("email")) {
                    var r = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    r.test(jQuery.trim($(this).val())) || ($(this).prev("label").text(), $(this).parent().append('<span class="error">Invalid email address</span>'), $(this).addClass(
                        "inputError"), s = !0);
                }
            }), !s) {
            $("form#form input.submit").fadeOut("normal", function() {
                $(this).parent().append("");
            });
            var r = $(this).serialize();
            $.post($(this).attr("action"), r, function() {
                $("form#form").slideUp("fast", function() {
                    $(this).before('<div class="success">Your email was sent successfully.</div>');
                });
            });
        }
        return !1;
    });
	
    // 8. modals
    // 8.1. sign up modal
    $(".sign-up-modal-launcher, .sign-up-modal-closer").on("click", function() {
        if ($(".sign-up-modal").hasClass("open")) {
            $(".sign-up-modal").removeClass("open");
        } else {
            $(".sign-up-modal").addClass("open");
        }
    });
    // 8.2. contact modal
    $(".contact-modal-launcher, .contact-modal-closer").on("click", function() {
        if ($(".contact-modal").hasClass("open")) {
            $(".contact-modal").removeClass("open");
        } else {
            $(".contact-modal").addClass("open");
        }
    });
	
    // 9. YouTube player
    $("#bgndVideo").YTPlayer();
	
    // 10. slick slider
    // 10.1. slick fullscreen slideshow ZOOM/FADE
    $(".slick-fullscreen-slideshow-zoom-fade").slick({
        arrows: false,
        initialSlide: 0,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        autoplay: true,
        autoplaySpeed: 4000,
        cssEase: "cubic-bezier(0.7, 0, 0.3, 1)",
        speed: 1600,
        draggable: true,
        dots: false,
        pauseOnDotsHover: true,
        pauseOnFocus: false,
        pauseOnHover: false
    });
	// 10.2. slick featured work
    $(".slick-left").slick({
        arrows: false,
        initialSlide: 0,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: ".slick-right",
        autoplay: true,
        autoplaySpeed: 3500,
        cssEase: "ease",
        speed: 500
    });
    $(".slick-right").slick({
        arrows: false,
        initialSlide: 0,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: ".slick-left",
        autoplay: true,
        autoplaySpeed: 3500,
        cssEase: "ease",
        speed: 500
    });
    $(".slick-bottom").slick({
        arrows: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        cssEase: 'ease',
        speed: 500,
        useCss: true,
        focusOnSelect: true,
        dots: true,
        responsive: [{
            breakpoint: 1023,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: "0px",
                slidesToShow: 3
            }
        }, {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: "0px",
                slidesToShow: 3
            }
        }]
    });

    $(".slick-bottom_v2").slick({
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'ease',
        speed: 500,
        useCss: true,
        focusOnSelect: true,
        dots: true,
        responsive: [{
            breakpoint: 1023,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: "0px",
                slidesToShow: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: "0px",
                slidesToShow: 1
            }
        }]
    });
	
    $(".slick-left").on("beforeChange", function(event, slick, currentSlide, nextSlide) {
        var classname = "bar" + nextSlide;
        document.getElementById("bar").className = classname;
        $(".slick-bottom").slick("slickGoTo", nextSlide);
		$(".slick-left .blockquote .facts-counter-number").countTo('restart');
		$('.circle').circleProgress('redraw');
    });
    /*$(".slick-left").on("afterChange", function(event, slick, currentSlide, nextSlide) {});*/
    $(".slick-bottom, .slick-bottom_v2").on("beforeChange", function(event, slick, currentSlide, nextSlide) {
        $(".slick-left, .slick-right").slick("slickGoTo", nextSlide);
    });
	$(".circle").each(function(){
		var $this = $(this);
		$this.circleProgress({
			value: $this.attr("data-value")/100,
			size: $(this).parents(".wheel").find("img").width()-5,
			thickness: ($(this).parents(".wheel").find("img").width()-5)/2,
			fill: "#f1972a",
			emptyFill: "rgba(0, 0, 0, 0)",
			startAngle: -Math.PI/2
		});
	});
	$(window).on("resize", debounce(function(e){
		$(".circle").each(function(){
			var $this = $(this);
			$this.circleProgress({
				size: $(this).parents(".wheel").find("img").width()-5,
				thickness: ($(this).parents(".wheel").find("img").width()-5)/2
			});
		});
	}));
	function debounce(func){
  		var timer;
  		return function(event){
    		if(timer) clearTimeout(timer);
    		timer = setTimeout(func,100,event);
  		};
	}
	/*.on('circle-inited', function() {
		$("canvas").css("opacity", 1);
	})*/
	
    // 11. owl slider
    // 11.1. owl news carousel slideshow
    var $newscarousel = $("#news-carousel");
	$newscarousel.children().each( function( index ) {
  		$(this).attr( 'data-position', index );
	});
    var $owl_news = $newscarousel.owlCarousel({
        loop: false,
        center: true,
        items: 3,
        margin: 0,
        autoplay: false,
        autoplaySpeed: 1000,
        autoplayTimeout: 5000,
        smartSpeed: 450,
        nav: true,
        navText: ["<i class='owl-custom ion-chevron-left'></i>", "<i class='owl-custom ion-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            }
        }
    });
	$("body").on('click', '#news-carousel .owl-item > div', function() {
	  	$owl_news.trigger('to.owl.carousel', $(this).data( 'position' ) ); 
	});
    // 11.2. owl home IMG carousel slider
    $("#home-page-img-carousel").owlCarousel({
        loop: true,
        center: true,
        items: 3,
        margin: 0,
        autoplay: true,
        autoplaySpeed: 1000,
        autoplayTimeout: 5000,
        smartSpeed: 450,
        nav: true,
        navText: ["<i class='owl-custom ion-chevron-left'></i>", "<i class='owl-custom ion-chevron-right'></i>"],
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            }
        }
    });
    // 11.3. owl home IMG gallery carousel slider
    $("#works-page-img-carousel").owlCarousel({
        loop: false,
        center: false,
        items: 3,
        margin: 0,
        autoplay: false,
        autoplaySpeed: 1000,
        autoplayTimeout: 5000,
        smartSpeed: 450,
        nav: true,
        navText: ["<i class='owl-custom ion-chevron-left'></i>", "<i class='owl-custom ion-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            }
        }
    });
    // 11.4. owl testimonials carousel slider
    $(".testimonials-carousel").owlCarousel({
        loop: true,
        center: true,
        items: 1,
        margin: 0,
        autoplay: true,
        autoplaySpeed: 1000,
        autoplayTimeout: 4000,
        smartSpeed: 450,
        nav: false
    });
	
    // 12. swiper slider
    // 12.1. swiper parallax slider
    var swiper = new Swiper(".parallax .swiper-container", {
        autoplay: 4000,
        speed: 800,
        parallax: true,
        mousewheelControl: false,
        keyboardControl: false,
        nextButton: ".swiper-button-next",
        prevButton: ".swiper-button-prev",
        paginationClickable: true
    });
    // 12.2. swiper thumbnail slider horizontal thumbs
    var swipersliderTop = new Swiper(".swiper-slider-top", {
        direction: "vertical",
        nextButton: ".swiper-button-next",
        prevButton: ".swiper-button-prev",
        autoplay: 4000,
        speed: 1600,
        spaceBetween: 0,
        centeredSlides: true,
        slidesPerView: "auto",
        touchRatio: 1,
        loop: true,
        slideToClickedSlide: true,
        mousewheelControl: false,
        keyboardControl: false
    });
    var swipersliderBottom = new Swiper(".swiper-slider-bottom", {
        direction: "horizontal",
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: "auto",
        touchRatio: 1,
        loop: true,
        slideToClickedSlide: true,
        mousewheelControl: false,
        keyboardControl: false
    });
    swipersliderTop.params.control = swipersliderBottom;
    swipersliderBottom.params.control = swipersliderTop;
	
    // 13. magnificPopup
    // 13.1. magnificPopup works gallery
    $(".popup-photo").magnificPopup({
        type: "image",
        gallery: {
            enabled: false,
            tPrev: "",
            tNext: "",
            tCounter: "%curr% / %total%"
        },
        removalDelay: 100,
        mainClass: "mfp-fade",
        fixedContentPos: false
    });
	// 13.2. magnificPopup works gallery slider
    $(".popup-photo-gallery").each(function() {
        $(this).magnificPopup({
            delegate: "a",
            type: "image",
            gallery: {
                enabled: true
            },
			removalDelay: 100,
			mainClass: "mfp-fade",
			fixedContentPos: false
        });
    });
	
    // 14. facts counter
    $(".facts-counter-number").each(function() {
        var count = $(this);
        count.countTo({
            from: 0,
            to: count.html(),
            speed: 1200,
            refreshInterval: 60,
			formatter: function (value, options) {
      			return value.toFixed(options.decimals) + "%";
    		}
        });
    });
	
    // 15. skills bar
    $(".show-skillbar").appear(function() {
        $(".skillbar").skillBars({
            from: 0,
            speed: 4000,
            interval: 100,
            decimals: 0
        });
    });


});