var BlackChiliSurvey = (function() {
	'use strict';

	var elements = {},
		ajax_request,
		indexStep = 0,
		risposte = [2,3,1,2,1,1,1,1,2,1,3,1],
		punteggi =
			[
				[0,10,-1],
				[-1,0,10],
				[10,0,-1],
				[-1,10,0],
				[10,0,-1],
				[10,0,-1],
				[10,-1,0],
				[10,0,-1],
				[0,10,-1],
				[10,0,-1],
				[-1,0,10],
				[10,0,-1]
			],
		currentIndex_first = 0,
		currentIndex_second = 0,
		resultImage,
		utenteRegistrato;

	function registraQuestionario(callback){

		var dataAjax = registrationData;
		dataAjax.action = 'blackChili__registraQuestionario';

		ajax_request = $.ajax({
			crossDomain: true,
			url: parameters.ajax_url,
			method: 'POST',
			dataType: "json",
			data: dataAjax
		});

		ajax_request.done(function( data ){

			if(data.status == window.onSuccess){

				if(callback){callback(data.data)};

			}

		});

	}

	function consigliaAmico(callback){

		ajax_request = $.ajax({
			crossDomain: true,
			url: parameters.ajax_url,
			method: 'POST',
			dataType: "json",
			data: {

				id_utente: utenteRegistrato,
				nome: window.registrationData.nome,
				email: elements.$consiglia_email.val(),
				action: 'blackChili__consigliaAmico'

			}
		});

		ajax_request.done(function( data ){

			if(data.status == window.onSuccess){

				if(callback){callback()};

			}

		});

	}

	function saveStep(numero_risposta,risposta,callback){

		ajax_request = $.ajax({
			crossDomain: true,
			url: parameters.ajax_url,
			method: 'POST',
			dataType: "json",
			data: {

				id: $("#id_utente").val(),
				email: $("#email_utente").val(),
				numero_risposta: numero_risposta+1,
				risposta: risposta,
				action: 'survey__saveStep'

			}
		});

		ajax_request.done(function( data ){

			if(callback){callback()};

		});

	}

	function bindEvents(){

		// CLICK RISPOSTA
		$(".radio--domanda").on("click", function(){

			indexStep = $("body").find(".slide_question").index($("body").find(".slide_question.active"));

			$(".slide_question").eq(indexStep).find(".radio--domanda").prop("disabled", true);
			$(".slide_question").eq(indexStep).find(".risposta--content").removeClass("active");
			if(risposte[indexStep] == $(".slide_question").eq(indexStep).find(".radio--domanda:checked").attr("data-risposta")){

				$(".slide_question").eq(indexStep).find(".risposta--content").eq(1).addClass("active");

			} else {

				$(".slide_question").eq(indexStep).find(".risposta--content").eq(0).addClass("active");

			}
			$(".slide_question").eq(indexStep).find(".risposta").addClass("active");
			$('html, body').animate({
				scrollTop: $(".slide_question").eq(indexStep).find(".risposta").offset().top - 250
			}, 500, function() {});

			// FA IN MODO CHE NON POSSA RICLICCARE SU ALTRE RISPOSTE IN CASO NE ABBIA APPENA CLICCATA UNA
			if($.isEmptyObject(window.registrationData.risposte[indexStep])){

				saveStep(
					indexStep,
					$(".slide_question").eq(indexStep).find(".radio--domanda:checked").val(),
					() => {

					window.registrationData.risposte[indexStep] = {

						numero_risposta: $(".slide_question").eq(indexStep).find(".radio--domanda:checked").attr("data-risposta"),
						risposta: $(".slide_question").eq(indexStep).find(".radio--domanda:checked").val()

					};

				});			

			}

		});



		// CLICK CONTINUA NEL BALLOON
		$(".risposta--content .custom-button").on("click", function(){

			console.log(indexStep);
			
			if(indexStep < 12){

				$(".step_"+(indexStep+1)).removeClass("active");
				$(".step_"+(indexStep+2)).addClass("active");
				$(".slide_question").eq(indexStep).removeClass("active");
				$(".slide_question").find(".risposta").removeClass("active");
				$(".slide_question").eq(indexStep+1).addClass("active");
				$('html, body').animate({
					scrollTop: $(".slide_question").eq(indexStep+1).offset().top - 100
                }, 500, function() {});

			} else {

				$(".step_13").removeClass("active");
				$(".step_14").addClass("active");
				$('html, body').animate({
					scrollTop: $(".step_14").offset().top - 100
                }, 500, function() {});

			}

		});

		elements.$btn_finish.on("click", function(){

			$(".step_6").removeClass("active");
			registraQuestionario((id_utente) => {

				utenteRegistrato = id_utente;
				$("#footer_end").show();
				$(".step_7").addClass("active");
				$('html, body').animate({
					scrollTop: $(".step_7").offset().top - 100
				}, 500, function() {});

			});

		});

		elements.$checkboxWrap.on("click", function(e){

			e.preventDefault();

			$("#"+$(this).attr("data-check")).addClass("is-active");
			$("#"+$(this).attr("data-check")).find(".form__lightbox--content").scrollTop(0);
			$(".form__lightbox_wrapper").addClass("is-active");
			$(this).find("input").prop("checked",false);

		});

		elements.$formLightbox_close.on("click", function(){

			$("#"+$(this).parent("div").attr("id")).removeClass("is-active");
			$(".form__lightbox_wrapper").removeClass("is-active");

		});

		elements.$confirm_check.on("click", function(){

			$("[data-check='"+$(this).parent("div").parent("div").attr("id")+"']").removeClass("error");
			$("[data-check='"+$(this).parent("div").parent("div").attr("id")+"']").addClass("readed");
			$("[data-check='"+$(this).parent("div").parent("div").attr("id")+"']").find("input").prop("checked",true);
			$(this).parent("div").parent("div").removeClass("is-active");
			$(".form__lightbox_wrapper").removeClass("is-active");

		});

	}

	function cacheElements(){

		elements.$btn_partecipa_ora = $("#btn_partecipa_ora");

		// REGISTER
		elements.$register_form = $("#formRegister");
		elements.$register_nome = $("input[name='register--nome']");
		elements.$register_email = $("input[name='register--email']");
		elements.$register_age = $("input[name='register--age']");
		elements.$register_terms = $("input[name='register--terms']");
		elements.$register_policy1 = $("input[name='register--policy1']");
		elements.$register_policy2 = $("input[name='register--policy2']");

		// OVERLAY
		elements.$checkboxWrap = $(".checkboxWrap.-overlay");
		elements.$confirm_check = $(".confirm_check");
		elements.$formLightbox_close = $(".form__lightbox--close");

		// RISULTATO
		elements.$text_result = $(".step_4").find(".title.text-center");

		// BADGE
		elements.$btn_crea_badge = $("#btn_crea_badge");
		elements.$btn_to_share = $("#btn_to_share");
		elements.$img_to_share = $(".step_6").find('.badge-share img');

		// FINISH
		elements.$btn_finish = $("#btn_finish");

		elements.$btn_share_result = $("#btn_share_result");
		elements.$btn_download_result = $("#btn_download_result");

		elements.$consiglia_form = $("#formConsigliaAmico");
		elements.$consiglia_email = $("#consiglia--email");

	}

	function init(){

		cacheElements();
		bindEvents();

		window.registrationData = {};
		window.registrationData.risposte = [{},{},{}];

	}

	return {
		init:init
	};

}());

module.exports = BlackChiliSurvey;
