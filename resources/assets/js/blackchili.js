import Facebook from './facebook';

var BlackChili = (function() {
	'use strict';

	var elements = {},
		ajax_request,
		// window.registrationData = {},
		indexStep = 0,
		risposte = [3,2,3],
		wrongOne = false,
		badges = [
			[
				'1A',
				'2A',
				'3A',
				'4A',
				'5A',
				'6A'
			],
			[
				'1B',
				'2B',
				'3B',
				'4B',
				'5B',
				'6B'
			],
			[
				'1C',
				'2C',
				'3C',
				'4C',
				'5C',
				'6C'
			],
			[
				'1D',
				'2D',
				'3D',
				'4D',
				'5D',
				'6D'
			],
			[
				'1E',
				'2E',
				'3E',
				'4E',
				'5E',
				'6E'
			],
			[
				'1F',
				'2F',
				'3F',
				'4F',
				'5F',
				'6F'
			],
			[
				'1G',
				'2G',
				'3G',
				'4G',
				'5G',
				'6G'
			]
		],
		currentIndex_first = 0,
		currentIndex_second = 0,
		resultImage,
		utenteRegistrato;

	function registraUtente(callback){

		var dataAjax = registrationData;
		dataAjax.action = 'blackChili__registraQuestionario';

		ajax_request = $.ajax({
			crossDomain: true,
			url: parameters.ajax_url,
			method: 'POST',
			dataType: "json",
			data: dataAjax
		});

		ajax_request.done(function( data ){

			if(data.status == window.onSuccess){

				if(callback){callback(data.data)};

			}

		});

	}

	function consigliaAmico(callback){

		ajax_request = $.ajax({
			crossDomain: true,
			url: parameters.ajax_url,
			method: 'POST',
			dataType: "json",
			data: {

				id_utente: utenteRegistrato,
				nome: window.registrationData.nome,
				email: elements.$consiglia_email.val(),
				action: 'blackChili__consigliaAmico'

			}
		});

		ajax_request.done(function( data ){

			if(data.status == window.onSuccess){

				if(callback){callback()};

			}

		});

	}

	function getSquadra(resultImage){

		if(resultImage.indexOf("A") > -1){

			return "Black Asphalt";			
		}

		if(resultImage.indexOf("B") > -1){

			return "Road Riders";			
		}

		if(resultImage.indexOf("C") > -1){

			return "Turbo Grip";			
		}

		if(resultImage.indexOf("D") > -1){

			return "Chili Racers";			
		}

		if(resultImage.indexOf("E") > -1){

			return "Tyre Masters";			
		}

		if(resultImage.indexOf("F") > -1){

			return "Motor Lovers";			
		}

		if(resultImage.indexOf("G") > -1){

			return "Driving Force";			
		}

	}

	function checkUser(user, callback){

		ajax_request = $.ajax({
			crossDomain: true,
			url: parameters.ajax_url,
			method: 'POST',
			dataType: "json",
			data: {

				fbid: user.fbid,
				email: user.email,
				action: 'check__userBlackChili'

			}
		});

		ajax_request.done(function( data ){

			if(callback){callback(data)};

		});

	}

	function bindEvents(){

		elements.$register_facebook.on("click", function(){

			window.facebook.loginFB(
				{
				
					withPicture: false,
					scope: 'public_profile,email'
				
				}, () => {

				window.facebook.getInfo((userInfo) => {

					checkUser(userInfo.data, (results) => {

						if(results == 0){

							window.registrationData = userInfo.data;
							$(".signin_1").removeClass("active");
							elements.$register_email.val(window.registrationData.email);
							elements.$register_nome.val(`${window.registrationData.nome} ${window.registrationData.cognome}`);
							$(".signin_2").addClass("active");

						} else {

							alert("Accesso già effettuato con questo account.");

						}

					});

				});
					
			});

		});

		elements.$btn_partecipa_ora.on("click", function(){

			$(".step_1").removeClass("active");
			$("#footer_intro").hide();
			$(".step_2").addClass("active");
			$('html, body').animate({
				scrollTop: $(".step_2").offset().top - 100
			}, 500, function() {});
		});

		elements.$preRegister_form.on("submit", function(e){

			e.preventDefault();

			let user = {email:elements.$preRegister_email.val()};
			checkUser(user, (results) => {

				if(results == 0){

					window.registrationData.email = elements.$preRegister_email.val();
					$(".signin_1").removeClass("active");
					elements.$register_email.val(window.registrationData.email);
					$(".signin_2").addClass("active");

				} else {

					alert("Accesso già effettuato con questa e-mail.");

				}

			});

		});

		elements.$register_form.on("submit", function(e){

			e.preventDefault();

			if(window.registrationData.fbid){

				window.registrationData.age = elements.$register_age.prop("checked") == true ? 1 : 0;
				window.registrationData.terms =  elements.$register_terms.prop("checked") == true ? 1 : 0;
				window.registrationData.policy1 = elements.$register_policy1.prop("checked") == true ? 1 : 0;
				window.registrationData.policy2 = elements.$register_policy2.prop("checked") == true ? 1 : 0;

			} else{
				window.registrationData = {

					nome: elements.$register_nome.val(),
					email: elements.$register_email.val(),
					age: elements.$register_age.prop("checked") == true ? 1 : 0,
					terms: elements.$register_terms.prop("checked") == true ? 1 : 0,
					policy1: elements.$register_policy1.prop("checked") == true ? 1 : 0,
					policy2: elements.$register_policy2.prop("checked") == true ? 1 : 0

				};
			}

			$(".step_2").removeClass("active");
			window.registrationData.risposte = [{},{},{}];
			$(".step_3").addClass("active");
			$('html, body').animate({
				scrollTop: $(".step_3").offset().top - 100
			}, 500, function() {});

		});

		$(".radio--domanda").on("click", function(){

			indexStep = $("body").find(".slide_question").index($("body").find(".slide_question.active"));

			$(".slide_question").eq(indexStep).find(".radio--domanda").prop("disabled", true);
			$(".slide_question").eq(indexStep).find(".risposta--content").removeClass("active");
			if(risposte[indexStep] == $(".slide_question").eq(indexStep).find(".radio--domanda:checked").attr("data-risposta")){

				$(".slide_question").eq(indexStep).find(".risposta--content").eq(1).addClass("active");

			} else {

				wrongOne = true;
				$(".slide_question").eq(indexStep).find(".risposta--content").eq(0).addClass("active");

			}
			$(".slide_question").eq(indexStep).find(".risposta").addClass("active");
			$('html, body').animate({
				scrollTop: $(".slide_question").eq(indexStep).find(".risposta").offset().top - 250
			}, 500, function() {});

			if($.isEmptyObject(window.registrationData.risposte[indexStep])){

				window.registrationData.risposte[indexStep] = {

					numero_risposta: $(".slide_question").eq(indexStep).find(".radio--domanda:checked").attr("data-risposta"),
					risposta: $(".slide_question").eq(indexStep).find(".radio--domanda:checked").val()

				};			

			}

		});

		
		$(".risposta--content .custom-button").on("click", function(){
			
			if(indexStep < 2){

				$(".slide_question").eq(indexStep).removeClass("active");
				$(".slide_question").find(".risposta").removeClass("active");
				$(".slide_question").eq(indexStep+1).addClass("active");
				$('html, body').animate({
					scrollTop: $(".slide_question").eq(indexStep+1).offset().top - 100
                }, 500, function() {});

			} else {

				if(wrongOne == false){

					elements.$text_result.eq(0).addClass("active");

				} else {

					elements.$text_result.eq(1).addClass("active");				

				}

				$(".step_3").removeClass("active");
				$(".step_4").addClass("active");
				$('html, body').animate({
					scrollTop: $(".step_4").offset().top - 100
                }, 500, function() {});

			}

		});

		elements.$btn_crea_badge.on("click", function(){

			$(".step_4").removeClass("active");
			$(".step_5").addClass("active");
			$('html, body').animate({
				scrollTop: $(".step_5").offset().top - 100
			}, 500, function() {});

			elements.$carousel_badge.owlCarousel({
			    loop:true,
			    center:true,
			    margin:10,
			    nav:true,
			    navText: ['<img src="/wp-content/themes/gripline/public/images/black-chili/arrow-left.png" alt=""/>', '<img src="/wp-content/themes/gripline/public/images/black-chili/arrow-right.png" alt=""/>'],
			    dots:false,
			    items:1
			});

			elements.$carousel_badge_target.owlCarousel({
			    loop:true,
			    center:true,
			    margin:10,
			    nav:true,
			    navText: ['<img src="/wp-content/themes/gripline/public/images/black-chili/arrow-left.png" alt=""/>', '<img src="/wp-content/themes/gripline/public/images/black-chili/arrow-right.png" alt=""/>'],
			    dots:false,
			    items:1
			});

			for (var i=0; i<elements.$carousel_badge_target.find('.item').length; i++) {
			   elements.$carousel_badge_target.trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
			}

			for (var i=0; i<badges[0].length; i++) {
				elements.$carousel_badge_target
				    .trigger('add.owl.carousel', [`<div class="item"><img src="${window.stylesheet_directory_uri}/public/images/black-chili/badge/badge/${badges[0][i]}.jpg" alt="" /></div>`])
				    .trigger('refresh.owl.carousel');
			}

			resultImage = badges[0][0];

			elements.$carousel_badge.on('translated.owl.carousel', function(event) {

				if(event.item.index-4 < 0){

					currentIndex_first = 6;

				} else if(event.item.index-4 > 6){

					currentIndex_first = 0;

				} else {

					currentIndex_first = event.item.index-4;

				}

				for (var i=0; i<badges[currentIndex_first].length; i++) {
				   elements.$carousel_badge_target.trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}

				for (var i=0; i<badges[currentIndex_first].length; i++) {
					elements.$carousel_badge_target
					    .trigger('add.owl.carousel', [`<div class="item"><img src="${window.stylesheet_directory_uri}/public/images/black-chili/badge/badge/${badges[currentIndex_first][i]}.jpg" alt="" /></div>`])
					    .trigger('refresh.owl.carousel');
				}

				resultImage = badges[currentIndex_first][0];

			});

			elements.$carousel_badge_target.on('changed.owl.carousel', function(event) {

				currentIndex_second = event.item.index-3;
				// currentIndex_second = event.item.index-3;
				resultImage = badges[currentIndex_first][currentIndex_second];

			});

		});

		elements.$btn_to_share.on("click", function(){

			window.registrationData.badge_name = resultImage;
			window.registrationData.squadra = getSquadra(resultImage);
			$(".step_5").removeClass("active");
			elements.$img_to_share.attr("src", `${window.stylesheet_directory_uri}/public/images/black-chili/badge/targhe-share/${resultImage}.jpg`);
			elements.$btn_share_result.attr("href",`https://www.facebook.com/dialog/share?app_id=${window.facebookAppID}&display=popup&href=https://www.sponsoroftheroad.com/black-chili-driving-experience?is=${resultImage}`);
			elements.$btn_download_result.attr("href",`${window.stylesheet_directory_uri}/public/images/black-chili/badge/targhe-share/${resultImage}.jpg`);

			$(".step_6").addClass("active");
			$('html, body').animate({
				scrollTop: $(".step_6").offset().top - 100
			}, 500, function() {});

		});

		elements.$btn_finish.on("click", function(){

			$(".step_6").removeClass("active");
			registraUtente((id_utente) => {

				utenteRegistrato = id_utente;
				$("#footer_end").show();
				$(".step_7").addClass("active");
				$('html, body').animate({
					scrollTop: $(".step_7").offset().top - 100
				}, 500, function() {});

			});

		});

		elements.$consiglia_form.on("submit", function(e){

			e.preventDefault();

			let user = {email:elements.$consiglia_email.val()};

			checkUser(user, (results) => {

				if(results == 0){

					consigliaAmico(() => {

						alert("Il tuo invito è stato inviato!");
						location.href="/";

					});

				} else {

					alert("Accesso già effettuato con questa e-mail.");

				}

			});

		});

		elements.$checkboxWrap.on("click", function(e){

			e.preventDefault();

			$("#"+$(this).attr("data-check")).addClass("is-active");
			$("#"+$(this).attr("data-check")).find(".form__lightbox--content").scrollTop(0);
			$(".form__lightbox_wrapper").addClass("is-active");
			$(this).find("input").prop("checked",false);

		});

		elements.$formLightbox_close.on("click", function(){

			$("#"+$(this).parent("div").attr("id")).removeClass("is-active");
			$(".form__lightbox_wrapper").removeClass("is-active");

		});

		elements.$confirm_check.on("click", function(){

			$("[data-check='"+$(this).parent("div").parent("div").attr("id")+"']").removeClass("error");
			$("[data-check='"+$(this).parent("div").parent("div").attr("id")+"']").addClass("readed");
			$("[data-check='"+$(this).parent("div").parent("div").attr("id")+"']").find("input").prop("checked",true);
			$(this).parent("div").parent("div").removeClass("is-active");
			$(".form__lightbox_wrapper").removeClass("is-active");

		});

	}

	function cacheElements(){

		elements.$carousel_badge = $('#slider-badge');
		elements.$carousel_badge_target = $('#slider-target');

		elements.$btn_partecipa_ora = $("#btn_partecipa_ora");

		// PRE-REGISTER
		elements.$register_facebook = $("#btn_login_facebook");
		elements.$preRegister_form = $("#formPreRegister");
		elements.$preRegister_email = $("input[name='preRegister--email']");

		// REGISTER
		elements.$register_form = $("#formRegister");
		elements.$register_nome = $("input[name='register--nome']");
		elements.$register_email = $("input[name='register--email']");
		elements.$register_age = $("input[name='register--age']");
		elements.$register_terms = $("input[name='register--terms']");
		elements.$register_policy1 = $("input[name='register--policy1']");
		elements.$register_policy2 = $("input[name='register--policy2']");

		// OVERLAY
		elements.$checkboxWrap = $(".checkboxWrap.-overlay");
		elements.$confirm_check = $(".confirm_check");
		elements.$formLightbox_close = $(".form__lightbox--close");

		// RISULTATO
		elements.$text_result = $(".step_4").find(".title.text-center");

		// BADGE
		elements.$btn_crea_badge = $("#btn_crea_badge");
		elements.$btn_to_share = $("#btn_to_share");
		elements.$img_to_share = $(".step_6").find('.badge-share img');

		// FINISH
		elements.$btn_finish = $("#btn_finish");

		elements.$btn_share_result = $("#btn_share_result");
		elements.$btn_download_result = $("#btn_download_result");

		elements.$consiglia_form = $("#formConsigliaAmico");
		elements.$consiglia_email = $("#consiglia--email");

	}

	function init(){

		Facebook.init();
		cacheElements();
		bindEvents();

	}

	return {
		init:init
	};

}());

module.exports = BlackChili;
