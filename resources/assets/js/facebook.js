window.facebook = (function(){

	var userInfo = {},
		ajax__checkFB,
		userExist;

    function initFB(){

        window.fbAsyncInit = function(appId) {

            FB.init({
                appId: window.facebookAppID,
                status: true,
                cookie: true,
                xfbml: false,
                version	: 'v3.3'
            });

        };

    }

    function getInfo(callback){

    	return callback(userInfo);

    }

    function getInfoFB(loginOptions, callback){

		FB.api('/me?fields=first_name,last_name,email&auth_type=rerequest', function(response) {

			userInfo.data.nome = response.first_name;
			userInfo.data.cognome = response.last_name;
			userInfo.data.email = response.email;

            if(loginOptions.withPicture){

                FB.api('/me/picture?height=720&redirect=0',function(response) {

    				userInfo.data.foto = response.data.url;
    				if(callback != null) {callback();}

    			});

            } else {

                if(callback != null) {callback();}

            }

		});

    }

    function loginFB(options, callback){
        
        FB.login(function(response) {

            checkUser(options, response, callback);
        
        }, {
            scope: options.scope,
            return_scopes: true,
            auth_type: 'rerequest'
        });

    }

    function checkUser(options, response, callback){

    	userInfo.data = {};

        if(response.status === 'connected') {

            if(response.authResponse.grantedScopes) {

                if(response.authResponse.grantedScopes.includes("email")){

                    response.loginOptions = options;
                    userInfo.status = window.onSuccess;
                    userInfo.data.fbid = response.authResponse.userID;
                    userInfo.data.token = response.authResponse.accessToken;
                    getInfoFB(response.loginOptions, callback);

                } else {

                    alert("Permessi aggiuntivi necessari");

                }

            }

        } else if(response.status === 'not_authorized') {

            // loginFB(options, callback);

        } else if(response.status === 'unknown') {

            // userInfo.status = "KO";
            // if(callback) {callback();}

        }

    }

    function attachFBScript(){

        (function(d, s, id){

            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/it_IT/all.js";
            fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'facebook-jssdk'));

    }

    function init() {

        attachFBScript();
        initFB();

    }

    return {
        init: init,
        loginFB:loginFB,
        getInfo:getInfo
    }

})();

module.exports = window.facebook;