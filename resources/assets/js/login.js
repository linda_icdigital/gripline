import Cookies from 'js-cookie';

window.loginOnlyTalent = (function() {
	'use strict';

	var elements = {},
		ajax_request = null;


	function logout(){

		Cookies.remove(window.cookieName);
		location.reload();

	}

	function checkUser(user, callback){

		ajax_request = $.ajax({
			crossDomain: true,
			url: window.urlService + 'login',
			method: 'POST',
			dataType: "json",
			data: {

				fbid: user.fbid,
				access_token: user.token

			}
		});

		ajax_request.done(function( data ){

			if(data.status == window.onSuccess) {

				Cookies.set(
					window.cookieName,
					{
						userid: data.data.userid,
						fbid: user.fbid,
						access_token: data.data.access_token,
						nome: data.data.nome,
						cognome: data.data.cognome
					},
					{ expires: 7 }
				);

				window.userid = data.data.fbid;

				if(callback){callback({status:window.onSuccess}, data.data)};

			} else {

				if(callback){callback(data, user)};

			}

		});

	}


	function login(withPicture, callback){

		window.facebook.loginFB(
			{
			
				withPicture: withPicture,
				scope: 'public_profile,email'
				// scope: 'public_profile,email'
			
			}, () => {

			window.facebook.getInfo((userInfo) => {

				checkUser(userInfo.data, callback);

			});
				
		});

	}


	function bindEvents(){

		elements.$btnLogin.on("click", () => {

			$("html").addClass("overlay-open");
			$("#overlay_login").addClass("active");

		});

		elements.$btnLogout.on('click', function(){

			logout();

		});

	}

	function cacheElements(){

		elements.$btnLogin = $("#btn_login");
		elements.$btnRegister = $("#btn_register");

		elements.$btnLogout = $(".btn_logout");

	}

	function init() {

		cacheElements();
		bindEvents();
		
	}

	return {
		init:init,
		login:login,
		logout:logout
	};

}());

module.exports = window.loginOnlyTalent;