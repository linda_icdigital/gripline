import Homepage from './homepage';
import BlackChili from './blackchili';
import BlackChiliSurvey from './blackchili-survey';
import Cookies from 'js-cookie';

var Main = (function() {
	'use strict';

	var ajax__request;

	function init() {

		if($('body').hasClass('homePage')){

			Homepage.init();

		}
		if($('body').hasClass('blackChiliPage')){

			BlackChili.init();

		}
		if($('body').hasClass('blackChiliSurveyPage')){

			BlackChiliSurvey.init();

		}
		$("body").on("click", ".owl-item.active.center [data-evento], .wrapper_card [data-evento]", function() {

			var id_evento = $(this).attr("data-evento");

	        if ($(".contact-modal").hasClass("open")) {
	            $(".contact-modal").removeClass("open");
	        } else {

	        	ajax__request = $.ajax({
		          	url: parameters.ajax_url,
					method: 'POST',
					dataType: "json",
					data: {
						id_evento: id_evento,
						action: 'get_detail_event'
					}
				});

				ajax__request.done(function( data ){

					$(".modal--title").html(data.title);
					$("#modal--location").html(data.location);
					$("#modal--cover").attr("src",data.cover);
					$("#modal--timer").html(data.data);
					$("#modal--description").html(data.description);
	           		$(".contact-modal").addClass("open");

	           	});

	        }

		});
		
		var $postscarousels = $(".posts-carousel");
		$postscarousels.each(function(){
			$(this).children().each( function( index ) {
				$(this).attr( 'data-position', index );
			});
			var $owl_news = $(this).owlCarousel({
				loop: true,
				center: true,
				items: 3,
				margin: 0,
				autoplay: false,
				autoplaySpeed: 1000,
				autoplayTimeout: 5000,
				smartSpeed: 450,
				nav: true,
				navText: ["<i class='owl-custom ion-chevron-left'></i>", "<i class='owl-custom ion-chevron-right'></i>"],
				responsive: {
					0: {
						items: 1
					},
					768: {
						items: 2
					},
					1170: {
						items: 3
					}
				}
			});
			$("body").on('click', '.posts-carousel .owl-item > div', function() {
				$owl_news.trigger('to.owl.carousel', $(this).data( 'position' ) ); 
			});
		});

	}

	return {
		init:init
	};

}());

module.exports = Main;
