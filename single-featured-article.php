<?php 
/*
 * Template Name: Featured Article
 * Template Post Type: post
*/

get_header(); setlocale(LC_TIME, 'it_IT.UTF8');?>
	<div id="home"></div>
	<?php
	if(have_posts()) : while(have_posts()) : the_post();
		$categories = get_the_category();
		$category_id = $categories[0]->cat_ID;
	?>


	<section class="upper-page section-dark screen">
		<?php $url_bg_image = get_the_post_thumbnail_url($post->ID, 'full'); ?>
        <div class="hero-fullscreen overlay overlay-dark-15">
			<div class="hero-fullscreen-FIX">
                <div class="hero-bg bg-img-SINGLE" style="background-image: url(<?= $url_bg_image; ?>);"></div>
            </div>
        </div>
    </section>


    <section class="wrapper_card inner-spacer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="inner-divider-news"></div>
					<?php /* ?><img alt="<?=the_title(false)?>" src="<?=get_the_post_thumbnail_url($post->ID, 'full');?>" class="img-responsive">
					<div class="inner-divider-news-half"></div><?php */ ?>
					<div class="the-overline <?php if($category_id == 4){ ?> black <?php } ?>"></div>
					<div class="inner-divider-news-half"></div>
					<h1 class="post-all-heading"><?=the_title(false)?></h1>
					<div class="inner-divider-news-half"></div>
					<div class="content">
						<?php the_content(); ?>
					</div>
					<div class="inner-divider-news-half"></div>
				<!-- VIDEO -->
					<?php if( have_rows('video_list_featured_article') ): 
						while( have_rows('video_list_featured_article') ): the_row();
                        $titolo_video = get_sub_field('titolo_video_featured_article'); 
						$id_video = get_sub_field('youtube_id_video_featured_article');
					?>
					<?php if( $id_video ): ?>
                        <div class="content__video">
                            <?php /*if( $titolo_video ): ?><h2 class="title"><?php echo $titolo_video; ?></h2><?php endif;*/ ?>
                            <?php echo $titolo_video; ?>
    						<div class="videoWrap">
    							<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $id_video; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    						</div>
                        </div>
					<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
				<!--end -->	
				<!-- TEXT BLOCK 2 -->
					<?php if(get_field('text_block_2_featured_article')) { ?>
						<div class="inner-divider-news-half"></div>
						<div class="content">
							<?php the_field('text_block_2_featured_article'); ?>
						</div>
						<div class="inner-divider-news-half"></div>
					<?php } ?>
				<!--end-->
				</div>
			</div>
		</div>
    </section>


    <!-- THUMBNAILS GALLERY -->
	<?php 
		$images = get_field('images_thumbnails_gallery_featured_article');
		if( $images ): 
	?>
    <section class="upper-page section-dark" id="thumbnail_gallery">
        <div class="hero-fullscreen-thumbnail-slider">
            <div class="hero-bg">
                <div class="swiper-container swiper-slider-top">
                    <!-- thumbnail slider IMG start -->
                    <div class="swiper-wrapper">
                        <?php foreach( $images as $image ): ?>
                        	<!-- overlay overlay-dark-60 -->
							<div class="swiper-slide" style="background-image:url(<?php echo $image['url']; ?>);"></div>
				        <?php endforeach; ?>
                    </div><!-- thumbnail slider IMG end -->
                    <!-- controls start -->
                    <div class="swiper-button-next swiper-button-white"></div>
                    <div class="swiper-button-prev swiper-button-white"></div>
                </div>
                <div class="swiper-container swiper-slider-bottom">
                    <!-- thumbnail slider thumbnail IMG start -->
                    <div class="swiper-wrapper">
                    	<?php foreach( $images as $image ): ?>
							<div class="swiper-slide" style="background-image:url(<?php echo $image['url']; ?>);"></div>
				        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<?php endif; ?>
    <!--end-->



    <!-- TEXT BLOCK 3 -->
    <?php if(get_field('text_block_3_featured_article')) { ?>
	    <section class="wrapper_card inner-spacer text_block">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">

                        <?php if(get_field('title_text_block_3_featured_article')) { ?>
    						<div class="inner-divider-news-half"></div>
    						<div class="the-overline"></div>
    						<div class="inner-divider-news-half"></div>
    						<h2 class="post-all-heading"><?php the_field('title_text_block_3_featured_article'); ?></h2>
                        <?php } ?>

						<div class="inner-divider-news-half"></div>
						<div class="content">
							<?php the_field('text_block_3_featured_article'); ?>
						</div>
						<div class="inner-divider-news-half"></div>
					</div>
				</div>
			</div>
	    </section>
    <?php } ?>
    <!--end-->



    <!--QUOTES-->
    <?php if( have_rows('quote_list_featured_article') ): ?>
    <section class="section-dark" id="testimonials">
        <div class="inner-divider"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 nopadding">
                    <div class="post-all">
                        <div class="owl-carousel testimonials-carousel">
                            <?php while( have_rows('quote_list_featured_article') ): the_row(); 
								$text_quote = get_sub_field('text_quote_featured_article');
								$author_quote = get_sub_field('author_quote_featured_article');
							?>
                            <div class="post-all">
                                <!--<div class="the-overline the-overline-testimonials"></div>
                                <div class="inner-divider-half"></div>-->
                                <div class="section-intro section-intro-testimonials section-intro-dark">
                                    <div class="intro-quote">
                            			<span class="ion-quote"></span>
                        			</div>
                                    <div class="inner-divider-half"></div>
                                	<?php if( $text_quote ): ?>
										<p><?php echo $text_quote; ?></p>
									<?php endif; ?>                                
                                    <div class="inner-divider-half"></div>
                                    <div class="testimonials-signature">
                                        <?php if( $author_quote ): ?>
											<?php echo $author_quote; ?>
										<?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="inner-divider"></div>
    </section>
	<?php endif; ?>
    <!--end-->



    <!--GALLERY-->
	<?php 
		$images = get_field('images_gallery_featured_article');
		if( $images ): 
	?>
    <section class="section-dark" id="works">
        <div class="container-fluid nopadding">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="owl-carousel" id="works-page-img-carousel">
                		<?php foreach( $images as $image ): ?>
                		<div class="item item-grid-size">
                            <div class="works-page-carousel-item" style="background-image:url(<?php echo $image['url']; ?>);">
                                <a class="link-wrap popup-photo" href="<?php echo $image['url']; ?>"><span></span><span></span></a>
                            </div>
                        </div>
                    	<?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!--end-->



    <!-- POST -->
    <?php if( have_rows('posts_list_featured_article') ): ?>
        <section class="inner-spacer section-light" id="featured-work">
            <div class="inner-divider featured-hide"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 nopadding">
                        <div class="slick-left">
                        	<?php while( have_rows('posts_list_featured_article') ): the_row(); 
								$title_post = get_sub_field('title_post_featured_article');
								$text_post = get_sub_field('text_post_featured_article');
							?>
                            <div class="blockquote">
                                <div class="center-container-slick">
                                    <div class="center-block-slick">
                                        <div class="inner-divider featured-show"></div>
                                        <div class="the-overline the-overline-featured"></div>
                                        <div class="inner-divider-half"></div>
                                        <h3 class="cd-featured-name">
                                            <?php echo $title_post; ?>
                                        </h3>
                                        <div class="inner-divider-news"></div>
                                        <div class="cd-section-txt">
                                            <?php echo $text_post; ?>
                                        </div>
                                        <div class="inner-divider-news"></div>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <div class="col-md-6 nopadding">
                        <div class="slick-right">
                            <?php while( have_rows('posts_list_featured_article') ): the_row();
								$image_post = get_sub_field('image_post_featured_article');
							?>
                            <div class="img-fullwidth-wrapper">
                                <div class="img-fullwidth img-fullwidth-all" style="background-image:url(<?php echo $image_post; ?>);"></div>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <div id="bar">
                        <img alt="Bar Arrow" src="<?=get_template_directory_uri()?>/public/images/bar.png">
                    </div>
                    <div class="slick-bottom_v2">
                    	<?php while( have_rows('posts_list_featured_article') ): the_row(); ?>
                    		<div>
	                            <div class="cd-author"></div>
	                        </div>
                    	<?php endwhile; ?>
                    </div>

                </div>
            </div>
            <div class="inner-divider"></div>
        </section>
    <?php endif; ?>
    <!--end-->



	<section class="wrapper_card inner-spacer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div style="margin:30px 0;">
                    	<a class="custom-button fadeIn-element" href="<?php bloginfo('url'); ?>">Torna alla home</a>
                    </div>
				</div>
			</div>
		</div>
    </section>


	<?php endwhile; endif; ?>
<?php get_footer(); ?>