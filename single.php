<?php get_header(); setlocale(LC_TIME, 'it_IT.UTF8');?>
	<div id="home"></div>
	<?php
	if(have_posts()) : while(have_posts()) : the_post();
		$categories = get_the_category();
		$category_id = $categories[0]->cat_ID;
	?>


	<section class="upper-page section-dark screen">
		<?php $url_bg_image = get_the_post_thumbnail_url($post->ID, 'full'); ?>
        <div class="hero-fullscreen overlay overlay-dark-15">
			<div class="hero-fullscreen-FIX">
                <div class="hero-bg bg-img-SINGLE" style="background-image: url(<?= $url_bg_image; ?>);"></div>
            </div>
        </div>
    </section>



    <section class="wrapper_card inner-spacer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="inner-divider-news"></div>
					<?php /* ?><img alt="<?=the_title(false)?>" src="<?=get_the_post_thumbnail_url($post->ID, 'full');?>" class="img-responsive">
					<div class="inner-divider-news-half"></div><?php */ ?>
					<div class="the-overline <?php if($category_id == 4){ ?> black <?php } ?>"></div>
					<div class="inner-divider-news-half"></div>
					<h1 class="post-all-heading"><?=the_title(false)?></h1>
					<div class="inner-divider-news-half"></div>
					<div class="content">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
    </section>

    <?php if(get_field('quote_article') || get_field('quote_image_article')) { ?>
    <section class="section-dark" id="quote">
        <div class="halves">
            <div class="half">
                <div class="inner-divider"></div>
                <div class="post-all inner-spacer">
                    <div class="the-overline"></div>
                    <div class="inner-divider-half"></div>
                    <div class="section-intro">
                        <p>
                            <?php the_field('quote_article'); ?>
                        </p>
                    </div>
                    <div class="inner-divider-half"></div>
                    <div class="intro-quote-wrapper">
                        <div class="intro-quote">
                            <span class="ion-quote"></span>
                        </div>
                    </div>
                </div>
                <div class="inner-divider"></div>
            </div>
            <div class="half about-bg hidden-xs" style="background-image:url(<?php the_field('quote_image_article'); ?>)"></div>
            <div class="half about-bg visible-xs" style="background-image:url(<?php the_field('quote_image_article'); ?>)"></div>
        </div>
    </section>
	<?php } ?>


	<section class="wrapper_card inner-spacer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div style="margin:30px 0;">
                    	<a class="custom-button fadeIn-element" href="<?php bloginfo('url'); ?>">Torna alla home</a>
                    </div>
				</div>
			</div>
		</div>
    </section>


	<?php endwhile; endif; ?>
	<?php /* ?><section class="section-light slider_events">
		<div class="inner-divider"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<h5 class="post-all-heading" style="text-align: center;">POTREBBE INTERESSARTI ANCHE</h5>
					<div class="inner-divider-half"></div>
					<div class="owl-carousel posts-carousel">
						<?php
						$args = array(
							'post_type'      => 'post',
							'posts_per_page'    => -1,
							'cat'     => 5
						);
						$wp_query = new WP_Query( $args );
						if($wp_query->have_posts()) :
							while($wp_query->have_posts()) : $wp_query->the_post();
								$categories = get_the_category();
								$category_id = $categories[0]->cat_ID;
						?>
						<div class="news-item">
							<figure class="news-content">
								<img alt="<?=the_title(false)?>" src="<?=get_the_post_thumbnail_url($post->ID, 'full');?>">
								<figcaption>
									<div class="inner-divider-news"></div>
									<div class="the-overline the-overline-news <?php if($category_id == 4){ ?> black <?php } ?>"></div>
									<div class="inner-divider-news-half"></div>
									<h4 class="post-all-heading"><?=the_title(false)?></h4>
									<div class="inner-divider-news-half"></div>
									<h5><?=get_the_date('d F Y');?></h5>
									<div class="inner-divider-news"></div>
									<div class="section-txt-news">
										<p><?=the_excerpt()?></p>
									</div>
									<div class="inner-divider-news"></div>
			                		<center><a class="custom-button" href="<?php the_permalink(); ?>">Read more</a></center>
								</figcaption>
							</figure>
						</div>
						<?php
							endwhile;
						endif;
						wp_reset_query();
						wp_reset_postdata();
						?>
					</div>
				</div>
			</div>
		</div>
		<center>
			<a href="<?php echo get_category_link(5); ?>" class="the-button-wrapper the-button-wrapper-news btn_archivio_eventi">
				<div class="the-button the-button-xl">Archivio news</div>
			</a>
		</center>
		<div class="inner-divider"></div>
	</section><?php */ ?>
<?php get_footer(); ?>