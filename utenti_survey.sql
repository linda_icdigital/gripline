-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jul 19, 2019 at 06:50 PM
-- Server version: 5.7.23
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `continental-gripline`
--

-- --------------------------------------------------------

--
-- Table structure for table `utenti_survey`
--

CREATE TABLE `utenti_survey` (
  `id` int(11) NOT NULL,
  `id_utente` int(11) DEFAULT NULL,
  `privacy_1` enum('0','1') DEFAULT NULL,
  `privacy_2` enum('0','1') DEFAULT NULL,
  `risposta_1` varchar(255) DEFAULT NULL,
  `risposta_2` varchar(255) DEFAULT NULL,
  `risposta_3` varchar(255) DEFAULT NULL,
  `risposta_4` varchar(255) DEFAULT NULL,
  `risposta_5` varchar(255) DEFAULT NULL,
  `risposta_6` varchar(255) DEFAULT NULL,
  `risposta_7` varchar(255) DEFAULT NULL,
  `risposta_8` varchar(255) DEFAULT NULL,
  `risposta_9` varchar(255) DEFAULT NULL,
  `risposta_10` varchar(255) DEFAULT NULL,
  `risposta_11` varchar(255) DEFAULT NULL,
  `risposta_12` varchar(255) DEFAULT NULL,
  `fb_url` varchar(255) DEFAULT NULL,
  `ig_url` varchar(255) DEFAULT NULL,
  `punteggio` int(11) NOT NULL,
  `quando` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `utenti_survey`
--

INSERT INTO `utenti_survey` (`id`, `id_utente`, `privacy_1`, `privacy_2`, `risposta_1`, `risposta_2`, `risposta_3`, `risposta_4`, `risposta_5`, `risposta_6`, `risposta_7`, `risposta_8`, `risposta_9`, `risposta_10`, `risposta_11`, `risposta_12`, `fb_url`, `ig_url`, `punteggio`, `quando`) VALUES
(1, 6, NULL, NULL, 'Scegliere pneumatici di alta qualità, controllarli costantemente, affidarsi a tecnici professionisti per il montaggio e smontaggio ed effettuare una manutenzione regolare', 'Il gruppo Continental è composto soltanto da 3 divisioni', 'Una crescita esponenziale della potenza della vettura per dare maggiore velocità al veicolo', 'Una mescola rivoluzionaria con un grip, una velocità e una durevolezza imbattibili, che si traducono in una tenuta di strada incredibilmente affidabile', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-07-19 16:46:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `utenti_survey`
--
ALTER TABLE `utenti_survey`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `utenti_survey`
--
ALTER TABLE `utenti_survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
